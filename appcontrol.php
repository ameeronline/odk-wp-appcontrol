<?php
/**
 * @package App_Control
 * @version 1.6
 */
/*
Plugin Name: App Control
Plugin URI: http://modsolutionz.com/
Description: Show/Update/Filter reports and Manage User permissions.
Author: Ameer Hamza
Version: 1.0
Author URI: https://www.upwork.com/freelancers/~01ca3023744795f701
*/

//error_reporting(E_STRICT);

if( !isset($_SESSION) )
	session_start();

define("APPCONTROL_PATH", plugin_dir_path( __FILE__ ));
define("APPCONTROL_URL", plugin_dir_url( __FILE__ ));

require_once plugin_dir_path( __FILE__ ) . 'vendors/mysqli/vendor/autoload.php';
require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
require_once plugin_dir_path( __FILE__ ) . 'inc/helperfunctions.php';

require_once APPCONTROL_PATH . 'inc/adminlte/rewrite.php';

$appcontroldb = new MysqliDb(
	get_option("appcontrol_dbhost", "localhost"),
	get_option("appcontrol_dbuser", "root"),
	get_option("appcontrol_dbpass", ""),
	get_option("appcontrol_dbname", "appcontrol_odkdb"),
	get_option("appcontrol_dbport", "3306"));

try {
	$appcontroldb->connect();
} catch (Exception $e) {
	add_action("admin_notices", function() {
		echo "<div class='error notice'><p>Can't Connect to Odk Database please configure them property.</p></div>";
	});
}

require_once plugin_dir_path( __FILE__ ) . 'inc/graph1.php';

function appcontrol_register_activation() {

	if(get_option("appcontrol_dbhost") == "")
		add_option("appcontrol_dbhost", "localhost");

	if(get_option("appcontrol_dbuser") == "")
		add_option("appcontrol_dbuser", "root");

	if(get_option("appcontrol_dbpass") == "")
		add_option("appcontrol_dbpass", "");

	if(get_option("appcontrol_dbport") == "")
		add_option("appcontrol_dbport", "3306");

	if(get_option("appcontrol_dbname") == "")
		add_option("appcontrol_dbname", "odk_testdb");

}

register_activation_hook(__FILE__, 'appcontrol_register_activation');

function appcontrol_adminhead() {
	echo '<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />';
	echo '<link rel="stylesheet" type="text/css" href="'. plugin_dir_url( __FILE__ ) .'assets/css/appcontrol.css" />';
	echo '<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>';
	echo '<script type="text/javascript" src="'. plugin_dir_url( __FILE__ ) .'assets/js/appcontrol.js"></script>';
}

add_action( 'admin_head', 'appcontrol_adminhead' );

function appcontrol_front() {
	wp_enqueue_style( 'appcontrol-css', plugin_dir_url( __FILE__ ) . 'css/front.css' );
	wp_enqueue_script( 'appcontrol-js', plugin_dir_url( __FILE__ ) . 'js/front.js' );
}

add_action( 'wp_head', 'appcontrol_front' );

// Registering admin pages for appcontrol

add_action('admin_menu', 'appcontrol_createmenu');

function appcontrol_createmenu() {

	add_menu_page('Odk tables', 'App control', 'administrator', 'appcontrol-settings', 'odktables_admin_page' , plugins_url('assets/img/icons/usermanagment.png', __FILE__) );

	add_submenu_page( 'appcontrol-settings', 'Odk Tables', 'Odk Tables', 'administrator', 'appcontrol-settings', 'odktables_admin_page' );

	add_submenu_page( 'appcontrol-settings', 'Settings', 'Settings', 'administrator', 'appcontrol-setting', 'appcontrol_settings_page' );

	add_submenu_page( 'appcontrol-settings', 'Form Add Report', 'Form Add Report', 'administrator', 'appcontrol-formaddreport', 'addreports_admin_page' );

	add_submenu_page( null, 'Form Permissions', 'Form Permissions', 'administrator', 'appcontrol-formpermission', 'appcontrol_form_permissions' );

	add_submenu_page( null, 'Form Reports', 'Form Reports', 'administrator', 'appcontrol-formreports', 'reports_admin_page' );

	add_submenu_page( null, 'Form Edit Report', 'Form Reports', 'administrator', 'appcontrol-formeditreport', 'editreports_admin_page' );

	add_submenu_page( null, 'Form Data', 'Form Data', 'administrator', 'appcontrol-formdata', 'odktable_data_admin_page' );

	add_submenu_page( null, 'Edit Form', 'Edit Form', 'administrator', 'appcontrol-editformdata', 'odktable_editdata_admin_page' );

}


function odktables_admin_page() {

	include( plugin_dir_path( __FILE__ ) . 'inc/odktables_page.php');

}

function reports_admin_page() {

	if( getqueryvar("formid") == "" ) {
		appcontrol_redirect("appcontrol-settings");
		exit;
	}

	$form = getform_by_id( getqueryvar("formid") );

	$reports = appcontrol_getreports( getqueryvar("formid") );

	include( plugin_dir_path( __FILE__ ) . 'inc/reports_page.php' );

}

function appcontrol_settings_page() {

	if (getpostvar("saveappcontrol_settings") == "1") {
		update_option("appcontrol_dbhost", getpostvar("appcontrol_dbhost"));
		update_option("appcontrol_dbuser", getpostvar("appcontrol_dbuser"));
		update_option("appcontrol_dbpass", getpostvar("appcontrol_dbpass"));
		update_option("appcontrol_dbport", getpostvar("appcontrol_dbport"));
		update_option("appcontrol_dbname", getpostvar("appcontrol_dbname"));
		$success = 1;
	}

	include( plugin_dir_path( __FILE__ ) . 'inc/settings_page.php');

}

function appcontrol_form_permissions() {

	if( getqueryvar("formid") == "" ) {
		appcontrol_redirect("appcontrol-settings");
		exit;
	}

	if (getpostvar("savepermissions") == 1) {

		$formid = getqueryvar("formid");

		$view_data = array();
		$create_reports = array();
		$edit_reports = array();

		$permission_data = array();

		if( !empty(getpostvar("view_data")) ) {
			foreach (getpostvar("view_data") as $k => $vd) {
				$permission_data[$k]["view_data"] = $vd;
			}
		}

		if( !empty(getpostvar("edit_data")) ) {
			foreach (getpostvar("edit_data") as $k => $vd) {
				$permission_data[$k]["edit_data"] = $vd;
			}
		}

		foreach (get_appcontrolusers() as $appuser) {
			if( !isset($permission_data[$appuser->ID]) ) {
				$permission_data[$appuser->ID] = array();
			}
		}

		foreach ($permission_data as $k => $permissions) {

			$_permissions = array( "view_data", "edit_data" );

			foreach ($_permissions as $_permission) {
				if( isset( $permissions[$_permission] ) ) {
					if( get_user_meta($k,$formid."_".$_permission, true) === "" ) {
						add_user_meta($k,$formid."_".$_permission, 1);
					} else if(get_user_meta($k,$formid."_".$_permission, true) == 0) {
						update_user_meta($k,$formid."_".$_permission, 1);
					}
				} else {
					update_user_meta($k,$formid."_".$_permission, 0);
				}
			}

		}

	}

	$form = getform_by_id( getqueryvar("formid") );

	include( plugin_dir_path( __FILE__ ) . 'inc/form_permissions.php');

}

function editreports_admin_page() {

	if( getqueryvar("reportid") == "" ) {
		appcontrol_redirect("appcontrol-settings");
		exit;
	}

	if( getpostvar("formfields") ) {
		
		appcontrol_updatereportfields( getpostvar("formfields"), getqueryvar("reportid") );

	}

	$report = appcontrol_getreport_by_id( getqueryvar("reportid") );

	$form = getform_by_id( $report["formid"] );

	include( plugin_dir_path( __FILE__ ) . 'inc/editreport.php');

}

function addreports_admin_page() {

	if( getpostvar("formfields") ) {

		if( getpostvar("formid") == "" ) {
			appcontrol_redirect("appcontrol-settings");
			exit;
		}
		
		$id = appcontrol_addreportfields( getpostvar("reportname"), getpostvar("formfields"), getpostvar("formid") );

		appcontrol_redirect("appcontrol-formeditreport&reportid=". $id);
		exit;

	}

	$forms = appcontrol_getforms();

	include( plugin_dir_path( __FILE__ ) . 'inc/addreport.php');

}

function odktable_data_admin_page() {

	if( getqueryvar("formid") == "" ) {
		appcontrol_redirect("appcontrol-settings");
		exit;
	}

	$form = getform_by_id( getqueryvar("formid") );
	
	include( plugin_dir_path( __FILE__ ) . 'inc/odktable_data.php');

}

function odktable_editdata_admin_page() {

	if( getqueryvar("formid") == "" ) {
		appcontrol_redirect("appcontrol-settings");
		exit;
	}

	$form = getform_by_id( getqueryvar("formid") );
	$row_id = getqueryvar("row");

	if ( getpostvar("saveformdata") == 1 ) {

		$formfields = array();

		foreach (getpostvar("formfields") as $k => $val) {
			if(in_array($k, $form["formfields"])) {
				$formfields[$k] = $val;
			}
		}

		global $appcontroldb;

		$appcontroldb->where('_URI', $row_id);
		$appcontroldb->update( $form["tablename"], $formfields );
		
		echo "<script>location=location.href</script>";
		exit;

	}

	include( plugin_dir_path( __FILE__ ) . 'inc/odktable_editdata.php');

}

//Registering Plugin admin ajax calls

add_action( 'wp_ajax_appcontrolshowformcolumns', 'appcontrolshowformcolumns' );

function appcontrolshowformcolumns() {

	$form = getform_by_id( $_POST['formid'] );
	include( plugin_dir_path( __FILE__ ) . 'inc/showformcolumns.php');

	wp_die();

}

//Disconnecting Database connection

global $appcontroldb;

if($appcontroldb != null)
	$appcontroldb->disconnect();