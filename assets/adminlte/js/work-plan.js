var angularapp = angular.module("angularApp",[]);

var mainController = angularapp.controller("mainController", function($scope) {

	$scope.nHandleSubmit = function() {
		//return false;
	};

});

var appscope = "";

var workPlan = angularapp.controller("workPlanCtrl", function($scope) {

	appscope = $scope;

	jQuery(workplans).each(function(k,v) {

		this.outcome = parseInt(this.outcome);
		this.output_number = parseInt(this.output_number);
		this.activity_number = parseInt(this.activity_number);

		if( this.planned_start ) {
			var planned_start = this.planned_start.split(/[- :]/);
			//planned_start[3], planned_start[4], planned_start[5]
			var r = new Date(Date.UTC(planned_start[0], planned_start[1]-1, planned_start[2], 0, 0 ,0));
			workplans[k]["planned_start"] = r == "Invalid Date" ? "" : r;
		} else {
			workplans[k]["planned_start"] = "";
		}

		if ( this.planned_end ) {
			var planned_end = this.planned_end.split(/[- :]/);
			//planned_end[3], planned_end[4], planned_end[5]
			var r = new Date(Date.UTC(planned_end[0], planned_end[1]-1, planned_end[2], 0, 0, 0));
			workplans[k]["planned_end"] = r == "Invalid Date" ? "" : r;
		} else {
			workplans[k]["planned_end"] = "";
		}

		if ( this.actual_start ) {
			var actual_start = this.actual_start.split(/[- :]/);
			var r = new Date(Date.UTC(actual_start[0], actual_start[1]-1, actual_start[2], 0,0,0));
			//actual_start[3], actual_start[4], actual_start[5]
			workplans[k]["actual_start"] = r == "Invalid Date" ? "" : r;
		} else {
			workplans[k]["actual_start"] = "";
		}

		if ( this.actual_end ) {
			var actual_end = this.actual_end.split(/[- :]/);
			var r = new Date(Date.UTC(actual_end[0], actual_end[1]-1, actual_end[2], 0, 0, 0));
			//, actual_end[3], actual_end[4], actual_end[5]
			workplans[k]["actual_end"] = r == "Invalid Date" ? "" : r;
		} else {
			workplans[k]["actual_end"];
		}

	});

	$scope.workplans = workplans;

	$scope.showAdd = false;

	$scope.id = null;
	$scope.project_code = "";
	$scope.outcome = "";
	$scope.output_number = "";
	$scope.activity_number = "";
	$scope.activity_name = "";
	$scope.planned_start = "";
	$scope.planned_end = "";
	$scope.actual_start = "";
	$scope.actual_end = "";
	$scope.total_target = "";
	$scope.achievement = "";
	$scope.progress = "";
	$scope.activity_status = "";
	$scope.comments = "";
	$scope.ii = null;

	$scope.projectCodeHandle = function() {

		$scope.project_code = $scope.projectCodeTxt;

	}

	$scope.changeHandleT = function() {
		var total_target = isNaN(parseInt($scope.total_target)) || $scope.total_target == 0 ? 1 : parseInt($scope.total_target);
		var achievement = isNaN(parseInt($scope.achievement)) ? 0 : parseInt($scope.achievement);
		$scope.progress = ((achievement/total_target) * 100).toFixed(2);
	}

	$scope.addAction = function(id) {
		
		var index;

		jQuery($scope.workplans).each(function(k,v) {
			if(v.id == id) {
				index = k;
				return false;
			}
		});

		$scope.id = null;
		//$scope.project_code = "";
		$scope.outcome = "";
		$scope.output_number = "";
		$scope.activity_number = "";
		$scope.activity_name = "";
		$scope.planned_start = "";
		$scope.planned_end = "";
		$scope.actual_start = "";
		$scope.actual_end = "";
		$scope.total_target = "";
		$scope.achievement = "";
		$scope.progress = "";
		$scope.activity_status = "";
		$scope.comments = "";
		$scope.showAdd = true;
		if( index != undefined ) {

			$scope.ii = index;
			$scope.id = $scope.workplans[index].id;

			$scope.project_code = $scope.workplans[index].project_code;
			$scope.outcome = $scope.workplans[index].outcome;
			$scope.output_number = $scope.workplans[index].output_number;
			$scope.activity_number = $scope.workplans[index].activity_number;
			$scope.activity_name = $scope.workplans[index].activity_name;
			$scope.planned_start = $scope.workplans[index].planned_start;
			$scope.planned_end = $scope.workplans[index].planned_end;
			$scope.actual_start = $scope.workplans[index].actual_start;
			$scope.actual_end = $scope.workplans[index].actual_end;
			$scope.total_target = $scope.workplans[index].total_target;
			$scope.achievement = $scope.workplans[index].achievement;
			$scope.progress = $scope.workplans[index].progress;
			$scope.activity_status = $scope.workplans[index].activity_status;
			$scope.comments = $scope.workplans[index].comments;

		}
	}

	$scope.cancelAction = function() {
		$scope.showAdd = false;
	}

	$scope.saveEntry = function() {

		//Validation

		//Outcome 1-9
		//Output (Outcome[3])31-39
		//Activity (Output[31])311-319

		var outcome = parseInt($scope.outcome);

		if( isNaN(outcome) || !(outcome > 0 && outcome <= 9) ) {
			alert("Outcome has invalid value.");
			return;
		}

		var output_number = parseInt($scope.output_number);

		var outputrangemin = (outcome*10)+1;
		var outputrangemax = (outcome*10)+9;

		if( isNaN(output_number) || !(output_number >= outputrangemin && output_number <= outputrangemax) ) {
			alert("Output has invalid value.");
			return;
		}

		var activity_number = parseInt($scope.activity_number);

		var activityrangemin = (output_number*10)+1;
		var activityrangemax = (output_number*10)+9;

		if( isNaN(activity_number) || !(activity_number >= activityrangemin && activity_number <= activityrangemax) ) {
			alert("Activity has invalid value.");
			return;
		}

		// Auto Status change query

		var planned_start = new Date($scope.planned_start);
		var planned_end = new Date($scope.planned_end);
		var actual_start = new Date($scope.actual_start);
		var actual_end = new Date($scope.actual_end);

		if( Date.compare( Date.today(),  planned_start) == -1 ) {
			$scope.activity_status = "NA";
		}

		if( Date.compare( Date.today(),  planned_start) == 1 &&  ( (!$scope.actual_start) || (Date.compare( actual_start, Date.today().add(-365 ).day() ) == -1) ) ) {
			$scope.activity_status = "Delayed";
		}

		if( !!$scope.actual_start && !!$scope.planned_start && Date.compare( actual_start,  planned_start) == 1 &&  Date.compare( Date.today(), planned_end ) == -1 ) {
			$scope.activity_status = "In Progress";
		}

		if( !!$scope.actual_start && !!$scope.actual_end && Date.compare( actual_start, Date.today().add(-365 ).day() ) == 1
			&& Date.compare( actual_end, Date.today().add(-365 ).day() ) == 1 ) {
			$scope.activity_status = "Completed";
		}

		//$scope.workplans[$scope.ii]

		var data = {
			project_code : $scope.project_code,
			outcome : $scope.outcome,
			output_number : $scope.output_number,
			activity_number : $scope.activity_number,
			activity_name : $scope.activity_name,
			planned_start : pDate(planned_start),
			planned_end : pDate(planned_end),
			actual_start : pDate(actual_start),
			actual_end : pDate(actual_end),
			total_target : $scope.total_target,
			achievement : $scope.achievement,
			progress : $scope.progress,
			activity_status : $scope.activity_status,
			comments : $scope.comments
		};

		if( $scope.id != null ) {
			data.id = $scope.id;
			$scope.workplans[$scope.ii] = data;
		}

		$scope.showAdd = false;

		jQuery.ajax({
			url: site_url+"/appcontrol/saveworkplan",
			data: data,
			type: "POST",
			success: function( res ) {
				if(res.indexOf("id") != -1) {
					var res = jQuery.parseJSON(res);
					data.id = res.id;
					$scope.workplans.push(data);
					$scope.$apply();
					alert("Project has been added.");
				} else {
					alert("Project has been updated.");
				}
			}
		});

	}

	$scope.call = function() {
		var outcome = $scope.outcome;
		alert(outcome);
	}

	$scope.deleteEntry = function(id) {

		var ii;

		jQuery($scope.workplans).each(function(k,v) {
			if(v.id == id) {
				ii = k;
				return false;
			}
		});

		if(!confirm("Are you sure to perform this action?"))
			return;

		if( $scope.workplans[ii] ) {

			jQuery.ajax({
				url: site_url+"/appcontrol/deleteworkplan",
				data: {
					id: $scope.workplans[ii].id
				},
				type: "POST",
				success: function( res ) {}
			});

			$scope.workplans.splice(ii, 1);
		}

	}

});

function activitycheck(){
		var outcometest = 0;
		var outputtest = 0;
		var activitytest = 0;
		var outcome = document.getElementById("outcome").value;
		var output = document.getElementById("output").value;
		var outputrangemin = outcome*10;
		var outputrangemax = outcome*10 + 10;
		var activity = document.getElementById("activity").value;
		var activityrangemin = output*10;
		var activityrangemax = output*10 + 10;
		if (outcome > 0 && outcome < 10){
		}
		else{
			outcometest = 1;
		}
		if (output > outputrangemin && output < outputrangemax){
		}
		else{
			outputtest = 1;
		}

		if (activity > activityrangemin && activity < activityrangemax){
		}
		else{
			activitytest = 1;
		}
		if (outcometest == 1 || outputtest == 1 || activitytest == 1){
			document.getElementById("outcome").style.backgroundColor = "red";
			document.getElementById("output").style.backgroundColor = "red";
			document.getElementById("activity").style.backgroundColor = "red";
			alert("Please confirm all red colored fields");
			return false;
		}
	}

function pDate(d) {
	if(d) {
		var m = (d.getMonth()+1);
		var day = d.getDate();
		return d.getFullYear() +"-"+ (m > 9 ? m : "0"+m) +"-"+ (day > 9 ? day : "0"+day);
	} else
		return "";
}


angularapp.directive('dateInput', function(){
	return {
		restrict : 'A',
		scope : {
			ngModel : '='
		},
		link: function (scope) {
			if (scope.ngModel) scope.ngModel = new Date(scope.ngModel);
		}
	}
});