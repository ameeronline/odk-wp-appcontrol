jQuery(document).ready(function() {
	jQuery("#formfieldselect").on("change", function() {
		jQuery.ajax({

			url: ajaxurl,
			type: "POST",
			data: {
				action: "appcontrolshowformcolumns",
				formid: jQuery("#formfieldselect").val()
			},
			success: function(res) {
				jQuery("#formfields").html(res);
			}

		});
	});
	if( jQuery("#formfieldselect").get(0) ) {
		jQuery("#formfieldselect").change();
	}
});