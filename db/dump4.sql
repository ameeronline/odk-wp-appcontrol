-- MySQL dump 10.13  Distrib 5.7.21, for Win64 (x86_64)
--
-- Host: localhost    Database: logixsol_odk
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `form3_awareness_report_attendance_attendancesheetpicture_ref`
--

DROP TABLE IF EXISTS `form3_awareness_report_attendance_attendancesheetpicture_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_attendance_attendancesheetpicture_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_attendance_attendancesheetpicture_ref`
--

LOCK TABLES `form3_awareness_report_attendance_attendancesheetpicture_ref` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_attendance_attendancesheetpicture_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_attendance_attendancesheetpicture_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_attendance_module`
--

DROP TABLE IF EXISTS `form3_awareness_report_attendance_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_attendance_module` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PARTICIPANT_DETAILS_GENDER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PARTICIPANT_DETAILS_DEPARTMENT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PARTICIPANT_DETAILS_EMAIL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PARTICIPANT_DETAILS_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PARTICIPANT_DETAILS_DESIGNATION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PARTICIPANT_DETAILS_AGE_GROUP` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PARTICIPANT_DETAILS_CONTACT_NO` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_attendance_module`
--

LOCK TABLES `form3_awareness_report_attendance_module` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_attendance_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_attendance_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_core`
--

DROP TABLE IF EXISTS `form3_awareness_report_core`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_core` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_MODEL_VERSION` int(9) DEFAULT NULL,
  `_UI_VERSION` int(9) DEFAULT NULL,
  `_IS_COMPLETE` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `_MARKED_AS_COMPLETE_DATE` datetime(6) DEFAULT NULL,
  `LOCATION_LNG` decimal(38,10) DEFAULT NULL,
  `LOCATION_ALT` decimal(38,10) DEFAULT NULL,
  `REMARKS` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_LAT` decimal(38,10) DEFAULT NULL,
  `UC` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_ID` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DISTRICT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WARD` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `EVENTDATE` datetime(6) DEFAULT NULL,
  `ATTENDANCE_MODULE_INITIATE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEHSIL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORT_COMPLETE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MUHALLA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `EVENTSUBJECT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OUTCOME` int(9) DEFAULT NULL,
  `OUTPUT` int(9) DEFAULT NULL,
  `PROJECT_CODE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ATTENDANCE_PARTICIPANTS_FEMALE` int(9) DEFAULT NULL,
  `IMEI` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STREET` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ATTENDANCE_PARTICIPANTS_MALE` int(9) DEFAULT NULL,
  `ATTENDANCE_PARTICIPANTS_NON_MEMBERS` int(9) DEFAULT NULL,
  `LOCATION_ACC` decimal(38,10) DEFAULT NULL,
  `DATE` datetime(6) DEFAULT NULL,
  `EVENTTYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STARTTIME` datetime(6) DEFAULT NULL,
  `EVENT_TITLE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ATTENDANCE_PARTICIPANTS_MEMBERS` int(9) DEFAULT NULL,
  `FACILITATOR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVIEW_END_TIME` datetime(6) DEFAULT NULL,
  `ATTENDANCE_PARTICIPANTS_TOTAL` int(9) DEFAULT NULL,
  `ACTIVITY` int(9) DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_MARKED_AS_COMPLETE_DATE` (`_MARKED_AS_COMPLETE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_core`
--

LOCK TABLES `form3_awareness_report_core` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_core` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_core` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture1_blb`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture1_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture1_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture1_blb`
--

LOCK TABLES `form3_awareness_report_event_picture1_blb` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture1_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture1_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture1_bn`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture1_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture1_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture1_bn`
--

LOCK TABLES `form3_awareness_report_event_picture1_bn` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture1_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture1_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture1_ref`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture1_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture1_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture1_ref`
--

LOCK TABLES `form3_awareness_report_event_picture1_ref` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture1_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture1_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture2_blb`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture2_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture2_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture2_blb`
--

LOCK TABLES `form3_awareness_report_event_picture2_blb` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture2_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture2_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture2_bn`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture2_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture2_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture2_bn`
--

LOCK TABLES `form3_awareness_report_event_picture2_bn` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture2_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture2_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture2_ref`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture2_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture2_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture2_ref`
--

LOCK TABLES `form3_awareness_report_event_picture2_ref` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture2_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture2_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture3_blb`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture3_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture3_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture3_blb`
--

LOCK TABLES `form3_awareness_report_event_picture3_blb` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture3_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture3_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture3_bn`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture3_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture3_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture3_bn`
--

LOCK TABLES `form3_awareness_report_event_picture3_bn` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture3_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture3_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture3_ref`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture3_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture3_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture3_ref`
--

LOCK TABLES `form3_awareness_report_event_picture3_ref` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture3_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture3_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture4_blb`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture4_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture4_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture4_blb`
--

LOCK TABLES `form3_awareness_report_event_picture4_blb` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture4_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture4_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture4_bn`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture4_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture4_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture4_bn`
--

LOCK TABLES `form3_awareness_report_event_picture4_bn` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture4_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture4_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture4_ref`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture4_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture4_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture4_ref`
--

LOCK TABLES `form3_awareness_report_event_picture4_ref` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture4_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture4_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture5_blb`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture5_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture5_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture5_blb`
--

LOCK TABLES `form3_awareness_report_event_picture5_blb` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture5_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture5_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture5_bn`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture5_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture5_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture5_bn`
--

LOCK TABLES `form3_awareness_report_event_picture5_bn` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture5_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture5_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form3_awareness_report_event_picture5_ref`
--

DROP TABLE IF EXISTS `form3_awareness_report_event_picture5_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form3_awareness_report_event_picture5_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form3_awareness_report_event_picture5_ref`
--

LOCK TABLES `form3_awareness_report_event_picture5_ref` WRITE;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture5_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form3_awareness_report_event_picture5_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_core`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_core`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_core` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_MODEL_VERSION` int(9) DEFAULT NULL,
  `_UI_VERSION` int(9) DEFAULT NULL,
  `_IS_COMPLETE` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `_MARKED_AS_COMPLETE_DATE` datetime(6) DEFAULT NULL,
  `LOCATION_LNG` decimal(38,10) DEFAULT NULL,
  `RESPONSIBLE_SUB_WASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_ALT` decimal(38,10) DEFAULT NULL,
  `SCHEME_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REMARKS` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_LAT` decimal(38,10) DEFAULT NULL,
  `UC` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_ID` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DISTRICT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WARD` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INSTITUTIONS_INVOLVED_OTHER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SCHEME_TYPE_OTHER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEHSIL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SCHEME_NUMBER` int(9) DEFAULT NULL,
  `REPORT_COMPLETE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MUHALLA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OUTCOME` int(9) DEFAULT NULL,
  `OUTPUT` int(9) DEFAULT NULL,
  `INTERVENTION_TYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ESTIMATED_BENEFICIARIES_FEMALE` int(9) DEFAULT NULL,
  `PROJECT_CODE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INSTITUTIONS_INVOLVED` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `IMEI` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `RESPONSIBLE_PMG` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STREET` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `RESPONSIBLE_ECG` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORTINGDATE` datetime(6) DEFAULT NULL,
  `LOCATION_ACC` decimal(38,10) DEFAULT NULL,
  `DATE` datetime(6) DEFAULT NULL,
  `ESTIMATED_BENEFICIARIES_MALE` int(9) DEFAULT NULL,
  `STARTTIME` datetime(6) DEFAULT NULL,
  `FACILITATOR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SCHEME_TYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVIEW_END_TIME` datetime(6) DEFAULT NULL,
  `ACTIVITY` int(9) DEFAULT NULL,
  `ESTIMATED_BENEFICIARIES_CHILDREN` int(9) DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_MARKED_AS_COMPLETE_DATE` (`_MARKED_AS_COMPLETE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_core`
--

LOCK TABLES `form4a_infra_initiation_report_core` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_core` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_core` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture1_blb`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture1_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture1_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture1_blb`
--

LOCK TABLES `form4a_infra_initiation_report_picture1_blb` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture1_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture1_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture1_bn`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture1_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture1_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture1_bn`
--

LOCK TABLES `form4a_infra_initiation_report_picture1_bn` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture1_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture1_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture1_ref`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture1_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture1_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture1_ref`
--

LOCK TABLES `form4a_infra_initiation_report_picture1_ref` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture1_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture1_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture2_blb`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture2_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture2_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture2_blb`
--

LOCK TABLES `form4a_infra_initiation_report_picture2_blb` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture2_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture2_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture2_bn`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture2_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture2_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture2_bn`
--

LOCK TABLES `form4a_infra_initiation_report_picture2_bn` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture2_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture2_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture2_ref`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture2_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture2_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture2_ref`
--

LOCK TABLES `form4a_infra_initiation_report_picture2_ref` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture2_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture2_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture3_blb`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture3_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture3_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture3_blb`
--

LOCK TABLES `form4a_infra_initiation_report_picture3_blb` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture3_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture3_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture3_bn`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture3_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture3_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture3_bn`
--

LOCK TABLES `form4a_infra_initiation_report_picture3_bn` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture3_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture3_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture3_ref`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture3_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture3_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture3_ref`
--

LOCK TABLES `form4a_infra_initiation_report_picture3_ref` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture3_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture3_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture4_blb`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture4_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture4_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture4_blb`
--

LOCK TABLES `form4a_infra_initiation_report_picture4_blb` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture4_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture4_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture4_bn`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture4_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture4_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture4_bn`
--

LOCK TABLES `form4a_infra_initiation_report_picture4_bn` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture4_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture4_bn` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-07  9:31:07
