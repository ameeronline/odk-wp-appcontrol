-- MySQL dump 10.13  Distrib 5.7.21, for Win64 (x86_64)
--
-- Host: localhost    Database: logixsol_odk
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `form4a_infra_initiation_report_picture4_ref`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture4_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture4_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture4_ref`
--

LOCK TABLES `form4a_infra_initiation_report_picture4_ref` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture4_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture4_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture5_blb`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture5_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture5_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture5_blb`
--

LOCK TABLES `form4a_infra_initiation_report_picture5_blb` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture5_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture5_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture5_bn`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture5_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture5_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture5_bn`
--

LOCK TABLES `form4a_infra_initiation_report_picture5_bn` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture5_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture5_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4a_infra_initiation_report_picture5_ref`
--

DROP TABLE IF EXISTS `form4a_infra_initiation_report_picture5_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4a_infra_initiation_report_picture5_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4a_infra_initiation_report_picture5_ref`
--

LOCK TABLES `form4a_infra_initiation_report_picture5_ref` WRITE;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture5_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4a_infra_initiation_report_picture5_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_core`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_core`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_core` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_MODEL_VERSION` int(9) DEFAULT NULL,
  `_UI_VERSION` int(9) DEFAULT NULL,
  `_IS_COMPLETE` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `_MARKED_AS_COMPLETE_DATE` datetime(6) DEFAULT NULL,
  `LOCATION_LNG` decimal(38,10) DEFAULT NULL,
  `VARIATION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_ALT` decimal(38,10) DEFAULT NULL,
  `SCHEME_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REMARKS` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MANAGING_GROUP_TYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_LAT` decimal(38,10) DEFAULT NULL,
  `UC` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_ID` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DISTRICT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `POST_IMPLEMENTATION_OPERATION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WARD` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SCHEME_TYPE_OTHER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `COMPLETION_DATE` datetime(6) DEFAULT NULL,
  `TEHSIL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SCHEME_NUMBER` int(9) DEFAULT NULL,
  `COMPLETION_AS_PER_PLAN` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORT_COMPLETE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MUHALLA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OUTCOME` int(9) DEFAULT NULL,
  `OUTPUT` int(9) DEFAULT NULL,
  `INTERVENTION_TYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PROJECT_CODE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `IMEI` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STREET` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORTINGDATE` datetime(6) DEFAULT NULL,
  `LOCATION_ACC` decimal(38,10) DEFAULT NULL,
  `OTHER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DATE` datetime(6) DEFAULT NULL,
  `STARTTIME` datetime(6) DEFAULT NULL,
  `FACILITATOR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SCHEME_TYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVIEW_END_TIME` datetime(6) DEFAULT NULL,
  `ACTIVITY` int(9) DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_MARKED_AS_COMPLETE_DATE` (`_MARKED_AS_COMPLETE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_core`
--

LOCK TABLES `form4b_infra_completion_report_core` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_core` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_core` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture1_blb`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture1_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture1_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture1_blb`
--

LOCK TABLES `form4b_infra_completion_report_picture1_blb` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture1_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture1_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture1_bn`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture1_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture1_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture1_bn`
--

LOCK TABLES `form4b_infra_completion_report_picture1_bn` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture1_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture1_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture1_ref`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture1_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture1_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture1_ref`
--

LOCK TABLES `form4b_infra_completion_report_picture1_ref` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture1_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture1_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture2_blb`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture2_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture2_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture2_blb`
--

LOCK TABLES `form4b_infra_completion_report_picture2_blb` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture2_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture2_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture2_bn`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture2_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture2_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture2_bn`
--

LOCK TABLES `form4b_infra_completion_report_picture2_bn` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture2_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture2_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture2_ref`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture2_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture2_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture2_ref`
--

LOCK TABLES `form4b_infra_completion_report_picture2_ref` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture2_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture2_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture3_blb`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture3_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture3_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture3_blb`
--

LOCK TABLES `form4b_infra_completion_report_picture3_blb` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture3_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture3_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture3_bn`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture3_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture3_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture3_bn`
--

LOCK TABLES `form4b_infra_completion_report_picture3_bn` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture3_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture3_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture3_ref`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture3_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture3_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture3_ref`
--

LOCK TABLES `form4b_infra_completion_report_picture3_ref` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture3_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture3_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture4_blb`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture4_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture4_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture4_blb`
--

LOCK TABLES `form4b_infra_completion_report_picture4_blb` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture4_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture4_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture4_bn`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture4_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture4_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture4_bn`
--

LOCK TABLES `form4b_infra_completion_report_picture4_bn` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture4_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture4_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture4_ref`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture4_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture4_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture4_ref`
--

LOCK TABLES `form4b_infra_completion_report_picture4_ref` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture4_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture4_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture5_blb`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture5_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture5_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture5_blb`
--

LOCK TABLES `form4b_infra_completion_report_picture5_blb` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture5_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture5_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture5_bn`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture5_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture5_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture5_bn`
--

LOCK TABLES `form4b_infra_completion_report_picture5_bn` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture5_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture5_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form4b_infra_completion_report_picture5_ref`
--

DROP TABLE IF EXISTS `form4b_infra_completion_report_picture5_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form4b_infra_completion_report_picture5_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form4b_infra_completion_report_picture5_ref`
--

LOCK TABLES `form4b_infra_completion_report_picture5_ref` WRITE;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture5_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form4b_infra_completion_report_picture5_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form5_25eport_beneficiary_beneficiary_photo_blb`
--

DROP TABLE IF EXISTS `form5_25eport_beneficiary_beneficiary_photo_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form5_25eport_beneficiary_beneficiary_photo_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form5_25eport_beneficiary_beneficiary_photo_blb`
--

LOCK TABLES `form5_25eport_beneficiary_beneficiary_photo_blb` WRITE;
/*!40000 ALTER TABLE `form5_25eport_beneficiary_beneficiary_photo_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form5_25eport_beneficiary_beneficiary_photo_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form5_25eport_beneficiary_beneficiary_photo_bn`
--

DROP TABLE IF EXISTS `form5_25eport_beneficiary_beneficiary_photo_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form5_25eport_beneficiary_beneficiary_photo_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form5_25eport_beneficiary_beneficiary_photo_bn`
--

LOCK TABLES `form5_25eport_beneficiary_beneficiary_photo_bn` WRITE;
/*!40000 ALTER TABLE `form5_25eport_beneficiary_beneficiary_photo_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form5_25eport_beneficiary_beneficiary_photo_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form5_25eport_beneficiary_beneficiary_photo_ref`
--

DROP TABLE IF EXISTS `form5_25eport_beneficiary_beneficiary_photo_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form5_25eport_beneficiary_beneficiary_photo_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form5_25eport_beneficiary_beneficiary_photo_ref`
--

LOCK TABLES `form5_25eport_beneficiary_beneficiary_photo_ref` WRITE;
/*!40000 ALTER TABLE `form5_25eport_beneficiary_beneficiary_photo_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form5_25eport_beneficiary_beneficiary_photo_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form5_25eport_core`
--

DROP TABLE IF EXISTS `form5_25eport_core`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form5_25eport_core` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_MODEL_VERSION` int(9) DEFAULT NULL,
  `_UI_VERSION` int(9) DEFAULT NULL,
  `_IS_COMPLETE` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `_MARKED_AS_COMPLETE_DATE` datetime(6) DEFAULT NULL,
  `INTERVENTION_DETAILS_TRAINING_INSTITUTION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_LNG` decimal(38,10) DEFAULT NULL,
  `INTERVENTION_DETAILS_INSTITUTE_TYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_ALT` decimal(38,10) DEFAULT NULL,
  `REMARKS` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_LAT` decimal(38,10) DEFAULT NULL,
  `INTERVENTION_DETAILS_TRAINING_DURATION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `UC` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_ID` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BENEFICIARY_AGE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DISTRICT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVENTION_DETAILS_COURSE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVENTION_DETAILS_INSTITUTE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WARD` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BENEFICIARY_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEHSIL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORT_COMPLETE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MUHALLA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OUTCOME` int(9) DEFAULT NULL,
  `OUTPUT` int(9) DEFAULT NULL,
  `INTERVENTION_TYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PROJECT_CODE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `IMEI` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STREET` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORTINGDATE` datetime(6) DEFAULT NULL,
  `LOCATION_ACC` decimal(38,10) DEFAULT NULL,
  `DATE` datetime(6) DEFAULT NULL,
  `STARTTIME` datetime(6) DEFAULT NULL,
  `BENEFICIARY_SEX` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FACILITATOR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVIEW_END_TIME` datetime(6) DEFAULT NULL,
  `ACTIVITY` int(9) DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_MARKED_AS_COMPLETE_DATE` (`_MARKED_AS_COMPLETE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form5_25eport_core`
--

LOCK TABLES `form5_25eport_core` WRITE;
/*!40000 ALTER TABLE `form5_25eport_core` DISABLE KEYS */;
/*!40000 ALTER TABLE `form5_25eport_core` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_core`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_core`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_core` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_MODEL_VERSION` int(9) DEFAULT NULL,
  `_UI_VERSION` int(9) DEFAULT NULL,
  `_IS_COMPLETE` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `_MARKED_AS_COMPLETE_DATE` datetime(6) DEFAULT NULL,
  `THEMES_COVERED` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_LNG` decimal(38,10) DEFAULT NULL,
  `SENIOR_PERSON_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_ALT` decimal(38,10) DEFAULT NULL,
  `INFLUENCE_LEVEL` int(9) DEFAULT NULL,
  `LOCATION_LAT` decimal(38,10) DEFAULT NULL,
  `SENIOR_PERSON_DEPARTMENT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_ID` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ALIGNMENT_LEVEL` int(9) DEFAULT NULL,
  `WAY_FORWARD` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORT_COMPLETE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ACTION_COMITTED` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SENIOR_PERSON_DESIGNATION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OUTCOME` int(9) DEFAULT NULL,
  `OUTPUT` int(9) DEFAULT NULL,
  `OBSERVATIONS` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PROJECT_CODE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `IMEI` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PROGRAM_OTHER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MEETING_PLACE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORTINGDATE` datetime(6) DEFAULT NULL,
  `LOCATION_ACC` decimal(38,10) DEFAULT NULL,
  `DATE` datetime(6) DEFAULT NULL,
  `STARTTIME` datetime(6) DEFAULT NULL,
  `MEETING_OBJECTIVE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OTHER_PARTICIPANTS` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVIEW_END_TIME` datetime(6) DEFAULT NULL,
  `UNDERSTANDING_LEVEL` int(9) DEFAULT NULL,
  `ACTIVITY` int(9) DEFAULT NULL,
  `PROGRAM` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_MARKED_AS_COMPLETE_DATE` (`_MARKED_AS_COMPLETE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_core`
--

LOCK TABLES `form6_lobby_meeting_report_core` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_core` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_core` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_1_blb`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_1_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_1_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_1_blb`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_1_blb` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_1_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_1_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_1_bn`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_1_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_1_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_1_bn`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_1_bn` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_1_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_1_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_1_ref`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_1_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_1_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_1_ref`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_1_ref` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_1_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_1_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_2_blb`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_2_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_2_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_2_blb`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_2_blb` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_2_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_2_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_2_bn`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_2_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_2_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_2_bn`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_2_bn` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_2_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_2_bn` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-07  9:36:27
