-- MySQL dump 10.13  Distrib 5.7.21, for Win64 (x86_64)
--
-- Host: localhost    Database: logixsol_odk
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_2_ref`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_2_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_2_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_2_ref`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_2_ref` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_2_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_2_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_3_blb`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_3_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_3_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_3_blb`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_3_blb` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_3_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_3_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_3_bn`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_3_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_3_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_3_bn`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_3_bn` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_3_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_3_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form6_lobby_meeting_report_meeting_picture_3_ref`
--

DROP TABLE IF EXISTS `form6_lobby_meeting_report_meeting_picture_3_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form6_lobby_meeting_report_meeting_picture_3_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form6_lobby_meeting_report_meeting_picture_3_ref`
--

LOCK TABLES `form6_lobby_meeting_report_meeting_picture_3_ref` WRITE;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_3_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form6_lobby_meeting_report_meeting_picture_3_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_core`
--

DROP TABLE IF EXISTS `form7_success_story_core`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_core` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_MODEL_VERSION` int(9) DEFAULT NULL,
  `_UI_VERSION` int(9) DEFAULT NULL,
  `_IS_COMPLETE` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `_MARKED_AS_COMPLETE_DATE` datetime(6) DEFAULT NULL,
  `LOCATION_LNG` decimal(38,10) DEFAULT NULL,
  `LOCATION_ALT` decimal(38,10) DEFAULT NULL,
  `LOCATION_LAT` decimal(38,10) DEFAULT NULL,
  `UC` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_ID` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DISTRICT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WARD` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEHSIL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `KEY_WORDS` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORT_COMPLETE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MUHALLA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OUTCOME` int(9) DEFAULT NULL,
  `OUTPUT` int(9) DEFAULT NULL,
  `PROJECT_CODE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `IMEI` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STREET` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REPORTINGDATE` datetime(6) DEFAULT NULL,
  `NARRATION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_ACC` decimal(38,10) DEFAULT NULL,
  `DATE` datetime(6) DEFAULT NULL,
  `STARTTIME` datetime(6) DEFAULT NULL,
  `TITLE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVIEW_END_TIME` datetime(6) DEFAULT NULL,
  `ACTIVITY` int(9) DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_MARKED_AS_COMPLETE_DATE` (`_MARKED_AS_COMPLETE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_core`
--

LOCK TABLES `form7_success_story_core` WRITE;
/*!40000 ALTER TABLE `form7_success_story_core` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_core` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_1_blb`
--

DROP TABLE IF EXISTS `form7_success_story_picture_1_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_1_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_1_blb`
--

LOCK TABLES `form7_success_story_picture_1_blb` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_1_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_1_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_1_bn`
--

DROP TABLE IF EXISTS `form7_success_story_picture_1_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_1_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_1_bn`
--

LOCK TABLES `form7_success_story_picture_1_bn` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_1_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_1_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_1_ref`
--

DROP TABLE IF EXISTS `form7_success_story_picture_1_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_1_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_1_ref`
--

LOCK TABLES `form7_success_story_picture_1_ref` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_1_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_1_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_2_blb`
--

DROP TABLE IF EXISTS `form7_success_story_picture_2_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_2_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_2_blb`
--

LOCK TABLES `form7_success_story_picture_2_blb` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_2_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_2_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_2_bn`
--

DROP TABLE IF EXISTS `form7_success_story_picture_2_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_2_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_2_bn`
--

LOCK TABLES `form7_success_story_picture_2_bn` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_2_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_2_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_2_ref`
--

DROP TABLE IF EXISTS `form7_success_story_picture_2_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_2_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_2_ref`
--

LOCK TABLES `form7_success_story_picture_2_ref` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_2_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_2_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_3_blb`
--

DROP TABLE IF EXISTS `form7_success_story_picture_3_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_3_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_3_blb`
--

LOCK TABLES `form7_success_story_picture_3_blb` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_3_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_3_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_3_bn`
--

DROP TABLE IF EXISTS `form7_success_story_picture_3_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_3_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_3_bn`
--

LOCK TABLES `form7_success_story_picture_3_bn` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_3_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_3_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form7_success_story_picture_3_ref`
--

DROP TABLE IF EXISTS `form7_success_story_picture_3_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form7_success_story_picture_3_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form7_success_story_picture_3_ref`
--

LOCK TABLES `form7_success_story_picture_3_ref` WRITE;
/*!40000 ALTER TABLE `form7_success_story_picture_3_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form7_success_story_picture_3_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form8_monitoring_report_core`
--

DROP TABLE IF EXISTS `form8_monitoring_report_core`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form8_monitoring_report_core` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_MODEL_VERSION` int(9) DEFAULT NULL,
  `_UI_VERSION` int(9) DEFAULT NULL,
  `_IS_COMPLETE` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `_MARKED_AS_COMPLETE_DATE` datetime(6) DEFAULT NULL,
  `UPIDP_DEVELOPED` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `COMMUNTY_UNDERSTANDING_ABOUT_PROJECT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ATTENDED_TRAINING` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_TYPE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_MEMBER_DESIGNATION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_MEMBER_AGE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REMARKS` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ELECTION_HELD` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TRAINING_IMPACT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `UC` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_MEMBER_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_MEMBER_CONTACT_NO` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ISSUE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MEETING_FREQUENCY_SOCIAL_MOBILIZER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DATE_VISIT` datetime(6) DEFAULT NULL,
  `APPLICATION_TO_DEPARTMENT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SURVEY_COMPLETE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PARTICIPATION_LOCAL_GOVERNANCE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEHSIL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `META_INSTANCE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DEPARTMENT_RESPONSE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DOUBLE_FAMILY_MEMBERS_GROUP` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OUTPUT` int(9) DEFAULT NULL,
  `PROJECT_CODE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TRAINING_SUBJECT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PROCEEDINGS_RECORDED` int(9) DEFAULT NULL,
  `DATE` datetime(6) DEFAULT NULL,
  `STARTTIME` datetime(6) DEFAULT NULL,
  `MONITORING_OFFICER_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FACILITATOR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_MEMBER_POSITION_ASSIGNED` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_MEMBER_DEPARTMENT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `INTERVIEW_END_TIME` datetime(6) DEFAULT NULL,
  `LOCATION_LNG` decimal(38,10) DEFAULT NULL,
  `LOCATION_ALT` decimal(38,10) DEFAULT NULL,
  `MEETING_FREQUENCY_ECG` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_LAT` decimal(38,10) DEFAULT NULL,
  `PRA_TOOL_APPLIED` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `APPLICATION_TO_SERVICE_PROVIDER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `APPLICATION_CONSIDERATION_TIME` int(9) DEFAULT NULL,
  `META_INSTANCE_ID` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `DISTRICT` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WARD` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MUHALLA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OUTCOME` int(9) DEFAULT NULL,
  `IMEI` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STREET` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TRAINING_PRACTICED` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LOCATION_ACC` decimal(38,10) DEFAULT NULL,
  `GROUP_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TRAINING_PRACTICED_HOW` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_MEMBER_GENDER` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PROCEEDING_REGISTER_COMPLETE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CNIC_SUBMITTED` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ACTIVITY` int(9) DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_MARKED_AS_COMPLETE_DATE` (`_MARKED_AS_COMPLETE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form8_monitoring_report_core`
--

LOCK TABLES `form8_monitoring_report_core` WRITE;
/*!40000 ALTER TABLE `form8_monitoring_report_core` DISABLE KEYS */;
/*!40000 ALTER TABLE `form8_monitoring_report_core` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form8_monitoring_report_picture_blb`
--

DROP TABLE IF EXISTS `form8_monitoring_report_picture_blb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form8_monitoring_report_picture_blb` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `VALUE` mediumblob NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form8_monitoring_report_picture_blb`
--

LOCK TABLES `form8_monitoring_report_picture_blb` WRITE;
/*!40000 ALTER TABLE `form8_monitoring_report_picture_blb` DISABLE KEYS */;
/*!40000 ALTER TABLE `form8_monitoring_report_picture_blb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form8_monitoring_report_picture_bn`
--

DROP TABLE IF EXISTS `form8_monitoring_report_picture_bn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form8_monitoring_report_picture_bn` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_PARENT_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_ORDINAL_NUMBER` int(9) NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `UNROOTED_FILE_PATH` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_TYPE` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `CONTENT_LENGTH` int(9) DEFAULT NULL,
  `CONTENT_HASH` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_PARENT_AURI` (`_PARENT_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form8_monitoring_report_picture_bn`
--

LOCK TABLES `form8_monitoring_report_picture_bn` WRITE;
/*!40000 ALTER TABLE `form8_monitoring_report_picture_bn` DISABLE KEYS */;
/*!40000 ALTER TABLE `form8_monitoring_report_picture_bn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form8_monitoring_report_picture_ref`
--

DROP TABLE IF EXISTS `form8_monitoring_report_picture_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form8_monitoring_report_picture_ref` (
  `_URI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATOR_URI_USER` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_CREATION_DATE` datetime(6) NOT NULL,
  `_LAST_UPDATE_URI_USER` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `_LAST_UPDATE_DATE` datetime(6) NOT NULL,
  `_DOM_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_SUB_AURI` varchar(80) CHARACTER SET utf8 NOT NULL,
  `_TOP_LEVEL_AURI` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `PART` int(9) NOT NULL,
  KEY `_URI` (`_URI`) USING HASH,
  KEY `_LAST_UPDATE_DATE` (`_LAST_UPDATE_DATE`),
  KEY `_DOM_AURI` (`_DOM_AURI`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form8_monitoring_report_picture_ref`
--

LOCK TABLES `form8_monitoring_report_picture_ref` WRITE;
/*!40000 ALTER TABLE `form8_monitoring_report_picture_ref` DISABLE KEYS */;
/*!40000 ALTER TABLE `form8_monitoring_report_picture_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_columns`
--

DROP TABLE IF EXISTS `report_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formcolumn` varchar(200) NOT NULL,
  `reportid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_columns`
--

LOCK TABLES `report_columns` WRITE;
/*!40000 ALTER TABLE `report_columns` DISABLE KEYS */;
INSERT INTO `report_columns` VALUES (34,'date',1),(35,'outcome',1),(36,'district',1),(37,'muhalla',1),(38,'street',1),(39,'picture',1),(40,'survey_complete',1),(41,'interview_end_time',1),(42,'date',2),(43,'outcome',2),(44,'group_type',2),(45,'outcome',3),(46,'tehsil',3),(47,'issues_solutions',3),(48,'report_complete',3);
/*!40000 ALTER TABLE `report_columns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportname` varchar(200) NOT NULL DEFAULT '100',
  `user_id` int(11) NOT NULL,
  `adddate` datetime NOT NULL,
  `formid` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` VALUES (1,'Report name 1',1,'2018-04-10 12:21:15','form1_GroupFormationReport031318'),(2,'Report name',1,'2018-04-10 09:43:52','form1_GroupFormationReport031318'),(3,'Report 2',1,'2018-04-11 04:38:30','form2_UPDIPFormationReport032318');
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-07  9:53:52
