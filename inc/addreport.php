<h1 class="page-Title">
	Add Report
</h1>

<form class="form" method="POST" action="<?= appcontrol_url("appcontrol-formaddreport") ?>">

<h4>Report name: <?= $report["reportname"] ?></h4>
<h4>
	Form: 
	<select id="formfieldselect" name="formid">
		<?php foreach ($forms as $form): ?>
		<option value="<?= $form["formid"] ?>">
			<?= $form["title"] ?>
		</option>
		<?php endforeach ?>
	</select>
</h4>

<h4>
	Report name: 
	<input type="text" name="reportname" />
</h4>

<h6>Select Columns below you want to show in Reports: </h6>

<div class="col-md-12">

		<div id="formfields">
			
			

		</div>

		<div class="clearfix"></div>

		<input class="btn btn-primary pull-right" type="submit" value="Add" />

</div>

</form>