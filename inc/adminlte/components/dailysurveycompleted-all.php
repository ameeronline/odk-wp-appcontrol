<?php $forms = appcontrol_getforms(); ?>

<div class="row">
	
	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">Daily Completed Surveys</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<canvas id="barChart" style="height: 280px;"></canvas>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>

	<?php foreach ($forms as $k => $form): ?>

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title"><?= $form["title"] ?></h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<canvas id="barChart<?= $k ?>" style="height: 280px;"></canvas>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>

	<script type="text/javascript">
		
		jQuery(document).ready(function() {

			<?php $daily_surveys = []; ?>
			<?php $daily_surveys_label = []; ?>

			<?php

				for($i = 0;$i < cal_days_in_month(CAL_GREGORIAN, intval(date("m")), intval(date("Y")) ); $i++) {
					$daily_surveys_label[] = $i+1;
				}

				$daily_surveys = appcontrol_dailycompletedsurvey( $form["formid"] );

			?>

			var barChartCanvas<?= $k ?> = jQuery('#barChart<?= $k ?>').get(0).getContext('2d');
			var barChart<?= $k ?> = new Chart(barChartCanvas<?= $k ?>);

			var areaChartData = {
				labels: <?= json_encode( $daily_surveys_label ) ?>,
				datasets: [
					{
						label: 'Daily Completed Surveys',
						fillColor: '#00C0EF',
						strokeColor: '#00C0EF',
						pointColor: '#00C0EF',
						pointStrokeColor: '#c1c7d1',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(220,220,220,1)',
						data: <?= json_encode($daily_surveys) ?>
					}
				]
			};

			var barChartData = areaChartData;
			
			var barChartOptions = {
				//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
				scaleBeginAtZero: true,
				//Boolean - Whether grid lines are shown across the chart
				scaleShowGridLines: true,
				//String - Colour of the grid lines
				scaleGridLineColor: 'rgba(0,0,0,.05)',
				//Number - Width of the grid lines
				scaleGridLineWidth: 1,
				//Boolean - Whether to show horizontal lines (except X axis)
				scaleShowHorizontalLines: true,
				//Boolean - Whether to show vertical lines (except Y axis)
				scaleShowVerticalLines: true,
				//Boolean - If there is a stroke on each bar
				barShowStroke: true,
				//Number - Pixel width of the bar stroke
				barStrokeWidth: 2,
				//Number - Spacing between each of the X value sets
				barValueSpacing: 5,
				//Number - Spacing between data sets within X values
				barDatasetSpacing: 1,
				//String - A legend template
				legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
				//Boolean - whether to make the chart responsive
				responsive: true,
				maintainAspectRatio: true
			};

			barChart<?= $k ?>.Bar(barChartData, barChartOptions);

		});

	</script>
		
	<?php endforeach ?>

</div>

<script type="text/javascript">

jQuery(document).ready(function() {

	<?php $daily_surveys = []; ?>
	<?php $daily_surveys_label = []; ?>

	<?php

		for($i = 0;$i < cal_days_in_month(CAL_GREGORIAN, intval(date("m")), intval(date("Y")) ); $i++) {
			$daily_surveys_label[] = $i+1;
		}

		$daily_surveys = appcontrol_alldailycompletedsurvey();

	?>

	var barChartCanvas = jQuery('#barChart').get(0).getContext('2d');

	var areaChartData = {
		labels: <?= json_encode( $daily_surveys_label ) ?>,
		datasets: [{
				label: 'Daily Completed Surveys',
				fillColor: '#00C0EF',
				strokeColor: '#00C0EF',
				pointColor: '#00C0EF',
				pointStrokeColor: '#c1c7d1',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: <?= json_encode($daily_surveys) ?>
			}
		]
	};

	var barChart = new Chart(barChartCanvas);

	var barChartData = areaChartData;
	
	var barChartOptions = {
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		scaleBeginAtZero: true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines: true,
		//String - Colour of the grid lines
		scaleGridLineColor: 'rgba(0,0,0,.05)',
		//Number - Width of the grid lines
		scaleGridLineWidth: 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,
		//Boolean - If there is a stroke on each bar
		barShowStroke: true,
		//Number - Pixel width of the bar stroke
		barStrokeWidth: 2,
		//Number - Spacing between each of the X value sets
		barValueSpacing: 5,
		//Number - Spacing between data sets within X values
		barDatasetSpacing: 1,
		//String - A legend template
		legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
		//Boolean - whether to make the chart responsive
		responsive: true,
		maintainAspectRatio: true
	}

	barChartOptions.datasetFill = false;
	barChart.Bar(barChartData, barChartOptions);

});

</script>