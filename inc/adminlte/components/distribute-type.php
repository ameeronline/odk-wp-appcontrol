<div class="col-md-6">
	<div class="box">
		<div class="box-header with-border">
			<a href="report-activities_type.php">
				<h3 class="box-title">Distribution of Activities By Type</h3>
			</a>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="height: 345px;">
			<div class="row">
				<div class="col-md-8">
					<div class="chart" style="height: 300px;">
						<!-- Sales Chart Canvas -->
						<canvas id="pieChart" style="height: 300px;"></canvas>
					</div>
					<!-- /.chart-responsive -->
				</div>
				<div class="col-md-4">
					<ul class="chart-legend clearfix">
						<?php $gc = 0; ?>
						
						<?php global $grouptypedata; ?>
						<?php global $colorarray; ?>

						<?php foreach ( $grouptypedata as $grouptypename => $grouptypevalue ): ?>
						<li><i class="fa fa-circle-o" style="color: <?= $colorarray[$gc] ?>"></i> <?= $grouptypename ?></li>
						<?php $gc++; ?>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- ./box-body -->
	</div>
	<!-- /.box -->
</div>
<script>
	var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
	pieChartCanvas.height = 300;
	pieChartCanvas.width = 300;
	var pieChart = new Chart(pieChartCanvas);
	var PieData = [{
					value: 40,
					color: "#f56954",
					highlight: "#f56954",
					label: "Recreational"
			},
			{
					value: 200,
					color: "#00a65a",
					highlight: "#00a65a",
					label: "Awareness"
			},
			{
					value: 600,
					color: "#f39c12",
					highlight: "#f39c12",
					label: "Training"
			},
			{
					value: 50,
					color: "#00c0ef",
					highlight: "#00c0ef",
					label: "Social Mobilization"
			}
	];
	var pieOptions = {
			//Boolean - Whether we should show a stroke on each segment
			segmentShowStroke: true,
			//String - The colour of each segment stroke
			segmentStrokeColor: "#fff",
			//Number - The width of each segment stroke
			segmentStrokeWidth: 1,
			//Number - The percentage of the chart that we cut out of the middle
			percentageInnerCutout: 50, // This is 0 for Pie charts
			//Number - Amount of animation steps
			animationSteps: 100,
			//String - Animation easing effect
			animationEasing: "easeOutBounce",
			//Boolean - Whether we animate the rotation of the Doughnut
			animateRotate: true,
			//Boolean - Whether we animate scaling the Doughnut from the centre
			animateScale: false,
			//Boolean - whether to make the chart responsive to window resizing
			responsive: true,
			// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			maintainAspectRatio: false,
			//String - A legend template
			legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
			//String - A tooltip template
			tooltipTemplate: "<%=value %> <%=label%>"
	};
	//Create pie or douhnut chart
	// You can switch between pie and douhnut using the method below.
	pieChart.Doughnut(PieData, pieOptions);
</script>