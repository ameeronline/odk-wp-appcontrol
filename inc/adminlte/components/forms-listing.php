<?php //$form = getform_by_id( "form1_GroupFormationReport031318" ); ?>
<?php //$formdata = appcontrol_get_formdata( "form1_GroupFormationReport031318" ); ?>
<?php

	$forms = appcontrol_getforms2();

	global $sheet_ids;
	
?>

<div class="row">
	<div class="col-md-12">
		
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">Form Entities View in Tabular form</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<table class="table table-bordered">
					<thead>
						<tr>
							<th width="40%">
								Name
							</th>
							<th width="15%">
								Submissions
							</th>
							<th width="10%">
								View
							</th>
							<th width="16%">
								Data
							</th>
							<th>
								Last Submission
							</th>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($forms as $k => $form): ?>

						<tr>

							<td>
								<a href="<?= site_url("/appcontrol/formdata/". $form["formid"]) ?>">
									<?php echo $form["title"] ?>
								</a>
							</td>
							<td>
								<?php echo appcontrol_formsubmissions($form["formid"]) ?>
							</td>
							<td>
								<a href="<?= site_url("/appcontrol/formmapdata/". $form["formid"]) ?>" class="anchor-icon">
									<img src="<?= adminlte_assets("images/pin-icon.png") ?>" />
								</a>
								<a href="<?= site_url("/appcontrol/formdata/". $form["formid"]) ?>" class="anchor-icon">
									<img src="<?= adminlte_assets("images/search-icon.png") ?>" />
								</a>
							</td>
							<td>
								<div class="achor-icon-wrapper">
									<a href="#" class="anchor-icon">
										<img src="<?= adminlte_assets("images/clouddown.png") ?>" />
									</a>
									<p class="achor-icon-label">
										Download <br />
										<a href="<?php echo site_url("appcontrol/exportform/". $form["formid"]) ?>">CSV</a>
										<a href="<?php echo site_url("appcontrol/exportform/". $form["formid"]) ?>">XSL</a>
										<a href="#">KML</a>
									</p>
								</div>
							</td>
							<td>
								<?php $lastsubmission = appcontrol_lastsubmission( $form["formid"] ); ?>
								<?php if ( $lastsubmission ): ?>
									<?php echo $lastsubmission; ?>
								<?php else: ?>
									No Submission yet
								<?php endif ?>
							</td>
							
						</tr>

						<?php endforeach ?>
						
						<?php if (count($forms) == 0): ?>
						
						<tr>
							<td colspan="<?= count($form["formfields"]) ?>">
								<center>
									No records found
								</center>
							</td>
						</tr>

						<?php endif ?>

					</tbody>
				</table>

			</div>
		</div>

	</div>
</div>