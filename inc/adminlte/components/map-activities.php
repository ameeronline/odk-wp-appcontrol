<div class="col-md-12">
	<!-- MAP & BOX PANE -->
	<div class="box box-success">
		<div class="box-header with-border">
			<a href="<?= site_url("appcontrol/mapactivities") ?>">
				<h3 class="box-title">Map of Activities</h3>
			</a>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
				<!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="pad">
						<!-- Map will be created here -->
						<div id="map-activities" style="height: 650px;"></div>
					</div>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			<div class="row">
				<!-- <div class="col-md-12 col-sm-12" align="center">
					<a href="#map-bara" class="btn btn-sm btn-success" onClick="reloadMarkers(locBara, 'Bara');">Bara</a>
					<a href="#map-bannu" class="btn btn-sm btn-success" onClick="reloadMarkers(locBannu, 'Bannu');">Bannu</a>
				</div> -->
			</div>
		</div>
	</div>
</div>
<script>

	<?php

		$locationdata = appcontrol_getonlylocationdata();

		$_locationdata = [];

		foreach ($locationdata as $location) {
			$_locationdata[] = [
				"",
				$location["LOCATION_LAT"],
				$location["LOCATION_LNG"],
				$location["ACTIVITY"],
				appcontrol_colorize($location["ACTIVITY"])
			];
		}

		?>
	//var locations = locBannu.concat(locBara);

	var locations = <?= json_encode($_locationdata) ?>;

	var icon = {
		url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|ffef00", // url
	};
	
	var map;
	var markers = [];
	var bounds = [];
	
	function setMarkers(loc, district) {
		bounds = new google.maps.LatLngBounds();
		
		if (district && loc.length == 0) {
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({'address': district + ' Pakistan'}, function(results, status) {
				if (status === 'OK') {
					map.setCenter(results[0].geometry.location);
					for (i = 0; i < loc.length; i++) { 
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(loc[i][1], loc[i][2]),
							map: map,
							title: loc[i][0],
							icon: icon
						});
	
						bounds.extend(marker.position);
						markers.push(marker);
					}
				} else {
	
				}
			});
		}
		else {
			for (var i = 0; i < loc.length; i++) {
				
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(loc[i][1], loc[i][2]),
					map: map,
					title: loc[i][0],
					//Icon Customized with colorize
					icon: {
						url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|"+loc[i][4], // url
					}
				});

				var ii = i;
				(function(ii,loc){
					var infowindow = new google.maps.InfoWindow({
						content: "<b>Activitiy:</b> " + loc[ii][3]
					});
					marker.addListener("mouseover", function() {
						infowindow.open(map, markers[ii]);
					});
					marker.addListener("mouseout", function() {
						infowindow.close();
					});
				})(ii, loc);
	
				bounds.extend(marker.position);
				markers.push(marker);

			}
		}
	}
	
	function reloadMarkers(loc, district) {
		for (var i=0; i<markers.length; i++) {
			markers[i].setMap(null);
		}
	
		markers = [];
		setMarkers(loc, district);
		if (loc.length > 0) {
			map.fitBounds(bounds);
		}
	}
	
	function initMap() {
		map = new google.maps.Map(document.getElementById('map-activities'), {
			zoom: 8
		});
	
		setMarkers(locations);
		map.fitBounds(bounds);
	}
	
	function resizeMap() {
			var myMap = document.getElementById('map-activities');
			myMap.classList.toggle("fullscreen");
	}
	
</script>