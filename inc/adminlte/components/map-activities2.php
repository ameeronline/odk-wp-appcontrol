<?php $locationdata = appcontrol_getonlylocationdata(); ?>
<?php $activity_code = getpostvar("activity_code") ?>
<div class="col-md-12">
	<!-- MAP & BOX PANE -->
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Map of Activities</h3>
			<div class="box-tools pull-right">
				<!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
			</div>
			<div class="row" style="padding-top:10px;">
			<div class="col-md-12 col-lg-12">
				<div style="display: inline-block;float: left;padding-left: 5px;"><h4 for="selecttag">Filter Activity&nbsp;&nbsp;&nbsp;&nbsp;</h4></div>
				<div style="display: inline-block;float: left;width: 150px;">
					<form id="f1" method="POST">
						<div class="form-group">
							<select id="selecttag" name="activity_code" class="form-control" onchange="jQuery('#f1').get(0).submit()">
								<option value="">
									-- Select Activity --
								</option>
								<?php $activities = array(); ?>
								<?php foreach ($locationdata as $location) { $activities[] = $location["ACTIVITY"]; } $activities = array_unique($activities); ?>
								<?php foreach ($activities as $activity): ?>
								<option value="<?= $activity ?>" <?= $activity_code == $activity ? "selected" : "" ?>>
									<?= $activity ?>
								</option>
								<?php endforeach ?>
							</select>
						</div>
					</form>
				</div>
			</div>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="pad">
						<!-- Map will be created here -->
						<div id="map-activities" style="height: 650px;"></div>
					</div>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			<div class="row">
				<div class="col-md-12 col-sm-12" align="center">
					<!-- <a href="#map-bara" class="btn btn-sm btn-success" onClick="reloadMarkers(locBara, 'Bara');">Bara</a>
					<a href="#map-bannu" class="btn btn-sm btn-success" onClick="reloadMarkers(locBannu, 'Bannu');">Bannu</a> -->
				</div>
			</div>
		</div>
	</div>
</div>
<script>

	<?php
		ob_start();
	?>{"Bara":[["'Sajid Kilay'","33.93780000","71.37670000"],["'Said Rasool Kilay'","33.93910000","71.38620000"],["'Sahafi Mehboob Shah Kilay'","33.93270000","71.38120000"],["'Jabir Karaty club'","33.93130000","71.37670000"],["'Miagano Kilay'","33.85790000","71.19390000"],["'People Garhi'","33.89720000","71.43110000"],["'Jargo Khwar'","33.92630000","71.35800000"],["'Zulfiqar Garhi'","33.90320000","71.44090000"],["'Sandali Kali'","33.89470000","71.42050000"],["'Malang Gharhi'","33.81940000","70.57330000"],["'Toth Dhand'","33.94080000","71.36110000"],["'Tauheed Abad'","33.92580000","71.42640000"],["'Yousaf Talab'","33.89000000","71.37110000"],["'Chargai Dagarri'","33.90970000","71.26550000"],["'Surkas No1'","33.91750000","71.40590000"],["'Haneef Kilay'","33.93500000","71.38630000"],["'Malik Garhi'","33.89940000","71.43360000"],["'Karigar Garhi'","33.87470000","70.54750000"],["'Wali Abad Kilay'","33.93620000","71.37150000"],["'Sur Dhand'","33.90120000","71.31940000"],["'Sawat Abad'","33.91870000","71.44000000"],["'Malakanano Kilay'","33.92930000","71.38920000"],["'Khanby Khel Kilay'","33.93700000","71.35790000"],["'Karawal'","33.91210000","71.34600000"],["'Malang Garhi'","33.89620000","71.42730000"],["'Dora Kili'","33.80720000","70.78980000"],["'Gulabad'","33.91760000","71.43350000"],["'Dr Kifayat Shah Kilay'","33.92850000","71.34350000"],["'Sarki Kamar'","33.93490000","71.29100000"],["'Shanko\/ Shinki Kamar'","33.94520000","71.32130000"],["'Laiq Thikidar Kilay'","33.93760000","71.36270000"],["'Kagokay'","33.94710000","71.32610000"],["'Jhansi Fort'","33.87250000","71.39580000"]],"Bannu":[["'Jango kala'","32.90400000","70.72280000"]]}<?php
		$ob_json = ob_get_clean();

		$dataMap = json_decode( $ob_json );
		$arrData = array();
		
		foreach($dataMap as $district => $row){
			foreach($row as $row2) {
				$arrData[$district][] = '[' . implode(',', $row2) . ']';
			}
		}

		$_locationdata = [];

		foreach ($locationdata as $location) {
			
			if($activity_code != "" && $activity_code != $location["ACTIVITY"] )
				continue;

			$_locationdata[] = [
				"",
				$location["LOCATION_LAT"],
				$location["LOCATION_LNG"],
				$location["ACTIVITY"],
				appcontrol_colorize($location["ACTIVITY"])
			];
		}


		?>
	var locBannu = [<?php echo implode(',', $arrData['Bannu']);?>];
	var locBara = [<?php echo implode(',', $arrData['Bara']);?>];

	var locations = <?= json_encode($_locationdata) ?>;
	
	var icon = {
		url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|ffef00", // url
	};
	
	var map;
	var markers = [];
	var bounds = [];
	
	function setMarkers(loc, district) {
		var marker, i;
		bounds = new google.maps.LatLngBounds();
		
		if (district && loc.length == 0) {
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({'address': district + ' Pakistan'}, function(results, status) {
				if (status === 'OK') {
					map.setCenter(results[0].geometry.location);
					for (i = 0; i < loc.length; i++) { 
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(loc[i][1], loc[i][2]),
							map: map,
							title: loc[i][0],
							icon: icon
						});
	
						bounds.extend(marker.position);
						markers.push(marker);
					}
				} else {
	
				}
			});
		}
		else {
			for (i = 0; i < loc.length; i++) { 
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(loc[i][1], loc[i][2]),
					map: map,
					title: loc[i][0],
					//Icon Customized with colorize
					icon: {
						url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|"+loc[i][4], // url
					}
				});
				
				var ii = i;
				(function(ii,loc){
					console.log(loc);
					var infowindow = new google.maps.InfoWindow({
						content: "<b>Activitiy:</b> " + loc[ii][3]
					});
					marker.addListener("mouseover", function() {
						infowindow.open(map, markers[ii]);
					});
					marker.addListener("mouseout", function() {
						infowindow.close();
					});
				})(ii, loc);
	
				bounds.extend(marker.position);
				markers.push(marker);
			}
		}
	}
	
	function reloadMarkers(loc, district) {
		for (var i=0; i<markers.length; i++) {
			markers[i].setMap(null);
		}
	
		markers = [];
		setMarkers(loc, district);
		if (loc.length > 0) {
			map.fitBounds(bounds);
		}
	}
	
	function initMap() {
		map = new google.maps.Map(document.getElementById('map-activities'), {
			zoom: 8
		});
	
		setMarkers(locations);
		map.fitBounds(bounds);
	}
	
	function mapBanu(marker) {
		var i;
	
		marker.setMap(null);
		for (i = 0; i < locBannu.length; i++) { 
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locBannu[i][1], locBannu[i][2]),
				map: map,
				title: locBannu[i][0],
				icon: icon
			});
	
			bounds.extend(marker.position);
		}
	}
	
	function resizeMap() {
			var myMap = document.getElementById('map-activities');
			myMap.classList.toggle("fullscreen");
	}
	
</script>