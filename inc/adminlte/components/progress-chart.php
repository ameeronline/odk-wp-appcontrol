<div class="col-md-6 col-sm-12 col-xs-12">
	<div class="box box-active">
		<div class="box-header with-border">
			<h3 class="box-title">Overall project progress</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
				<!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body pouterbox">
			<p class="text-center" style="position: absolute;transform: rotate(270deg);transform-origin: left;top: 60%;left: 18px">
				<b>
					Percentage (%)
				</b>
			</p>
			<div class="outergraph" style="width: 95%;float: right;">
				<canvas id="projectProgressChart" style="height: 280px;"></canvas>
			</div>
			<!-- /.row -->
			<p class="text-center">
				<b>
					Activity code
				</b>
			</p>
		</div>
		<!-- /.box-body -->
	</div>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {

	<?php

	global $gproject_code;

	$workplans = appcontrol_fetchWorkPlan( $gproject_code );

	$activities = array();

	$progresses = array();

	foreach ($workplans as $workplan) {
		$activities[] = $workplan["activity_number"];
		$progresses[] = $workplan["progress"];
	}

	?>

	var projectProgressChart = jQuery('#projectProgressChart').get(0).getContext('2d');

	var areaChartData = {
		labels: <?= json_encode( $activities ) ?>,
		datasets: [{
				label: 'Overall project progress',
				fillColor: '#00a65a',
				strokeColor: '#00a65a',
				pointColor: '#00a65a',
				pointStrokeColor: '#c1c7d1',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: <?= json_encode( $progresses ) ?>
			}
		],
		options: {
			scales: {
				yAxes: [
					{
						ticks: {
							min: 0,
							max: 100,
							callback: function(value){return value+ "%"}
						},
						scaleLabel: {
							display: true,
							labelString: "Percentage"
						}
					}
				]
			}
		}
	};

	var projectProgressChart = new Chart(projectProgressChart);

	var barChartOptions = {
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		scaleBeginAtZero: true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines: true,
		//String - Colour of the grid lines
		scaleGridLineColor: 'rgba(0,0,0,.05)',
		//Number - Width of the grid lines
		scaleGridLineWidth: 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,
		//Boolean - If there is a stroke on each bar
		barShowStroke: true,
		//Number - Pixel width of the bar stroke
		barStrokeWidth: 2,
		//Number - Spacing between each of the X value sets
		barValueSpacing: 5,
		//Number - Spacing between data sets within X values
		barDatasetSpacing: 1,
		//String - A legend template
		legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
		//Boolean - whether to make the chart responsive
		responsive: true,
		maintainAspectRatio: true
	};

	projectProgressChart.Bar(areaChartData, barChartOptions);

});
</script>