<?php //$ucs = array("Bhuttar Pur" => "Bhuttar Pur","Lutkaran" => "Lutkaran"); ?>

<?php

global $grouptypedata;
global $colorarray;

$colorarray = array("#f56954", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de");
$upidpsdata = appcontrol_allupidpsdata();

?>

<div class="col-md-6">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">UPIDPs Developed</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
				<!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="height: 400px;">
			<div class="row">
				<div class="col-md-8">
					<div class="chart" style="height: 300px;">
						<!-- Sales Chart Canvas -->
						<canvas id="pieChart4" style="height: 300px;"></canvas>
					</div>
					<!-- /.chart-responsive -->
				</div>
				<div class="col-md-4">
					<ul class="chart-legend clearfix">
						<?php
						
						$i = 0;
						foreach ($upidpsdata as $k => $v) {
						
						?>
						<li><i class="fa fa-circle-o" style="color: <?= $colorarray[$i+2] ?>"></i> <?= $k ?></li>
						<?php
						
							$i++;
						
						}

						?>
					</ul>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- ./box-body -->
	</div>
	<!-- /.box -->
</div>
<script>
	jQuery(document).ready(function() {
		var pieChartCanvas = $("#pieChart4").get(0).getContext("2d");
		pieChartCanvas.height = 300;
		pieChartCanvas.width = 300;
		var pieChart = new Chart(pieChartCanvas);

		<?php

		$piedata = array();
		$i = 0;
		foreach ($upidpsdata as $k => $v) {
			$piedata[] = array(
				"value" => $v,
				"color" => $colorarray[$i+2],
				"highlight" => $colorarray[$i+2],
				"label" => $k
			);
			$i++;
		}

		?>

		var PieData = <?= json_encode( $piedata ) ?>;

		var pieOptions = {
				//Boolean - Whether we should show a stroke on each segment
				segmentShowStroke: true,
				//String - The colour of each segment stroke
				segmentStrokeColor: "#fff",
				//Number - The width of each segment stroke
				segmentStrokeWidth: 1,
				//Number - The percentage of the chart that we cut out of the middle
				percentageInnerCutout: 50, // This is 0 for Pie charts
				//Number - Amount of animation steps
				animationSteps: 100,
				//String - Animation easing effect
				animationEasing: "easeOutBounce",
				//Boolean - Whether we animate the rotation of the Doughnut
				animateRotate: true,
				//Boolean - Whether we animate scaling the Doughnut from the centre
				animateScale: false,
				//Boolean - whether to make the chart responsive to window resizing
				responsive: true,
				// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
				maintainAspectRatio: false,
				//String - A legend template
				legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
				//String - A tooltip template
				tooltipTemplate: "<%=value %> <%=label%>"
		};
		//Create pie or douhnut chart
		// You can switch between pie and douhnut using the method below.
		pieChart.Doughnut(PieData, pieOptions);
	});
</script>