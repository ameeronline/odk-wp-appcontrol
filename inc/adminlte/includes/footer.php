				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 1.6.0
				</div>
				<strong>Copyright &copy; 2014-<?= date("Y") ?> Agahe eMonitoring.</strong> All rights
				reserved.
			</footer>

		</div>
		<!-- ./wrapper -->
		<!-- jQuery UI 1.11.4 -->
		<script src="<?= adminlte_assets("bower_components/jquery-ui/jquery-ui.min.js") ?>"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		<!-- Bootstrap 3.3.7 -->
		<script src="<?= adminlte_assets("bower_components/bootstrap/dist/js/bootstrap.min.js") ?>"></script>
		<!-- Morris.js charts -->
		<script src="<?= adminlte_assets("bower_components/raphael/raphael.min.js") ?>"></script>
		<script src="<?= adminlte_assets("bower_components/morris.js/morris.min.js") ?>"></script>
		
		<!-- Chart.js -->
		<script src="<?= adminlte_assets("bower_components/chart.js/Chart.js") ?>"></script>

		<!-- Sparkline -->
		<script src="<?= adminlte_assets("bower_components/jquery-sparkline/dist/jquery.sparkline.min.js") ?>"></script>
		<!-- jvectormap -->
		<script src="<?= adminlte_assets("plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") ?>"></script>
		<script src="<?= adminlte_assets("plugins/jvectormap/jquery-jvectormap-world-mill-en.js") ?>"></script>
		<!-- jQuery Knob Chart -->
		<script src="<?= adminlte_assets("bower_components/jquery-knob/dist/jquery.knob.min.js") ?>"></script>
		<!-- daterangepicker -->
		<script src="<?= adminlte_assets("bower_components/moment/min/moment.min.js") ?>"></script>
		<script src="<?= adminlte_assets("bower_components/bootstrap-daterangepicker/daterangepicker.js") ?>"></script>
		<!-- datepicker -->
		<script src="<?= adminlte_assets("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") ?>"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<script src="<?= adminlte_assets("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js") ?>"></script>
		<!-- Slimscroll -->
		<script src="<?= adminlte_assets("bower_components/jquery-slimscroll/jquery.slimscroll.min.js") ?>"></script>
		<!-- FastClick -->
		<script src="<?= adminlte_assets("bower_components/fastclick/lib/fastclick.js") ?>"></script>
		<!-- AdminLTE App -->
		<script src="<?= adminlte_assets("dist/js/adminlte.min.js") ?>"></script>

		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMkqRw-3Y171HTn-1DqINPDfxwZSqb6fM&callback=initMap"></script>

	</body>
</html>