<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>
			Agahe E-Monitoring
		</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.7 -->
		<link rel="stylesheet" href="<?= adminlte_assets("bower_components/bootstrap/dist/css/bootstrap.min.css") ?>">
		<!-- media queries -->
		<link rel="stylesheet" href="<?= adminlte_assets("css/themes.css") ?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?= adminlte_assets("bower_components/font-awesome/css/font-awesome.min.css") ?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?= adminlte_assets("bower_components/Ionicons/css/ionicons.min.css") ?>">

		<link rel="icon" href="https://agahe.org.pk/wp-content/uploads/2018/03/cropped-logo-2-1-150x104.png" sizes="32x32" />
		<link rel="icon" href="https://agahe.org.pk/wp-content/uploads/2018/03/cropped-logo-2-1.png" sizes="192x192" />

		<!-- Theme style -->
		<link rel="stylesheet" href="<?= adminlte_assets("dist/css/AdminLTE.min.css") ?>">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
			folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="<?= adminlte_assets("dist/css/skins/_all-skins.min.css") ?>">
		<!-- Morris chart -->
		<link rel="stylesheet" href="<?= adminlte_assets("bower_components/morris.js/morris.css") ?>">
		<!-- jvectormap -->
		<link rel="stylesheet" href="<?= adminlte_assets("bower_components/jvectormap/jquery-jvectormap.css") ?>">
		<!-- Date Picker -->
		<link rel="stylesheet" href="<?= adminlte_assets("bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") ?>">
		<!-- Daterange picker -->
		<link rel="stylesheet" href="<?= adminlte_assets("bower_components/bootstrap-daterangepicker/daterangepicker.css") ?>">
		<!-- bootstrap wysihtml5 - text editor -->
		<link rel="stylesheet" href="<?= adminlte_assets("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css") ?>">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<!-- Google Font -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

		<link rel="stylesheet" href="<?= adminlte_assets("css/theme.css") ?>" />
		
		<!-- jQuery 3 -->
		<script src="<?= adminlte_assets("bower_components/jquery/dist/jquery.min.js") ?>"></script>

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>

		<script type="text/javascript" src="<?= adminlte_assets("js/work-plan.js") ?>"></script>

		<script type="text/javascript">
			var site_url = "<?= site_url() ?>";
		</script>

	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<!-- Logo -->
				<a href="index2.html" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>A</b>EM</span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Agahe</b> E-Monitoring</span>
				</a>
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?= adminlte_assets("dist/img/profile.png") ?>" class="user-image" alt="User Image">
									<span class="hidden-xs">Agahe</span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<img src="<?= adminlte_assets("dist/img/profile.png") ?>" class="img-circle" alt="User Image">
										<p>
											Agahe
											<small>Backend User</small>
										</p>
									</li>
									<!-- Menu Body -->
									<!-- <li class="user-body">
										<div class="row">
											<div class="col-xs-4 text-center">
												<a href="#">Followers</a>
											</div>
											<div class="col-xs-4 text-center">
												<a href="#">Sales</a>
											</div>
											<div class="col-xs-4 text-center">
												<a href="#">Friends</a>
											</div>
										</div>
									</li> -->
									<!-- Menu Footer-->
									<li class="user-footer">
										<!-- <div class="pull-left">
											<a href="#" class="btn btn-default btn-flat">Profile</a>
										</div> -->
										<div class="pull-right">
											<a href="<?= site_url("/appcontrol-logout") ?>" class="btn btn-default btn-flat">Sign out</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
					
					<!-- Sidebar user panel -->
					<div class="user-panel">
						<div class="pull-left image">
							<img src="<?= adminlte_assets("dist/img/profile.png") ?>" class="img-circle" alt="User Image">
						</div>
						<div class="pull-left info">
							<p>Agahe</p>
							<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
						</div>
					</div>
					
					<!-- sidebar menu: : style can be found in sidebar.less -->
					<ul class="sidebar-menu" data-widget="tree">
						<li class="header">MAIN NAVIGATION</li>
						<li class="<?= get_query_var("appcontrolroute") == "dashboard" ? "active" : "" ?>">
							<a href="<?= site_url("appcontrol/dashboard") ?>">
								<i class="fa fa-dashboard"></i>
								<span>Dashboard</span>
							</a>
						</li>
						<li>
							<a href="http://206.189.244.134:8080/ODKAggregate/multimode_login.html">
								<i class="fa fa-sign-in"></i>
								<span>Data Visualization</span>
							</a>
						</li>
						<!-- <li class="treeview <?= get_query_var("appcontrolroute") == "reportdata" ? "active" : "" ?>">
							<a href="javascript:;">
								<i class="fa fa-pie-chart"></i>
								<span>Reports</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<?php $forms = appcontrol_getforms2(); ?>
								<?php foreach ($forms as $form): ?>
								<li class="<?= get_query_var("reportid") == $form["formid"] ? "active" : "" ?>">
									<a href="<?= site_url("appcontrol/reportdata/".$form["formid"]) ?>">
										<i class="fa fa-circle-o"></i>
										<?= $form["title"] ?>
									</a>
								</li>
								<?php endforeach ?>
							</ul>
						</li> -->
						<li class="treeview <?= get_query_var("appcontrolroute") == "formdata" ? "active" : "" ?>">
							<a href="javascript:;">
								<i class="fa fa-pie-chart"></i>
								<span>Reports</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<?php $forms = appcontrol_getforms2(); ?>
								<?php foreach ($forms as $form): ?>
								<li class="<?= get_query_var("formid") == $form["formid"] ? "active" : "" ?>">
									<a href="<?= site_url("appcontrol/formdata/".$form["formid"]) ?>">
										<i class="fa fa-circle-o"></i>
										<?= $form["title"] ?>
									</a>
								</li>
								<?php endforeach ?>
							</ul>
						</li>
						<li class="<?= get_query_var("appcontrolroute") == "work-plan" ? "active" : "" ?>">
							<a href="<?= site_url("appcontrol/work-plan") ?>">
								<i class="fa fa-cog"></i>
								<span>Work Plan</span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
								<i class="fa fa-bar-chart"></i>
								<span>Indicators progress</span>
							</a>
						</li>
						<li class="<?= get_query_var("appcontrolroute") == "mapactivities" ? "active" : "" ?>">
							<a href="<?= site_url("appcontrol/mapactivities") ?>">
								<i class="fa fa-map-marker"></i>
								<span>Map of Activities</span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
								<i class="fa fa-server"></i>
								<span>About Project</span>
							</a>
						</li>
						<li>
							<a href="https://agahe.org.pk/all-downloads/">
								<i class="fa fa-file"></i>
								<span>Document Management</span>
							</a>
						</li>
						<li>
							<a href="http://agahe.org.pk/support">
								<i class="fa fa-file"></i>
								<span>Complaint Management</span>
							</a>
						</li>
						<li>
							<a href="<?= site_url("/appcontrol-logout") ?>">
								<i class="fa fa-sign-out"></i>
								<span>Sign out</span>
							</a>
						</li>
					</ul>
				</section>
				<!-- /.sidebar -->
			</aside>
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<?php global $pageTitle; ?>
					<?php $pageTitle = isset($pageTitle) ? $pageTitle : ""; ?>
					<h1>
						<?php echo $pageTitle ?>
						<small>Version 1.6.0</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active"><?php echo $pageTitle ?></li>
					</ol>
				</section>
				<!-- Main content -->
				<section class="content">