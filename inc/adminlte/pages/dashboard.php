<?php $pageTitle = "Dashboard"; ?>
<?php adminlte_inc("includes/header.php") ?>

<?php

global $grouptypedata;
global $colorarray;

global $gproject_code;

$colorarray = array("#f56954", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de");
$grouptypedata = appcontrol_allgrouptypevals( $gproject_code );

?>
<div class="row">
	<form action="<?= site_url("appcontrol/setprojectcode") ?>" method="POST" id="project_code_form">
		<div class="col-md-4">
			<label>
				Select Project Code:
			</label>
			<select id="project_code" name="project_code" class="form-control" onchange="jQuery('#project_code_form').get(0).submit()">
				<option value="">
					-- Project code --
				</option>
				<?php foreach (appcontrol_getprojectcodes() as $projectcode): ?>
				<option value="<?= $projectcode["PROJECT_CODE"] ?>" <?= $gproject_code == $projectcode["PROJECT_CODE"] ? "selected" : "" ?>>
					<?= $projectcode["PROJECT_CODE"] ?>
				</option>
				<?php endforeach ?>
			</select>
		</div>
	</form>
	<div class="clearfix"></div>
	<div class="height10"></div>
</div>
<div class="row">

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Project Progress</span>
				<span class="info-box-number">
					<?= appcontrol_projectprogress( $gproject_code ) ?>%
				</span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-red"><i class="glyphicon glyphicon-map-marker"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Group Formed</span>
				<span class="info-box-number">
					<?= appcontrol_sumgrouptypes( $gproject_code ) ?>
				</span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green"><i class="glyphicon glyphicon-search"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">UPIDP Formed</span>
				<span class="info-box-number">
					<?= appcontrol_sumupidpsdata( $gproject_code ) ?>
				</span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Beneficiaries</span>
				<span class="info-box-number">
					<?= appcontrol_beneficiaries( $gproject_code ) ?>
				</span>
			</div>
		</div>
	</div>
	
</div>



<div class="row">
	
	<?php adminlte_inc("components/survey-piechart.php") ?>

	<?php adminlte_inc("components/upidps-developed.php") ?>

</div>

<div class="row">
	
	<?php adminlte_inc("components/progress-chart.php") ?>
	<?php adminlte_inc("components/demo-pie.php") ?>

</div>

<div class="row" style="display: none">
	
	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<a href="<?= site_url("/appcontrol/surveycompleted-all") ?>">
					<h3 class="box-title">Survey Completed (Monthly)</h3>
				</a>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<canvas id="barChart" style="height: 280px;"></canvas>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<a href="<?= site_url("/appcontrol/dailysurveycompleted-all") ?>">
					<h3 class="box-title">Survey Completed (Daily)</h3>
				</a>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<canvas id="barChart2" style="height: 280px;"></canvas>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>

</div>

<div class="row">
	
	<?php adminlte_inc("components/map-activities.php") ?>

	<?php //adminlte_inc("components/distribute-type.php") ?>

</div>

<?php //adminlte_inc("components/forms-listing.php") ?>

<script type="text/javascript">

jQuery(document).ready(function() {

	var barChartCanvas = jQuery('#barChart').get(0).getContext('2d');
	var barChartCanvas2 = jQuery('#barChart2').get(0).getContext('2d');

	<?php $monthly_surveys = appcontrol_allmonthlycompletedsurvey( $gproject_code ); ?>

	var areaChartData = {
		labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		datasets: [{
				label: 'Survey Completed',
				fillColor: '#00a65a',
				strokeColor: '#00a65a',
				pointColor: '#00a65a',
				pointStrokeColor: '#c1c7d1',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: <?= json_encode( $monthly_surveys ) ?>
			}
		]
	};

	<?php $daily_surveys = []; ?>
	<?php $daily_surveys_label = []; ?>

	<?php

		for($i = 0;$i < cal_days_in_month(CAL_GREGORIAN, intval(date("m")), intval(date("Y")) ); $i++) {
			$daily_surveys_label[] = $i+1;
		}

		$daily_surveys = appcontrol_alldailycompletedsurvey( $gproject_code );

	?>

	var areaChartData2 = {
		labels: <?= json_encode( $daily_surveys_label ) ?>,
		datasets: [{
				label: 'Daily Completed Surveys',
				fillColor: '#00C0EF',
				strokeColor: '#00C0EF',
				pointColor: '#00C0EF',
				pointStrokeColor: '#c1c7d1',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: <?= json_encode($daily_surveys) ?>
			}
		]
	};

	//cal_days_in_month(CAL_GREGORIAN, 8, 2003);

	var barChart = new Chart(barChartCanvas);
	var barChart2 = new Chart(barChartCanvas2);

	var barChartData = areaChartData;
	/*barChartData.datasets[1].fillColor = '#00a65a';
	barChartData.datasets[1].strokeColor = '#00a65a';
	barChartData.datasets[1].pointColor = '#00a65a';*/

	var barChartOptions = {
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		scaleBeginAtZero: true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines: true,
		//String - Colour of the grid lines
		scaleGridLineColor: 'rgba(0,0,0,.05)',
		//Number - Width of the grid lines
		scaleGridLineWidth: 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,
		//Boolean - If there is a stroke on each bar
		barShowStroke: true,
		//Number - Pixel width of the bar stroke
		barStrokeWidth: 2,
		//Number - Spacing between each of the X value sets
		barValueSpacing: 5,
		//Number - Spacing between data sets within X values
		barDatasetSpacing: 1,
		//String - A legend template
		legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
		//Boolean - whether to make the chart responsive
		responsive: true,
		maintainAspectRatio: true
	}

	barChartOptions.datasetFill = false;
	barChart.Bar(barChartData, barChartOptions);

	barChart2.Bar(areaChartData2, barChartOptions);

	/* pieChart */

	var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
	var pieChart = new Chart(pieChartCanvas);

	<?php

	$piedata = array();

	$i = 0;

	foreach ( $grouptypedata as $grouptype => $grouptypevalue ) {
		
		$piedata[] = array(
			"value" => $grouptypevalue,
			"color" => $colorarray[$i],
			"highlight" => $colorarray[$i],
			"label" => $grouptype
		);
		
		$i++;

	}

	?>

	var PieData = <?= json_encode( $piedata ) ?>;

	var pieOptions = {
		//Boolean - Whether we should show a stroke on each segment
		segmentShowStroke: true,
		//String - The colour of each segment stroke
		segmentStrokeColor: '#fff',
		//Number - The width of each segment stroke
		segmentStrokeWidth: 2,
		//Number - The percentage of the chart that we cut out of the middle
		percentageInnerCutout: 50, // This is 0 for Pie charts
		//Number - Amount of animation steps
		animationSteps: 100,
		//String - Animation easing effect
		animationEasing: 'easeOutBounce',
		//Boolean - Whether we animate the rotation of the Doughnut
		animateRotate: true,
		//Boolean - Whether we animate scaling the Doughnut from the centre
		animateScale: false,
		//Boolean - whether to make the chart responsive to window resizing
		responsive: true,
		// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		maintainAspectRatio: true,
		//String - A legend template
		legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
	};
	//Create pie or douhnut chart
	// You can switch between pie and douhnut using the method below.
	pieChart.Doughnut(PieData, pieOptions);

});

</script>

<?php adminlte_inc("includes/footer.php") ?>