<?php $pageTitle = "Edit Form"; ?>
<?php adminlte_inc("includes/header.php") ?>
<?php $data = appcontrol_getformrowdata( $form, get_query_var("row") ); ?>

<div class="row">
	<div class="col-md-12">
		<form class="form" method="POST" action="<?= site_url("/appcontrol/editform/". get_query_var("formid") ."/". get_query_var("row") ) ?>">
			<?php foreach ($form["formfields"] as $formfield => $formfielddata): ?>
			<div class="form-group">
				<label>
					<?= $formfielddata["name"] ?>
				</label>
				<input type="text" class="form-control" name="formfields[<?= $formfield ?>]" value="<?= $data[strtoupper($formfield)] ?>" />
			</div>
			<?php endforeach ?>
			<input type="hidden" name="saveformdata" value="1" />
			<input type="submit" value="Update" class="btn btn-primary" />
		</form>
	</div>
</div>

<?php adminlte_inc("includes/footer.php") ?>