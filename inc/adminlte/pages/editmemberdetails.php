<?php $pageTitle = "Edit Member Details"; ?>
<?php adminlte_inc("includes/header.php") ?>

<?php

global $othertables;
global $tablesfields;

$member_details_tblname = $othertables["FORM1_GROUP_FORMATION_REPORT_CORE"]["member_details"];

$data = appcontrol_getformrowdata( $member_details_tblname, get_query_var("row"), true );

$formfields = $tablesfields[ $member_details_tblname ];

?>

<div class="row">
	<div class="col-md-12">
		<form class="form" method="POST" action="<?= site_url("/appcontrol/editmemberdetails/". get_query_var("formid") ."/". $data["_URI"] ) ?>">
			<?php foreach ($formfields as $formfield => $formfielddata): ?>
			<div class="form-group">
				<label>
					<?= $formfielddata["name"] ?>
				</label>
				<?php echo appcontrol_formfield($formfield, $formfielddata, $data[strtoupper($formfield)]); ?>
				<!-- <input type="text" class="form-control" name="formfields[<?= $formfield ?>]" value="<?= $data[strtoupper($formfield)] ?>" /> -->
			</div>
			<?php endforeach ?>
			<input type="hidden" name="saveformdata" value="1" />
			<input type="submit" value="Update" class="btn btn-primary" />
		</form>
	</div>
</div>

<?php adminlte_inc("includes/footer.php") ?>