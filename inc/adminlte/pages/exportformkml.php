<?xml version="1.0" encoding="utf-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
	<Document>
		<?php foreach( $locationdata as $data ): if( $data["LOCATION_LNG"] == null ) continue; ?>
		<Placemark>
			<name></name>
			<Point>
				<coordinates><?= $data["LOCATION_LAT"] ?>,<?= $data["LOCATION_LNG"] ?>,0</coordinates>
			</Point>
		</Placemark>
		<?php endforeach; ?>
	</Document>
</kml>