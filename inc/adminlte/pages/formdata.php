<?php $pageTitle = "Table view"; ?>
<?php adminlte_inc("includes/header.php") ?>
<?php $form = getform_by_id2(get_query_var("formid")); ?>
<?php $formdata = appcontrol_getform_data2( get_query_var("formid") ); ?>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

<div class="modal fade" id="showImagesModal" tabindex="-1" role="dialog" aria-labelledby="showImagesModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="showImagesModalLabel">Images</h4>
			</div>
			<div class="modal-body imagesarea">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<div class="height10"></div>
			<a href="<?php echo site_url("appcontrol/exportform/". $form["formid"]) ?>" class="btn btn-primary">Export CSV</a>
			<a href="<?php echo site_url("appcontrol/exportform/". $form["formid"]) ?>" class="btn btn-success">Export XSL</a>
			<a href="<?php echo site_url("appcontrol/exportformkml/". $form["formid"]) ?>" class="btn btn-info">Export KML</a>
			<a href="<?= site_url("/appcontrol/formmapdata/". $form["formid"]) ?>" class="btn btn-warning">Map View</a>
			<div class="height10"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div style="background-color:#fff">
			<form method="POST" class="form">
				<div class="col-md-4">
					<div class="form-group">
						<label>
							Date
						</label>
						<input type="text" name="search_date" class="datepicker form-control" value="<?= date("m-d-Y") ?>" />
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12">
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Search</button>
					</div>
				</div>
			</form>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped" style="background-color:#fff">
			<?php $limit = 0; ?>
			<?php $skip = 0; ?>
			<?php $nextsort = appcontrol_changesort(); ?>
			<tr>
				<?php foreach ($form["formfields"] as $formfield => $formfielddata): ?>
				<?php //$skip++; if( $skip < 7 ) continue; ?>
				<th>
					<a href="?<?= appcontrol_buildquery( array( "sortby" => $formfield, "sort" => $nextsort ) ) ?>">
						<?= $formfield ?>
						<?php if( getqueryvar("sortby") == $formfield ) {
						?>
						
						<?php if ($nextsort == "DESC"): ?>
						<i class="fa fa-caret-up"></i>
						<?php else: ?>
						<i class="fa fa-caret-down"></i>
						<?php endif ?>

						<?php
						} ?>
						
					</a>
				</th>
				<?php $limit++; if ( $limit == 6 ) { break; } ?>
				<?php endforeach ?>
				<th>Actions</th>
			</tr>
			<?php foreach ($formdata as $_data): ?>

			<tr class="openable" data-id="<?= $_data["_URI"] ?>">
				<?php $limit = 0; ?>
				<?php $skip = 0; ?>
				<?php foreach ($form["formfields"] as $formfield => $formfielddata) { ?>
					<?php //$skip++; if( $skip < 7 ) continue; ?>
					<td>
						<?= $_data[$formfield] ?>
					</td>
					<?php $limit++; if ( $limit == 6 ) { break; } ?>
				<?php } ?>
				<td>
					
					<?php if (appcontrol_hasview_permission($form["formid"])): ?>
					<?php $images = ( appcontrol_extract_images( $form["formid"], $_data["_URI"] ) ) ?>
					<button class="btn btn-success showimage-init" data-images="<?= htmlspecialchars(json_encode($images)) ?>" type="button" data-toggle="modal" data-target="#showImagesModal">Show Images</button>	
					<?php endif ?>
					
					<?php if (appcontrol_hasedit_permission($form["formid"])): ?>
					<a href="<?= site_url("/appcontrol/editform/". $form["formid"] ."/". $_data["_URI"] ) ?>" class="btn btn-primary">
						Edit
					</a>
					<?php endif ?>
					
					<?php if (appcontrol_hasview_permission($form["formid"])): ?>

					<?php if ( $form["formid"] == "form1_GroupFormationReport" ): ?>
					<a href="<?= site_url("/appcontrol/". $form["formid"] ."/memberdetails/". $_data["_URI"] ) ?>" class="btn btn-info">
						Member Details
					</a>
					<?php endif ?>

					<?php endif ?>
					
					<?php if (appcontrol_hasedit_permission($form["formid"])): ?>
					<button onclick="event.stopPropagation();if(confirm('You really want to delete this record?')) window.location = this.getAttribute('data-url');" data-url="<?= site_url("/appcontrol/deleteform/". $form["formid"] ."/". $_data["_URI"] ) ?>" class="btn btn-danger">
						Delete
					</button>
					<?php endif ?>
				</td>
			</tr>

			<?php endforeach ?>
			<?php if (count($formdata) == 0): ?>
			<tr>
				<td colspan="<?= count($form["formfields"]) ?>">
					<center>
						No records found
					</center>
				</td>
			</tr>
			<?php endif ?>
		</table>
	</div>
</div>


<script type="text/javascript">
	var formid = '<?= $form["formid"] ?>';
	
	jQuery(".openable a").click(function(e) {
		e.stopPropagation();
		jQuery(this).trigger("click");
	});

	jQuery(".openable").click(function(e) {
		
		e.stopPropagation();
		window.location = '<?= site_url() ?>/appcontrol/rowview/' + formid + '/' + jQuery(this).attr("data-id");

	});
	
	jQuery(".showimage-init").on("click", function(e) {
		e.stopPropagation();
		var _json = jQuery.parseJSON(jQuery(this).attr("data-images"));
		var htm = '';
		for(k in _json) {
			htm += '<a href="<?= site_url() ?>/appcontrol/showimage/' + encodeURIComponent(_json[k]) +'" target="_blank" download><img src="<?= site_url() ?>/appcontrol/showimage/' + encodeURIComponent(_json[k]) +'" style="width:100px;"/></a>';
			//htm += '<a href="' + (_json[k]) +'" target="_blank" download><img src="' + (_json[k]) +'" style="width:100px;"/></a>';
		}

		if(htm == '') {
			htm += '<p>No Image found.</p>';
		}
		jQuery(".imagesarea").html(htm);
		jQuery("#showImagesModal").modal("show");
	});

	jQuery(".datepicker").datepicker();

</script>

<?php adminlte_inc("includes/footer.php") ?>