<?php $pageTitle = "Form Map View"; ?>

<?php $form = getform_by_id2(get_query_var("formid")); ?>

<?php adminlte_inc("includes/header.php") ?>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">
					Map for <?= $form["title"] ?>
				</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div class="row">
					<div class="col-md-12">
						<div id="map-activities" style="height: 325px;"></div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>

<script>

	<?php
		

		$locationdata = appcontrol_locationdatabytbl( $form["formid"] );

		$_locationdata = [];

		foreach ($locationdata as $location) {
			$_locationdata[] = [
				"",
				$location["LOCATION_LAT"],
				$location["LOCATION_LNG"],
			];
		}

		?>
	//var locations = locBannu.concat(locBara);

	var locations = <?= json_encode($_locationdata) ?>;

	var icon = {
		url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|ffef00", // url
	};
	
	var map;
	var markers = [];
	var bounds = [];
	
	function setMarkers(loc, district) {
		var marker, i;
		bounds = new google.maps.LatLngBounds();
		
		if (district && loc.length == 0) {
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({'address': district + ' Pakistan'}, function(results, status) {
				if (status === 'OK') {
					map.setCenter(results[0].geometry.location);
					for (i = 0; i < loc.length; i++) { 
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(loc[i][1], loc[i][2]),
							map: map,
							title: loc[i][0],
							icon: icon
						});
	
						bounds.extend(marker.position);
						markers.push(marker);
					}
				} else {
	
				}
			});
		}
		else {
			for (i = 0; i < loc.length; i++) { 
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(loc[i][1], loc[i][2]),
					map: map,
					title: loc[i][0],
					icon: icon
				});
	
				bounds.extend(marker.position);
				markers.push(marker);
			}
		}
	}
	
	function reloadMarkers(loc, district) {
		for (var i=0; i<markers.length; i++) {
			markers[i].setMap(null);
		}
	
		markers = [];
		setMarkers(loc, district);
		if (loc.length > 0) {
			map.fitBounds(bounds);
		}
	}
	
	function initMap() {
		map = new google.maps.Map(document.getElementById('map-activities'), {
			zoom: 8
		});
	
		setMarkers(locations);
		map.fitBounds(bounds);
	}
	
	function mapBanu(marker) {
		var i;
	
		marker.setMap(null);
		for (i = 0; i < locBannu.length; i++) { 
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locBannu[i][1], locBannu[i][2]),
				map: map,
				title: locBannu[i][0],
				icon: icon
			});
	
			bounds.extend(marker.position);
		}
	}
	
	function resizeMap() {
		var myMap = document.getElementById('map-activities');
		myMap.classList.toggle("fullscreen");
	}
	
</script>

<?php adminlte_inc("includes/footer.php") ?>