<?php $pageTitle = "Member Details"; ?>
<?php adminlte_inc("includes/header.php") ?>
<?php 

global $othertables;
global $tablesfields;

$member_details_tblname = $othertables["FORM1_GROUP_FORMATION_REPORT_CORE"]["member_details"];

$member_details = $appcontroldb->get( $member_details_tblname );

$formfields = $tablesfields[ $member_details_tblname ];

$row_id = get_query_var("row");

$formdata = appcontrol_getform_data2( $member_details_tblname, true );

?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped" style="background-color:#fff">
			<?php $limit = 0; ?>
			<?php $skip = 0; ?>
			<?php $nextsort = appcontrol_changesort(); ?>
			<tr>
				<?php foreach ($formfields as $formfield => $formfielddata): ?>
				<?php //$skip++; if( $skip < 7 ) continue; ?>
				<th>
					<a href="?<?= appcontrol_buildquery( array( "sortby" => $formfield, "sort" => $nextsort ) ) ?>">
						<?= $formfielddata["name"] ?>
						<?php if( getqueryvar("sortby") == $formfield ) {
						?>
						
						<?php if ($nextsort == "DESC"): ?>
						<i class="fa fa-caret-up"></i>
						<?php else: ?>
						<i class="fa fa-caret-down"></i>
						<?php endif ?>

						<?php
						} ?>
						
					</a>
				</th>
				<?php $limit++; if ( $limit == 6 ) { break; } ?>
				<?php endforeach ?>
				<?php if (appcontrol_hasedit_permission()): ?>
					<th>Actions</th>
				<?php endif ?>
			</tr>
			<?php foreach ($formdata as $_data): ?>

			<tr class="openable" data-id="<?= $_data["_URI"] ?>">
				<?php $limit = 0; ?>
				<?php $skip = 0; ?>
				<?php foreach ($formfields as $formfield => $formfielddata) { ?>
					<?php //$skip++; if( $skip < 7 ) continue; ?>
					<td>
						<?= $_data[$formfield] ?>
					</td>
					<?php $limit++; if ( $limit == 6 ) { break; } ?>
				<?php } ?>
				<?php if (appcontrol_hasedit_permission()): ?>
					<td>
						<a href="<?= site_url("/appcontrol/editmemberdetails/". $row_id ."/". $_data["_URI"] ) ?>" class="btn btn-primary">
							Edit
						</a>
					</td>
				<?php endif ?>
			</tr>

			<?php endforeach ?>
			<?php if (count($formdata) == 0): ?>
			<tr>
				<td colspan="<?= count($form["formfields"]) ?>">
					<center>
						No records found
					</center>
				</td>
			</tr>
			<?php endif ?>
		</table>
	</div>
</div>

<?php adminlte_inc("includes/footer.php") ?>