<?php $pageTitle = "Report"; ?>
<?php adminlte_inc("includes/header.php") ?>

<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Survey Complete</span>
				<span class="info-box-number">0</span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-red"><i class="glyphicon glyphicon-map-marker"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">UC Covered</span>
				<span class="info-box-number">0</span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green"><i class="glyphicon glyphicon-search"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Tehsil covered</span>
				<span class="info-box-number">0</span>
			</div>
		</div>
	</div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">District Covered</span>
				<span class="info-box-number">0</span>
			</div>
		</div>
	</div>
</div>

<div class="row">
	
	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">Survey Completed (Monthly)</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<canvas id="barChart" style="height: 280px;"></canvas>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">Survey Completed (Daily)</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<canvas id="barChart2" style="height: 280px;"></canvas>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>

</div>

<div class="row">
	
	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">Map of Activities</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<canvas id="mapOfActivities" style="height: 210px;"></canvas>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">Distribution by Group Type</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-10">
						<div class="chart-responsive">
							<canvas id="pieChart" style="height: 210px;"></canvas>
							<!-- <canvas id="pieChart" height="150"></canvas> -->
						</div>
						<!-- ./chart-responsive -->
					</div>
					<!-- /.col -->
					<div class="col-md-2 no-padding">
						<ul class="chart-legend clearfix">
							<li><i class="fa fa-circle-o text-red"></i> Chrome</li>
							<li><i class="fa fa-circle-o text-green"></i> IE</li>
							<li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
							<li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
							<li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
							<li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
						</ul>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-12">
		
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">Form Entities View in Tabular form</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {

	var barChartCanvas = jQuery('#barChart').get(0).getContext('2d');
	var barChartCanvas2 = jQuery('#barChart2').get(0).getContext('2d');

	var areaChartData = {
		labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
		datasets: [{
				label: 'Electronics',
				fillColor: 'rgba(210, 214, 222, 1)',
				strokeColor: 'rgba(210, 214, 222, 1)',
				pointColor: 'rgba(210, 214, 222, 1)',
				pointStrokeColor: '#c1c7d1',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: [65, 59, 80, 81, 56, 55, 40]
			},
			{
				label: 'Digital Goods',
				fillColor: 'rgba(60,141,188,0.9)',
				strokeColor: 'rgba(60,141,188,0.8)',
				pointColor: '#3b8bba',
				pointStrokeColor: 'rgba(60,141,188,1)',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(60,141,188,1)',
				data: [28, 48, 40, 19, 86, 27, 90]
			}
		]
	};

	var barChart = new Chart(barChartCanvas);
	var barChart2 = new Chart(barChartCanvas2);

	var barChartData = areaChartData;
	barChartData.datasets[1].fillColor = '#00a65a';
	barChartData.datasets[1].strokeColor = '#00a65a';
	barChartData.datasets[1].pointColor = '#00a65a';

	var barChartOptions = {
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		scaleBeginAtZero: true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines: true,
		//String - Colour of the grid lines
		scaleGridLineColor: 'rgba(0,0,0,.05)',
		//Number - Width of the grid lines
		scaleGridLineWidth: 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,
		//Boolean - If there is a stroke on each bar
		barShowStroke: true,
		//Number - Pixel width of the bar stroke
		barStrokeWidth: 2,
		//Number - Spacing between each of the X value sets
		barValueSpacing: 5,
		//Number - Spacing between data sets within X values
		barDatasetSpacing: 1,
		//String - A legend template
		legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
		//Boolean - whether to make the chart responsive
		responsive: true,
		maintainAspectRatio: true
	}

	barChartOptions.datasetFill = false;
	barChart.Bar(barChartData, barChartOptions);

	barChart2.Bar(barChartData, barChartOptions);

	/* pieChart */

	var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
	var pieChart = new Chart(pieChartCanvas);

	var PieData = [
		{
			value: 700,
			color: '#f56954',
			highlight: '#f56954',
			label: 'Chrome'
		},
		{
			value: 500,
			color: '#00a65a',
			highlight: '#00a65a',
			label: 'IE'
		},
		{
			value: 400,
			color: '#f39c12',
			highlight: '#f39c12',
			label: 'FireFox'
		},
		{
			value: 600,
			color: '#00c0ef',
			highlight: '#00c0ef',
			label: 'Safari'
		},
		{
			value: 300,
			color: '#3c8dbc',
			highlight: '#3c8dbc',
			label: 'Opera'
		},
		{
			value: 100,
			color: '#d2d6de',
			highlight: '#d2d6de',
			label: 'Navigator'
		}
	];

	var pieOptions = {
		//Boolean - Whether we should show a stroke on each segment
		segmentShowStroke: true,
		//String - The colour of each segment stroke
		segmentStrokeColor: '#fff',
		//Number - The width of each segment stroke
		segmentStrokeWidth: 2,
		//Number - The percentage of the chart that we cut out of the middle
		percentageInnerCutout: 50, // This is 0 for Pie charts
		//Number - Amount of animation steps
		animationSteps: 100,
		//String - Animation easing effect
		animationEasing: 'easeOutBounce',
		//Boolean - Whether we animate the rotation of the Doughnut
		animateRotate: true,
		//Boolean - Whether we animate scaling the Doughnut from the centre
		animateScale: false,
		//Boolean - whether to make the chart responsive to window resizing
		responsive: true,
		// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		maintainAspectRatio: true,
		//String - A legend template
		legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
	};
	//Create pie or douhnut chart
	// You can switch between pie and douhnut using the method below.
	pieChart.Doughnut(PieData, pieOptions);

});

</script>

<?php adminlte_inc("includes/footer.php") ?>