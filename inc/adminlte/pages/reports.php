<?php $pageTitle = "Reports"; ?>
<?php adminlte_inc("includes/header.php") ?>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="box box-active">
			<div class="box-header with-border">
				<h3 class="box-title">
					Form Entities
				</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<?php $forms = appcontrol_getforms2(); ?>
				<?php foreach ($forms as $k => $form): ?>
					<a href="<?= site_url("/appcontrol/formdata/". $form["formid"]) ?>" style="font-size: 18px;">
						<?php echo $form["title"] ?>
					</a><br />
				<?php endforeach ?>
				<div class="clearfix"></div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>

<?php adminlte_inc("includes/footer.php") ?>