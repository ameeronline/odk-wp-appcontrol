<?php $pageTitle = "Form View"; ?>
<?php adminlte_inc("includes/header.php") ?>
<?php $data = appcontrol_getformrowdata( $form, get_query_var("row") ); ?>

<?php

global $tablesfields;

$formfields = $tablesfields[strtolower($form["tablename"])];

?>

<div class="row">

	<?php adminlte_inc("components/map-activities2.php") ?>

	<div class="col-md-12">
		<table class="table table-striped table-bordered" style="background-color: #fff;">
			<thead>
				<?php foreach ($formfields as $k => $formfield): ?>
				<tr>
					<th>
						<?= $formfield["name"] ?>
					</th>
					<td>
						<?= $data[$k] ?>
					</td>
				</tr>
				<?php endforeach ?>
			</thead>
		</table>
	</div>
	
</div>

<?php adminlte_inc("includes/footer.php") ?>