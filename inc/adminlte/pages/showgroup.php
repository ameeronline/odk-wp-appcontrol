<?php $pageTitle = "Group data"; ?>
<?php adminlte_inc("includes/header.php") ?>

<?php $groups = array("ECG" => "ECG","Sub_WASH" => "Sub_WASH","Common trade and skills group" => "Common trade and skills group","PM&EG" => "PM&EG"); ?>

<?php


global $grouptypedata;
global $colorarray;

$grouptypedata = appcontrol_grouptypevals();

$colorarray = array("#f56954", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de");

$groupdt = array( "total" => 0, "male" => 0, "female" => 0 );

foreach ($grouptypedata as $groupname => $grouptype) {
	if( $groupname == urldecode(get_query_var("groupname")) ) {
		$groupdt = $grouptype;
		break;
	}
}

$memberdetails = appcontrol_memberdetailsByGroup( urldecode(get_query_var("groupname")) );

?>

<div class="col-md-6">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Genders</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="height: 345px;">
			<div class="row">
				<div class="col-md-8">
					<div class="chart" style="height: 300px;">
						<!-- Sales Chart Canvas -->
						<canvas id="pieChart3" style="height: 300px;"></canvas>
					</div>
					<!-- /.chart-responsive -->
				</div>
				<div class="col-md-4">
					<ul class="chart-legend clearfix">
						<li><i class="fa fa-circle-o" style="color: <?= $colorarray[0] ?>"></i> Male</li>
						<li><i class="fa fa-circle-o" style="color: <?= $colorarray[1] ?>"></i> Female</li>
					</ul>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- ./box-body -->
	</div>
	<!-- /.box -->
</div>
<script>
	jQuery(document).ready(function() {
		var pieChartCanvas = $("#pieChart3").get(0).getContext("2d");
		pieChartCanvas.height = 300;
		pieChartCanvas.width = 300;
		var pieChart = new Chart(pieChartCanvas);

		<?php

		$piedata = array();

		$piedata[] = array(
			"value" => $groupdt["male"],
			"color" => $colorarray[0],
			"highlight" => $colorarray[0],
			"label" => "Male"
		);

		$piedata[] = array(
			"value" => $groupdt["female"],
			"color" => $colorarray[1],
			"highlight" => $colorarray[1],
			"label" => "Female"
		);

		?>

		var PieData = <?= json_encode( $piedata ) ?>;

		var pieOptions = {
				//Boolean - Whether we should show a stroke on each segment
				segmentShowStroke: true,
				//String - The colour of each segment stroke
				segmentStrokeColor: "#fff",
				//Number - The width of each segment stroke
				segmentStrokeWidth: 1,
				//Number - The percentage of the chart that we cut out of the middle
				percentageInnerCutout: 50, // This is 0 for Pie charts
				//Number - Amount of animation steps
				animationSteps: 100,
				//String - Animation easing effect
				animationEasing: "easeOutBounce",
				//Boolean - Whether we animate the rotation of the Doughnut
				animateRotate: true,
				//Boolean - Whether we animate scaling the Doughnut from the centre
				animateScale: false,
				//Boolean - whether to make the chart responsive to window resizing
				responsive: true,
				// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
				maintainAspectRatio: false,
				//String - A legend template
				legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
				//String - A tooltip template
				tooltipTemplate: "<%=value %> <%=label%>"
		};
		//Create pie or douhnut chart
		// You can switch between pie and douhnut using the method below.
		pieChart.Doughnut(PieData, pieOptions);
	});
</script>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>
				Name
			</th>
			<th>
				Contact no.
			</th>
			<th>
				Education
			</th>
			<th>
				Gender
			</th>
			<th>
				Designation
			</th>
			<th>
				Age
			</th>
			<th>
				Position Assigned
			</th>
			<th>
				Department
			</th>
			<th>
				NIC
			</th>
		</tr>
	</thead>
	<tbody>
		<?php if( count($memberdetails) == 0 ): ?>
		<tr>
			<td colspan="9" class="text-center">
				No Records Found
			</td>
		</tr>
		<?php else: ?>

		<?php foreach( $memberdetails as $memberdetail ) { ?>
		<tr>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_NAME"] ?>
			</td>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_CONTACT_NO"] ?>
			</td>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_EDUCATION"] ?>
			</td>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_GENDER"] ?>
			</td>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_DESIGNATION"] ?>
			</td>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_AGE"] ?>
			</td>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_POSITION_ASSIGNED"] ?>
			</td>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_DEPARTMENT"] ?>
			</td>
			<td>
				<?php echo $memberdetail["GROUP_MEMBER_NIC"] ?>
			</td>
		</tr>
		<?php } ?>

		<?php endif; ?>
	</tbody>
</table>

<?php adminlte_inc("includes/footer.php") ?>