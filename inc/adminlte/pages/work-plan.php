<?php $pageTitle = "Work Plan"; ?>
<?php adminlte_inc("includes/header.php") ?>

<?php

global $colorarray;

$colorarray = array("#f56954", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de");

$workplans = appcontrol_fetchWorkPlan();

?>

<div class="row" ng-app="angularApp" ng-controller="workPlanCtrl">
	<div class="col-md-12">
		
		<div class="box box-active">
			
			<div class="box-header with-border">
				<h3 class="box-title">
					Workplan for project
				</h3>
			</div>
		<div class="row" style="padding-top:10px;">
			<div class="col-md-12 col-lg-12">
				<div style="display: inline-block;float: left;padding-left: 5px;"><h4 for="selecttag">Project Code&nbsp;&nbsp;&nbsp;&nbsp;</h4></div>
				<div style="display: inline-block;float: left;width: 150px;">
					<div class="form-group">
						
						<!-- <input type="text" ng-model="projectCodeTxt" class="form-control" /> -->
						<select id="selecttag" ng-model="projectCodeTxt" class="form-control" ng-change="projectCodeHandle()">
							<option value="">
								-- Project code --
							</option>
							<?php foreach (appcontrol_getprojectcodes() as $projectcode): ?>
							<option value="<?= $projectcode["PROJECT_CODE"] ?>" <?= $gproject_code == $projectcode["PROJECT_CODE"] ? "selected" : "" ?>>
								<?= $projectcode["PROJECT_CODE"] ?>
							</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
			</div>
				<div class="col-md-8">
				<h3>
					&nbsp;&nbsp;
					<?php if (appcontrol_hasedit_permission()): ?>
					<button class="btn btn-success" ng-click="addAction()">Add Activity</button>
					<?php endif ?>
					&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="<?= site_url("appcontrol/exportworkplan") ?>">Export to XLS</a></h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body" style="overflow: auto;width: 100%;">

				

				<div class="clearfix"></div>

				<div class="height10"></div>

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>
								Project Code
							</th>
							<th>
								Outcome
							</th>
							<th>
								Output
							</th>
							<th>
								Activity
							</th>
							<th>
								Activity name
							</th>
							<th>
								Planned Start
							</th>
							<th>
								Planned End
							</th>
							<th>
								Actual Start
							</th>
							<th>
								Actual End
							</th>
							<th>
								Total Target
							</th>
							<th>
								Achievement
							</th>
							<th>
								% progress
							</th>
							<th>
								Activity Status
							</th>
							<th>
								Comments
							</th>
							<?php if (appcontrol_hasedit_permission()): ?>
							<th>
								Action
							</th>
							<?php endif ?>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="workplan in workplans | filter:projectCodeTxt:strict" ng-class="{ 'bg-success': workplan.activity_status == 'Completed', 'bg-danger': workplan.activity_status == 'Delayed', 'bg-warning': workplan.activity_status == 'In Progress' }">
							<td>
								{{ workplan.project_code }}
							</td>
							<td>
								{{ workplan.outcome }}
							</td>
							<td>
								{{ workplan.output_number }}
							</td>
							<td>
								{{ workplan.activity_number }}
							</td>
							<td>
								{{ workplan.activity_name }}
							</td>
							<td>
								{{ workplan.planned_start | date:"yyyy-MM-dd" }}
							</td>
							<td>
								{{ workplan.planned_end | date:"yyyy-MM-dd" }}
							</td>
							<td>
								{{ workplan.actual_start | date:"yyyy-MM-dd" }}
							</td>
							<td>
								{{ workplan.actual_end | date:"yyyy-MM-dd" }}
							</td>
							<td>
								{{ workplan.total_target }}
							</td>
							<td>
								{{ workplan.achievement }}
							</td>
							<td>
								{{ workplan.progress }}
							</td>
							<td>
								{{ workplan.activity_status }}
							</td>
							<td>
								{{ workplan.comments }}
							</td>
							<?php if (appcontrol_hasedit_permission()): ?>
							<td>
								<a href="javascript:;" ng-click="addAction(workplan.id,$index)" class="btn btn-primary" style="padding:0;"><i style="font-size: 18px;" class="fa fa-edit"></i></a>
								<a href="javascript:;" ng-click="deleteEntry(workplan.id,$index)" class="btn btn-danger" style="padding:0;"><i style="font-size: 18px;" class="fa fa-trash"></i></a>
							</td>
							<?php endif ?>
						</tr>

						<tr ng-show="showAdd">
							<td>
								<input type="text" ng-model="project_code" class="form-control" placeholder="Project code" readonly />
							</td>
							<td>
								<input type="number" ng-model="outcome" id="outcome" class="form-control" placeholder="Outcome" />
							</td>
							<td>
								<input type="number" ng-model="output_number" id="output" class="form-control" placeholder="Output number" />
							</td>
							<td>
								<input type="number" ng-model="activity_number" id="activity" class="form-control" placeholder="Activity number" />
							</td>
							<td>
								<input type="text" ng-model="activity_name" class="form-control" placeholder="Activity name" />
							</td>
							<td>
								<input date-input type="date" ng-model="planned_start" class="form-control" id="psdate" placeholder="Planned Start" />
							</td>
							<td>
								<input date-input type="date" ng-model="planned_end" class="form-control" id="pedate" placeholder="Planned End" />
							</td>
							<td>
								<input date-input type="date" ng-model="actual_start" class="form-control" id="asdate" placeholder="Actual Start" />
							</td>
							<td>
								<input date-input type="date" ng-model="actual_end" class="form-control" id="aedate" placeholder="Actual End" />
							</td>
							<td>
								<input type="text" ng-model="total_target" class="form-control" placeholder="Total Target" ng-keyup="changeHandleT()" />
							</td>
							<td>
								<input type="text" ng-model="achievement" class="form-control" placeholder="Achievement" ng-keyup="changeHandleT()" />
							</td>
							<td>
								<input type="text" ng-model="progress" class="form-control" placeholder="% Progress" ng-keyup="changeHandleT()" />
							</td>
							<td>
								<select class="form-control" ng-model="activity_status">
									<option value="Completed">
										Completed
									</option>
									<option value="In Progress">
										In Progress
									</option>
									<option value="Delayed">
										Delayed
									</option>
									<option value="NA">
										NA
									</option>
								</select>
							</td>
							<td>
								<input type="text" ng-model="comments" class="form-control" placeholder="Comments" />
							</td>
						</tr>
						
						<tr ng-show="workplans.length == 0">
							<td colspan="5">
								<center>
									No records found
								</center>
							</td>
						</tr>

					</tbody>
				</table>

				<?php if (appcontrol_hasedit_permission()): ?>
				<div ng-show="showAdd" style="margin-top: 5px;margin-left: 5px;">
					<a href="javascript:;" class="btn btn-success" ng-show="id == null" ng-click="saveEntry()"><i style="font-size: 18px;" class="fa fa-check"></i></a>
					<a href="javascript:;" class="btn btn-success" ng-show="id != null" ng-click="saveEntry()"><i style="font-size: 18px;" class="fa fa-edit"></i></a>
					<a href="javascript:;" class="btn btn-danger" ng-click="cancelAction()"><i style="font-size: 18px;" class="fa fa-times"></i></a>
					<div class="clearfix"></div>
				</div>
				<?php endif ?>

				<div class="clearfix"></div>

			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	<?php

	/*foreach ($workplans as &$workplan) {
		$workplan["planned_start"] = date("m-d-Y");
		$workplan["planned_end"] = date("m-d-Y");
		$workplan["actual_start"] = date("m-d-Y");
		$workplan["actual_end"] = date("m-d-Y");
	}*/

	?>
	var workplans = <?= json_encode($workplans) ?>;
</script>

<?php adminlte_inc("includes/footer.php") ?>