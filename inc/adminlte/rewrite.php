<?php

add_filter( 'rewrite_rules_array','appcontrol_insert_rewrite_rules' );
add_filter( 'query_vars','appcontrol_insert_query_vars' );
add_action( 'wp_loaded','appcontrol_flush_rules' );

function appcontrol_flush_rules(){
	$rules = get_option( 'rewrite_rules' );

	if ( ! isset( $rules['appcontrol/editmemberdetails/([^\/]+)/([^\/]+)'] ) ) {
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
	}
}

// Adding a new rule
function appcontrol_insert_rewrite_rules( $rules )
{
	$newrules = array();
	$newrules['appcontrol-login'] = 'index.php?appcontrolroute=appcontrol-login'; //&myargument=$matches[2]
	$newrules['appcontrol-logout'] = 'index.php?appcontrolroute=appcontrol-logout';
	$newrules['appcontrol/reportdata/([^\/]+)'] = 'index.php?appcontrolroute=reportdata&reportid=$matches[1]';
	$newrules['appcontrol/formdata/([^\/]+)'] = 'index.php?appcontrolroute=formdata&formid=$matches[1]';
	$newrules['appcontrol/formmapdata/([^\/]+)'] = 'index.php?appcontrolroute=formmapdata&formid=$matches[1]';
	$newrules['appcontrol/editform/([^\/]+)/([^\/]+)'] = 'index.php?appcontrolroute=editform&formid=$matches[1]&row=$matches[2]';
	
	$newrules['appcontrol/([^\/]+)/memberdetails/([^\/]+)'] = 'index.php?appcontrolroute=memberdetails&formid=$matches[1]&row=$matches[2]';
	$newrules['appcontrol/editmemberdetails/([^\/]+)/([^\/]+)'] = 'index.php?appcontrolroute=editmemberdetails&formid=$matches[1]&row=$matches[2]';

	$newrules['appcontrol/rowview/([^\/]+)/([^\/]+)'] = 'index.php?appcontrolroute=rowview&formid=$matches[1]&row=$matches[2]';

	$newrules['appcontrol/deleteform/([^\/]+)/([^\/]+)'] = 'index.php?appcontrolroute=deleteform&formid=$matches[1]&row=$matches[2]';

	$newrules['appcontrol/showgroup/([^\/]+)'] = 'index.php?appcontrolroute=showgroup&groupname=$matches[1]';
	
	$newrules['appcontrol/exportform/([^\/]+)'] = 'index.php?appcontrolroute=exportform&formid=$matches[1]';

	$newrules['appcontrol/exportformkml/([^\/]+)'] = 'index.php?appcontrolroute=exportformkml&formid=$matches[1]';
	$newrules['appcontrol/showimage/([^\/]+)'] = 'index.php?appcontrolroute=showimage&imageid=$matches[1]';

	$newrules['appcontrol/work-plan'] = 'index.php?appcontrolroute=work-plan';
	$newrules['appcontrol/saveworkplan'] = 'index.php?appcontrolroute=saveworkplan';
	$newrules['appcontrol/deleteworkplan'] = 'index.php?appcontrolroute=deleteworkplan';
	$newrules['appcontrol/exportworkplan'] = 'index.php?appcontrolroute=exportworkplan';
	$newrules['appcontrol/reports'] = 'index.php?appcontrolroute=reports';

	$newrules['appcontrol/(dashboard|surveycompleted-all|dailysurveycompleted-all|mapactivities|forms|setprojectcode)'] = 'index.php?appcontrolroute=$matches[1]';
	return $newrules + $rules;
}

// Adding the id var so that WP recognizes it
function appcontrol_insert_query_vars( $vars )
{
	array_push($vars, 'appcontrolroute'); //, 'myargument'
	array_push($vars, 'reportid');
	array_push($vars, 'formid');
	array_push($vars, 'row');
	array_push($vars, 'groupname');
	array_push($vars, 'imageid');
	return $vars;
}

function appcontrol_parsequery(){
	if ( get_query_var("appcontrolroute") ) {

		global $pageTitle;

		global $gproject_code;

		$gproject_code = "";

		if( isset( $_SESSION["project_code"] ) ) {
			$gproject_code = $_SESSION["project_code"];
		}

		switch ( get_query_var("appcontrolroute") ) {

			case 'appcontrol-logout':
				unset($_SESSION["user"]);
				session_destroy();
				appcontrol_frontredirect("/appcontrol-login");
				exit;
			break;

			case 'memberdetails':

				if (!appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}

				global $appcontroldb;

				$usr = appcontrol_sessionuser();
				require_once ( APPCONTROL_PATH . "inc/adminlte/pages/" . get_query_var("appcontrolroute") . ".php" );

			break;

			case 'editmemberdetails':

				if (!appcontrol_checklogin() || !appcontrol_hasedit_permission("form1_GroupFormationReport")) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}

				global $othertables;
				$member_details_tblname = $othertables["FORM1_GROUP_FORMATION_REPORT_CORE"]["member_details"];

				$usr = appcontrol_sessionuser();

				global $appcontroldb;

				if ( getpostvar("saveformdata") == 1 ) {

					$formfields = array();

					foreach (getpostvar("formfields") as $k => $val) {
						//if(in_array($k, $form["formfields"])) {
							$formfields[$k] = $val;
						//}
					}

					$appcontroldb->where( '_URI', get_query_var("row") );
					$appcontroldb->update( $member_details_tblname, $formfields );
					
					echo "<script>location=location.href</script>";
					exit;

				}

				$usr = appcontrol_sessionuser();
				require_once ( APPCONTROL_PATH . "inc/adminlte/pages/" . get_query_var("appcontrolroute") . ".php" );

			break;

			case 'showimage':

				if (!appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}

				global $appcontroldb;

				$_imageid = explode("|",urldecode(get_query_var("imageid")));

				$imagetbl = $_imageid[0];

				$imageuid = $_imageid[1];

				$appcontroldb->where("_URI", $imageuid);

				$image = $appcontroldb->get($imagetbl);

				header("Content-Type: image/jpeg");

				echo $image[0]["VALUE"];

				break;

			case 'setprojectcode':

				if (!appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}
				
				if( isset( $_POST["project_code"] ) )
					$_SESSION["project_code"] = $_POST["project_code"];

				appcontrol_frontredirect("/appcontrol/dashboard");

				break;

			case 'deleteworkplan':

				if (!appcontrol_checklogin() || !appcontrol_hasedit_permission("form1_GroupFormationReport")) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}

				global $appcontroldb;

				$appcontroldb->rawQuery("DELETE FROM workplans WHERE id='". $appcontroldb->escape($_POST["id"]) ."'");

				break;

			case 'saveworkplan':

				if (!appcontrol_checklogin() || !appcontrol_hasedit_permission("form1_GroupFormationReport")) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}

				$workplan = array(
					"project_code" => getpostvar("project_code"),
					"outcome" => getpostvar("outcome"),
					"output_number" => intval(getpostvar("output_number")),
					"activity_number" => intval(getpostvar("activity_number")),
					"activity_name" => getpostvar("activity_name"),
					"planned_start" => date("Y-m-d", strtotime(getpostvar("planned_start")) ),
					"planned_end" => date("Y-m-d", strtotime(getpostvar("planned_end")) ),
					"actual_start" => date("Y-m-d", strtotime(getpostvar("actual_start")) ),
					"actual_end" => date("Y-m-d", strtotime(getpostvar("actual_end")) ),
					"total_target" => floatval(getpostvar("total_target")),
					"achievement" => getpostvar("achievement"),
					"progress" => getpostvar("progress"),
					"activity_status" => getpostvar("activity_status"),
					"comments" => getpostvar("comments")
				);

				$id = 0;

				if( intval(getpostvar("id")) > 0 ) {
					$id = getpostvar("id");
				}

				global $appcontroldb;

				if( $id > 0 ) {

					$appcontroldb->where("id", $id);
					$appcontroldb->update("workplans", $workplan);

				} else {
					
					$id = $appcontroldb->insert("workplans", $workplan);

					echo json_encode(array("id"=>$id));

				}

			break;
			
			case 'appcontrol-login':

				if (appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol/dashboard");
					exit;
				}

				$hasError = false;
				$error = "";

				if( isset( $_POST["username"] ) ) {
					$user = wp_authenticate( $_POST["username"], $_POST["password"] );
					if( $user instanceof WP_Error ) {
						$hasError = true;
						$error = "Username or password is incorrect!";
					} else {
						$_SESSION["user"] = $user;
						appcontrol_frontredirect("/appcontrol/dashboard");
					}
				}

				require_once ( APPCONTROL_PATH . "inc/adminlte/pages/" . get_query_var("appcontrolroute") . ".php" );

			break;
			case 'reportdata':
			case 'showgroup':
			case 'formdata':
			case 'forms':
			case 'dashboard':
			case 'surveycompleted-all':
			case 'dailysurveycompleted-all':
			case 'mapactivities':
			case 'work-plan':
			case 'reports':
			case 'formmapdata':
        		if (!appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}
				$usr = appcontrol_sessionuser();
				require_once ( APPCONTROL_PATH . "inc/adminlte/pages/" . get_query_var("appcontrolroute") . ".php" );
				break;

			case 'deleteform':
				
				if (!appcontrol_checklogin() || !appcontrol_hasedit_permission(get_query_var("formid"))) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}

				appcontrol_deleteformrow(
					get_query_var("formid"),
					get_query_var("row")
				);

				appcontrol_frontredirect("/appcontrol/formdata/". get_query_var("formid"));

				break;

			case 'editform':
				if (!appcontrol_checklogin() || !appcontrol_hasedit_permission(get_query_var("formid"))) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}
				$usr = appcontrol_sessionuser();
				$form = getform_by_id2(get_query_var("formid"));

				if ( getpostvar("saveformdata") == 1 ) {

					$formfields = array();

					foreach (getpostvar("formfields") as $k => $val) {
						//if(in_array($k, $form["formfields"])) {
							$formfields[$k] = $val;
						//}
					}

					global $appcontroldb;

					$appcontroldb->where( '_URI', get_query_var("row") );
					$appcontroldb->update( $form["tablename"], $formfields );
					
					echo "<script>location=location.href</script>";
					exit;

				}
				
				require_once ( APPCONTROL_PATH . "inc/adminlte/pages/" . get_query_var("appcontrolroute") . ".php" );
				break;

			case 'rowview':
				if (!appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}
				$usr = appcontrol_sessionuser();
				
				$form = getform_by_id(get_query_var("formid"));

				require_once ( APPCONTROL_PATH . "inc/adminlte/pages/" . get_query_var("appcontrolroute") . ".php" );
				break;

			case 'exportform':
				if (!appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}
				$usr = appcontrol_sessionuser();

				$formid = get_query_var("formid");
				$forms = appcontrol_getforms2();
				foreach ($forms as $k => $form) {
					if($form["formid"] == $formid) {
						appcontrol_exportform2( $form["tablename"] );
						break;
					}
				}
			break;

			case 'exportformkml':

				if (!appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}

				$formid = get_query_var("formid");

				$locationdata = appcontrol_locationdatabytbl( $formid );
				/*$forms = appcontrol_getforms2();
				foreach ($forms as $k => $form) {
					if($form["formid"] == $formid) {
						appcontrol_exportformkml( $form["tablename"] );
						break;
					}
				}*/

				header('Content-Type: text/application');
				header('Content-Disposition: attachment; filename="'. $formid .'.kml"');

				require_once ( APPCONTROL_PATH . "inc/adminlte/pages/" . get_query_var("appcontrolroute") . ".php" );

				break;

			case 'exportworkplan':
				
				if (!appcontrol_checklogin()) {
					appcontrol_frontredirect("/appcontrol-login");
					exit;
				}

				appcontrol_exportworkplan();

				break;

			// case ''
			
			default:
				# code...
				break;

		}
		
		exit;

	}
}

add_action('parse_query', 'appcontrol_parsequery');