<h1 class="page-Title">
	Edit Report
</h1>

<h4>Report name: <?= $report["reportname"] ?></h4>
<h4>Form: <?= $form["title"] ?></h4>

<h6>Select Columns below you want to show in Reports: </h6>

<div class="col-md-12">
	
	<form class="form" method="POST" action="<?= appcontrol_url("appcontrol-formeditreport&reportid=". $report["id"]) ?>">

		<?php foreach ($form["formfields"] as $formfield): ?>

		<div class="col-md-4">

			<label style="color: #000;font-weight: normal;">

				<input type="checkbox" name="formfields[]" value="<?= $formfield ?>" <?= in_array($formfield, $report["columns"]) ? "checked" : "" ?> /> <span style="position: relative;top: 2.5px;"><?= strtoupper($formfield) ?></span>

			</label>
			
		</div>
			
		<?php endforeach ?>

		<div class="clearfix"></div>

		<input class="btn btn-primary pull-right" type="submit" value="Update" />

	</form>

</div>