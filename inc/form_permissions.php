<div class="col-md-12">
	<h1 class="page-title">
		Form Permissions
	</h1>

	<h4>Form name: <?= $form["title"] ?></h4>

	<form class="form" method="post" action="<?= appcontrol_url("appcontrol-formpermission&formid=". $form["formid"]) ?>">

	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>
					Username
				</th>
				<th>
					Permissions
				</th>
			</tr>
		</thead>
		<tbody>
			<?php $_permissions = array( "view_data" => "View Data", "edit_data" => "Edit Data" ); ?>
			<?php foreach (get_appcontrolusers() as $k => $user): ?>
			<tr>
				<td>
					<?php echo $k+1 ?>
				</td>
				<td>
					<?php echo $user->display_name  ?>
				</td>
				<td>
					<?php foreach ($_permissions as $_permission => $pname) { ?>
					<label> <input type="checkbox" name="<?= $_permission ?>[<?php echo $user->ID  ?>]" value="1" <?= get_user_meta($user->ID,$form["formid"]."_".$_permission, true) == "1" ? "checked" : "" ?> /> <?= $pname ?></label>
					<?php } ?>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<input type="hidden" name="formid" value="<?= $form["formid"] ?>" />
	<input type="hidden" name="savepermissions" value="1" />

	<input class="btn btn-primary pull-right" type="submit" name="Save Permissions" />

	</form>
</div>