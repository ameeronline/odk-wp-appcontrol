<?php

add_action('wp_dashboard_setup', array(
	'Graph1_Widget',
	'init'
));

class Graph1_Widget {
	
	const wid = 'appcontrol_graph1';

	public static function init() {

		self::update_dashboard_widget_options(self::wid, //The  widget id
			array( //Associative array of options & default values
				'example_number' => 42
			), true //Add only (will not update existing options)
		);
								
		//Register the widget...
		wp_add_dashboard_widget(self::wid, //A unique slug/ID
			__('Appcontrol Graph', 'nouveau'), //Visible name for the widget
			array(
				'Graph1_Widget',
				'widget'
			), //Callback for the main widget content
			array(
				'Graph1_Widget',
				'config'
			) //Optional callback for widget configuration content
		);

	}

	public static function widget()
	{
		require_once(plugin_dir_path( __FILE__ ) . 'graph1/widget.php');
	}

	public static function config()
	{
		require_once(plugin_dir_path( __FILE__ ) . 'graph1/widget-config.php');
	}

	public static function get_dashboard_widget_options($widget_id = '')
	{
		$opts = get_option('dashboard_widget_options');
		
		if (empty($widget_id))
			return $opts;
		
		if (isset($opts[$widget_id]))
			return $opts[$widget_id];
		
		return false;
	}

	public static function get_dashboard_widget_option($widget_id, $option, $default = NULL)
	{

		$opts = self::get_dashboard_widget_options($widget_id);
		
		if (!$opts)
			return false;
		
		if (isset($opts[$option]) && !empty($opts[$option]))
			return $opts[$option];
		else
			return (isset($default)) ? $default : false;
					
	}

	public static function update_dashboard_widget_options($widget_id, $args = array(), $add_only = false)
	{
		
		$opts = get_option('dashboard_widget_options');
		
		$w_opts = (isset($opts[$widget_id])) ? $opts[$widget_id] : array();
		
		if ($add_only) {
			$opts[$widget_id] = array_merge($args, $w_opts);
		} else {
			$opts[$widget_id] = array_merge($w_opts, $args);
		}
		
		return update_option('dashboard_widget_options', $opts);

	}


}