<?php

	$number = ( isset( $_POST['number'] ) ) ? stripslashes( $_POST['number'] ) : '';

	if ( $number != '' ) {
		self::update_dashboard_widget_options(
			self::wid,
			array(
				'example_number' => $number,
			)
		);
	}

?>
<p>This is an example dashboard widget!</p>
<p>This is the configuration part of the widget, and can be found and edited from <tt><?php echo __FILE__ ?></tt></p>
<input type="text" name="number" />