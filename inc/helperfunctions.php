<?php

use phpFastCache\CacheManager;

CacheManager::setDefaultConfig([
	"path" => APPCONTROL_PATH.'cache'
]);

$sheet_ids = array(
	"1fB-ZCv_XqbUAP2-7j7lAD4cxI8SU3EKTWpEYkwYaays",
	"1hbuzfTuNWMiG78Sg4WQAPChbTXIpaIV1Qi8s7uJlt6Q",
	"1qbs-djB-m9DvuzRjoN7iTyKKthPPJqt-RBA4DjHrAew",
	"1SYZHEWn8TdJ6xQN8zu6urmupFwjEBeW_soUXNJTlBoY"
);

global $alltables;
global $reporttables;
global $tablesfields;
global $othertables;

$alltables = array(
	"FORM1_GROUP_FORMATION_REPORT_CORE" => array("form1_GroupFormationReport","Form 1: Group Formation Form"),
	"FORM18EPORT_CORE" => array("form2_UPIDPFormationReport", "Form 2: UPIDP Formation Report"),
	"FORM3_AWARENESS_REPORT_CORE" => array("form3_AwarenessReport", "Form 3: Awareness Activity"),
	"FORM4A_INFRA_INITIATION_REPORT_CORE" => array("form4a_InfraInitiationReport", "Form 4a: Infrastructure Intervention Initiation Report"),
	"FORM4B_INFRA_COMPLETION_REPORT_CORE" => array("form4b_InfraCompletionReport", "Form 4b: Infrastructure Intervention Completion Report"),
	"FORM5_25EPORT_CORE" => array("form5_LivelihoodInterventionReport", "Form 5: Livelihood Intervention Report"),
	"FORM6_LOBBY_MEETING_REPORT_CORE" => array("form6_LobbyMeetingReport", "Form 6: Lobby Meeting Report"),
	"FORM7_SUCCESS_STORY_CORE" => array("form7_SuccessStory", "Form 7: Success Story"),
	"FORM8_MONITORING_REPORT_CORE" => array("form8_monitoring_report", "Form 8: Monitoring Report")
);

$reporttables = array(
	"FORM1_GROUP_FORMATION_REPORT_CORE",
	"FORM18EPORT_CORE",
	"FORM3_AWARENESS_REPORT_CORE",
	"FORM4A_INFRA_INITIATION_REPORT_CORE",
	"FORM4B_INFRA_COMPLETION_REPORT_CORE",
	"FORM5_25EPORT_CORE",
	"FORM6_LOBBY_MEETING_REPORT_CORE"
);

$image_tables = array(
	"FORM1_GROUP_FORMATION_REPORT_CORE" => array(
		"FORM1_GROUP_FORMATION_REPORT_PICTURE_BLB"
	),
	"FORM18EPORT_CORE" => array(
		"FORM18EPORT_EVENT_PICTURE1_BLB",
		"FORM18EPORT_EVENT_PICTURE2_BLB",
		"FORM18EPORT_EVENT_PICTURE3_BLB",
		"FORM18EPORT_EVENT_PICTURE4_BLB",
		"FORM18EPORT_EVENT_PICTURE5_BLB"
	),
	"FORM3_AWARENESS_REPORT_CORE" => array(
		"FORM3_AWARENESS_REPORT_EVENT_PICTURE1_BLB",
		"FORM3_AWARENESS_REPORT_EVENT_PICTURE2_BLB",
		"FORM3_AWARENESS_REPORT_EVENT_PICTURE3_BLB",
		"FORM3_AWARENESS_REPORT_EVENT_PICTURE4_BLB",
		"FORM3_AWARENESS_REPORT_EVENT_PICTURE5_BLB"
	),
	"FORM4A_INFRA_INITIATION_REPORT_CORE" => array(
		"FORM4A_INFRA_INITIATION_REPORT_PICTURE1_BLB",
		"FORM4A_INFRA_INITIATION_REPORT_PICTURE2_BLB",
		"FORM4A_INFRA_INITIATION_REPORT_PICTURE3_BLB",
		"FORM4A_INFRA_INITIATION_REPORT_PICTURE4_BLB",
		"FORM4A_INFRA_INITIATION_REPORT_PICTURE5_BLB"
	),
	"FORM4B_INFRA_COMPLETION_REPORT_CORE" => array(
		"FORM4B_INFRA_COMPLETION_REPORT_PICTURE1_BLB",
		"FORM4B_INFRA_COMPLETION_REPORT_PICTURE2_BLB",
		"FORM4B_INFRA_COMPLETION_REPORT_PICTURE3_BLB",
		"FORM4B_INFRA_COMPLETION_REPORT_PICTURE4_BLB",
		"FORM4B_INFRA_COMPLETION_REPORT_PICTURE5_BLB"
	),
	"FORM5_25EPORT_CORE" => array(
		"FORM5_25EPORT_BENEFICIARY_BENEFICIARY_PHOTO_BLB"
	),
	"FORM6_LOBBY_MEETING_REPORT_CORE" => array(
		"FORM6_LOBBY_MEETING_REPORT_MEETING_PICTURE_1_BLB"
	)
);

//ATTENDANCE_PARTICIPANTS_TOTAL

$othertables["FORM1_GROUP_FORMATION_REPORT_CORE"] = array(
	"member_details" => "FORM1_GROUP_FORMATION_REPORT_MEMBER_DETAILS"
);

$tablesfields = array();

$tablesfields["FORM1_GROUP_FORMATION_REPORT_MEMBER_DETAILS"] = array(

	"GROUP_MEMBER_NAME" => array(
		"name" => "Name",
		"type" => "text"
	),

	"GROUP_MEMBER_CONTACT_NO" => array(
		"name" => "Contact no.",
		"type" => "text"
	),

	"GROUP_MEMBER_EDUCATION" => array(
		"name" => "Education",
		"type" => "select",
		"options" => array(
			"Illiterate" => "Illiterate",
			"Primary" => "Primary",
			"Secondary" => "Secondary",
			"Middle" => "Middle",
			"Matric" => "Matric",
			"Intermediate" => "Intermediate",
			"Gradute" => "Gradute",
			"Post-graduation" => "Post-graduation"
		)
	),

	"GROUP_MEMBER_GENDER" => array(
		"name" => "Gender",
		"type" => "select",
		"options" => array(
			"Male" => "Male",
			"Female" => "Female"
		)
	),

	"GROUP_MEMBER_DESIGNATION" => array(
		"name" => "Designation",
		"type" => "text"
	),

	"GROUP_MEMBER_AGE" => array(
		"name" => "Age",
		"type" => "select",
		"options" => array(
			"Less than 14 years" => "Less than 14 years",
			"15 - 19" => "15 - 19",
			"20 - 24" => "20 - 24",
			"25 - 29" => "25 - 29",
			"30 - 34" => "30 - 34",
			"35 - 40" => "35 - 40",
			"41 - 45" => "41 - 45",
			"Above 45 years" => "Above 45 years"
		)
	),

	"GROUP_MEMBER_POSITION_ASSIGNED" => array(
		"name" => "Assigned",
		"type" => "select",
		"options" => array(
			"President" => "President",
			"Vice President" => "Vice President",
			"General Secretary" => "General Secretary",
			"Member" => "Member"
		)
	),

	"GROUP_MEMBER_DEPARTMENT" => array(
		"name" => "Department",
		"type" => "text"
	),

	"GROUP_MEMBER_NIC" => array(
		"name" => "NIC",
		"type" => "text"
	)

);

$tablesfields["FORM1_GROUP_FORMATION_REPORT_CORE"] = array(
	"GROUP_TYPE" => array(
		"name" => "Group type",
		"type" => "select",
		"options" => array(
			"ECG" => "ECG",
			"Sub_WASH" => "Sub_WASH",
			"PM&EG" => "PM&amp;EG",
			"Common trade and skills group" => "Common trade and skills group"
		)
	),
	"REMARKS" => array(
		"name" => "Remarks",
		"type" => "text"
	),
	"UC" => array(
		"name" => "UC",
		"type" => "text"
	),
	"DISTRICT" => array(
		"name" => "District",
		"type" => "select",
		"options" => array(
			"Lahore" => "Lahore",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"WARD" => array(
		"name" => "Ward",
		"type" => "select",
		array(
			"Ward 1" => "Ward 1",
			"Ward 2" => "Ward 2",
			"Ward 3" => "Ward 3",
			"Ward 4" => "Ward 4",
			"Ward 5" => "Ward 5",
			"Ward 6" => "Ward 6"
		)
	),
	"SURVEY_COMPLETE" => array(
		"name" => "Survey Complete",
		"type" => "select",
		array(
			"Yes" => "Yes",
			"No" => "No"
		)
	),
	"TEHSIL" => array(
		"name" => "Tehsil",
		"type" => "select",
		"options" => array(
			"Shalamar Town" => "Shalamar Town",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"MUHALLA" => array(
		"name" => "Muhalla",
		"type" => "text"
	),
	"OUTCOME" => array(
		"name" => "Outcome",
		"type" => "int"
	),
	"OUTPUT" => array(
		"name" => "Output",
		"type" => "int"
	),
	"PROJECT_CODE" => array(
		"name" => "Project code",
		"type" => "text"
	),
	"STREET" => array(
		"name" => "Street",
		"type" => "text"
	),
	"GROUP_NAME" => array(
		"name" => "Group name",
		"type" => "text"
	),
	"DATE" => array(
		"name" => "Date",
		"type" => "date"
	),
	"FACILITATOR" => array(
		"name" => "Facilitator",
		"type" => "text"
	),
	"ACTIVITY" => array(
		"name" => "Activity",
		"type" => "int"
	)
);

$tablesfields["FORM18EPORT_CORE"] = array(
	"DATE" => array(
		"name" => "Date",
		"type" => "text"
	),
	"PROJECT_CODE" => array(
		"name" => "Project code",
		"type" => "text"
	),
	"OUTCOME" => array(
		"name" => "Outcome",
		"type" => "int"
	),
	"OUTPUT" => array(
		"name" => "Output",
		"type" => "int"
	),
	"ACTIVITY" => array(
		"name" => "Activity",
		"type" => "int"
	),
	"FACILITATOR" => array(
		"name" => "Facilitator",
		"type" => "text"
	),
	"UPIDP_LEVEL" => array(
		"name" => "UPIDP Level",
		"type" => "select",
		"options" => array(
			"UC_Ward" => "UC / Ward",
			"Muhalla" => "Muhalla"
		)
	),
	"UC" => array(
		"name" => "UC",
		"type" => "text"
	),
	"WARD" => array(
		"name" => "Ward",
		"type" => "select",
		"options" => array(
			"Ward 1" => "Ward 1",
			"Ward 2" => "Ward 2",
			"Ward 3" => "Ward 3",
			"Ward 4" => "Ward 4",
			"Ward 5" => "Ward 5",
			"Ward 6" => "Ward 6"
		)
	),
	"MUHALLA" => array(
		"name" => "Muhalla",
		"type" => "text"
	),
	"STREET" => array(
		"name" => "Street",
		"type" => "text"
	),
	"WAY_FORWARD" => array(
		"name" => "Way Forward",
		"type" => "text"
	),
	"REMARKS" => array(
		"name" => "Remarks",
		"type" => "text"
	),
	"REPORT_COMPLETE" => array(
		"name" => "Report Complete",
		"type" => "select",
		"options" => array(
			"Yes" => "Yes",
			"No" => "No"
		)
	)
);

$tablesfields["FORM3_AWARENESS_REPORT_CORE"] = array(
	"DATE" => array(
		"name" => "Date",
		"type" => "datetime"
	),
	"PROJECT_CODE" => array(
		"name" => "Project code",
		"type" => "text"
	),
	"OUTCOME" => array(
		"name" => "Outcome",
		"type" => "int"
	),
	"OUTPUT" => array(
		"name" => "Output",
		"type" => "int"
	),
	"ACTIVITY" => array(
		"name" => "Activity",
		"type" => "int"
	),
	"EVENTDATE" => array(
		"name" => "Event date",
		"type" => "datetime"
	),
	"EVENTTYPE" => array(
		"name" => "Event type",
		"type" => "select",
		"options" => array(
			"Campaign" => "Campaign",
			"Day celebration" => "Day celebration",
			"Public Walk" => "Public Walk"
		)
	),
	"EVENT_TITLE" => array(
		"name" => "Event title",
		"type" => "text"
	),
	"FACILITATOR" => array(
		"name" => "Facilitator",
		"type" => "text"
	),
	"DISTRICT" => array(
		"name" => "District",
		"type" => "select",
		"options" => array(
			"Lahore" => "Lahore",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"TEHSIL" => array(
		"name" => "Tehsil",
		"type" => "select",
		"options" => array(
			"Shalamar Town" => "Shalamar Town",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"UC" => array(
		"name" => "UC",
		"type" => "text"
	),
	"WARD" => array(
		"name" => "Ward",
		"type" => "select",
		"options" => array(
			"Ward 1" => "Ward 1",
			"Ward 2" => "Ward 2",
			"Ward 3" => "Ward 3",
			"Ward 4" => "Ward 4",
			"Ward 5" => "Ward 5",
			"Ward 6" => "Ward 6"
		)
	),
	"MUHALLA" => array(
		"name" => "Muhalla",
		"type" => "text"
	),
	"STREET" => array(
		"name" => "Street",
		"type" => "text"
	),
	"EVENTSUBJECT" => array(
		"name" => "Event Subject",
		"type" => "text"
	),
	"REMARKS" => array(
		"name" => "Remarks",
		"type" => "text"
	),
	"REPORT_COMPLETE" => array(
		"name" => "Report Complete",
		"type" => "select",
		"options" => array(
			"Yes" => "Yes",
			"No" => "No"
		)
	)
);

$tablesfields["FORM4A_INFRA_INITIATION_REPORT_CORE"] = array(
	"DATE" => array(
		"name" => "Date",
		"type" => "datetime"
	),
	"PROJECT_CODE" => array(
		"name" => "Project code",
		"type" => "text"
	),
	"OUTCOME" => array(
		"name" => "Outcome",
		"type" => "int"
	),
	"OUTPUT" => array(
		"name" => "Output",
		"type" => "int"
	),
	"ACTIVITY" => array(
		"name" => "Activity",
		"type" => "int"
	),
	"REPORTINGDATE" => array(
		"name" => "Reporting Date",
		"type" => "datetime"
	),
	"FACILITATOR" => array(
		"name" => "Facilitator",
		"type" => "text"
	),
	"DISTRICT" => array(
		"name" => "District",
		"type" => "select",
		"options" => array(
			"Lahore" => "Lahore",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"TEHSIL" => array(
		"name" => "Tehsil",
		"type" => "select",
		"options" => array(
			"Shalamar Town" => "Shalamar Town",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"UC" => array(
		"name" => "UC",
		"type" => "text"
	),
	"WARD" => array(
		"name" => "Ward",
		"type" => "select",
		"options" => array(
			"Ward 1" => "Ward 1",
			"Ward 2" => "Ward 2",
			"Ward 3" => "Ward 3",
			"Ward 4" => "Ward 4",
			"Ward 5" => "Ward 5",
			"Ward 6" => "Ward 6"
		)
	),
	"MUHALLA" => array(
		"name" => "Muhalla",
		"type" => "text"
	),
	"STREET" => array(
		"name" => "Street",
		"type" => "text"
	),
	"INTERVENTION_TYPE" => array(
		"name" => "Intervention type",
		"type" => "select",
		"options" => array(
			"Rehabilitation of existing infrastructure" => "Rehabilitation of existing infrastructure",
			"New development" => "New development"
		)
	),
	"SCHEME_TYPE" => array(
		"name" => "Scheme type",
		"type" => "select",
		"options" => array(
			"Water supply" => "Water supply",
			"Filtration plant" => "Filtration plant",
			"Sewerage" => "Sewerage",
			"Drainage" => "Drainage",
			"Communal water tap" => "Communal water tap",
			"Solid waste" => "Solid waste",
			"Public Toilet" => "Public Toilet",
			"Other" => "Other"
		)
	),
	"SCHEME_NAME" => array(
		"name" => "Scheme name",
		"type" => "text"
	),
	"SCHEME_NUMBER" => array(
		"name" => "Scheme Number",
		"type" => "int"
	),
	"RESPONSIBLE_ECG" => array(
		"name" => "Responsible ECG",
		"type" => "text"
	),
	"RESPONSIBLE_SUB_WASH" => array(
		"name" => "Responsible Sub_Wash",
		"type" => "text"
	),
	"RESPONSIBLE_PMG" => array(
		"name" => "Responsible PMG",
		"type" => "text"
	),
	"INSTITUTIONS_INVOLVED" => array(
		"name" => "Institutions Involved",
		"type" => "select",
		"options" => array(
			"Public works" => "Public works",
			"TMA" => "TMA",
			"UC Chairman" => "UC Chairman",
			"General Councilor" => "General Councilor",
			"Other" => "Other"
		)
	),
	"REMARKS" => array(
		"name" => "Remarks",
		"type" => "text"
	),
	"REPORT_COMPLETE" => array(
		"name" => "Report Complete",
		"type" => "select",
		"options" => array(
			"Yes" => "Yes",
			"No" => "No"
		)
	)
);

$tablesfields["FORM4B_INFRA_COMPLETION_REPORT_CORE"] = array(
	"DATE" => array(
		"name" => "Date",
		"type" => "datetime"
	),
	"PROJECT_CODE" => array(
		"name" => "Project code",
		"type" => "text"
	),
	"OUTCOME" => array(
		"name" => "Outcome",
		"type" => "int"
	),
	"OUTPUT" => array(
		"name" => "Output",
		"type" => "int"
	),
	"ACTIVITY" => array(
		"name" => "Activity",
		"type" => "int"
	),
	"REPORTINGDATE" => array(
		"name" => "Reporting Date",
		"type" => "datetime"
	),
	"FACILITATOR" => array(
		"name" => "Facilitator",
		"type" => "text"
	),
	"DISTRICT" => array(
		"name" => "District",
		"type" => "select",
		"options" => array(
			"Lahore" => "Lahore",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"TEHSIL" => array(
		"name" => "Tehsil",
		"type" => "select",
		"options" => array(
			"Shalamar Town" => "Shalamar Town",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"UC" => array(
		"name" => "UC",
		"type" => "text"
	),
	"WARD" => array(
		"name" => "Ward",
		"type" => "select",
		"options" => array(
			"Ward 1" => "Ward 1",
			"Ward 2" => "Ward 2",
			"Ward 3" => "Ward 3",
			"Ward 4" => "Ward 4",
			"Ward 5" => "Ward 5",
			"Ward 6" => "Ward 6"
		)
	),
	"MUHALLA" => array(
		"name" => "Muhalla",
		"type" => "text"
	),
	"STREET" => array(
		"name" => "Street",
		"type" => "text"
	),
	"INTERVENTION_TYPE" => array(
		"name" => "Intervention type",
		"type" => "select",
		"options" => array(
			"Rehabilitation of existing infrastructure" => "Rehabilitation of existing infrastructure",
			"New development" => "New development"
		)
	),
	"SCHEME_TYPE" => array(
		"name" => "Scheme type",
		"type" => "select",
		"options" => array(
			"Water supply" => "Water supply",
			"Filtration plant" => "Filtration plant",
			"Sewerage" => "Sewerage",
			"Drainage" => "Drainage",
			"Communal water tap" => "Communal water tap",
			"Solid waste" => "Solid waste",
			"Public Toilet" => "Public Toilet",
			"Other" => "Other"
		)
	),
	"SCHEME_NAME" => array(
		"name" => "Scheme name",
		"type" => "text"
	),
	"SCHEME_NUMBER" => array(
		"name" => "Scheme Number",
		"type" => "int"
	),
	"COMPLETION_DATE" => array(
		"name" => "Completion date",
		"type" => "datetime"
	),
	"COMPLETION_AS_PER_PLAN" => array(
		"name" => "Completion As Per Plan",
		"type" => "select",
		"options" => array(
			"Yes" => "Yes",
			"No" => "No"
		)
	),
	"VARIATION" => array(
		"name" => "Variation",
		"type" => "text"
	),
	"POST_IMPLEMENTATION_OPERATION" => array(
		"name" => "Post Implementation Operation",
		"type" => "select",
		"options" => array(
			"Community_managed" => "Community_managed",
			"Handed_over_to_individual" => "Handed_over_to_individual",
			"PPP" => "PPP"
		)
	),
	"MANAGING_GROUP_TYPE" => array(
		"name" => "Managing Group Type",
		"type" => "select",
		"options" => array(
			"ECG_group" => "ECG group",
			"Muhalla_committee" => "Muhalla committee",
			"Masjid_committee" => "Masjid committee",
			"other" => "Other"
		)
	),
	"REMARKS" => array(
		"name" => "Remarks",
		"type" => "text"
	),
	"REPORT_COMPLETE" => array(
		"name" => "Report Complete",
		"type" => "select",
		"options" => array(
			"Yes" => "Yes",
			"No" => "No"
		)
	)
);

$tablesfields["FORM5_25EPORT_CORE"] = array(
	"DATE" => array(
		"name" => "Date",
		"type" => "datetime"
	),
	"PROJECT_CODE" => array(
		"name" => "Project code",
		"type" => "text"
	),
	"OUTCOME" => array(
		"name" => "Outcome",
		"type" => "int"
	),
	"OUTPUT" => array(
		"name" => "Output",
		"type" => "int"
	),
	"ACTIVITY" => array(
		"name" => "Activity",
		"type" => "int"
	),
	"REPORTINGDATE" => array(
		"name" => "Reporting Date",
		"type" => "datetime"
	),
	"FACILITATOR" => array(
		"name" => "Facilitator",
		"type" => "text"
	),
	"BENEFICIARY_NAME" => array(
		"name" => "Beneficiary name",
		"type" => "text"
	),
	"BENEFICIARY_SEX" => array(
		"name" => "Beneficiary gender",
		"type" => "select",
		"options" => array(
			"Male" => "Male",
			"Female" => "Female"
		)
	),
	"BENEFICIARY_AGE" => array(
		"name" => "Beneficiary Age",
		"type" => "text"
	),
	"DISTRICT" => array(
		"name" => "District",
		"type" => "select",
		"options" => array(
			"Lahore" => "Lahore",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"TEHSIL" => array(
		"name" => "Tehsil",
		"type" => "select",
		"options" => array(
			"Shalamar Town" => "Shalamar Town",
			"Muzaffargarh" => "Muzaffargarh"
		)
	),
	"UC" => array(
		"name" => "UC",
		"type" => "text"
	),
	"WARD" => array(
		"name" => "Ward",
		"type" => "select",
		"options" => array(
			"Ward 1" => "Ward 1",
			"Ward 2" => "Ward 2",
			"Ward 3" => "Ward 3",
			"Ward 4" => "Ward 4",
			"Ward 5" => "Ward 5",
			"Ward 6" => "Ward 6"
		)
	),
	"MUHALLA" => array(
		"name" => "Muhalla",
		"type" => "text"
	),
	"STREET" => array(
		"name" => "Street",
		"type" => "text"
	),
	"INTERVENTION_TYPE" => array(
		"name" => "Intervention type",
		"type" => "select",
		"options" => array(
			"Skills_development_training" => "Skills development training",
			"Linkage_with_external_institution" => "Linkage with external institution"
		)
	),
	"REMARKS" => array(
		"name" => "Remarks",
		"type" => "text"
	),
	"REPORT_COMPLETE" => array(
		"name" => "Report Complete",
		"type" => "select",
		"options" => array(
			"Yes" => "Yes",
			"No" => "No"
		)
	)
);

$tablesfields["FORM6_LOBBY_MEETING_REPORT_CORE"] = array(
	"DATE" => array(
		"name" => "Date",
		"type" => "datetime"
	),
	"PROJECT_CODE" => array(
		"name" => "Project code",
		"type" => "text"
	),
	"OUTCOME" => array(
		"name" => "Outcome",
		"type" => "int"
	),
	"OUTPUT" => array(
		"name" => "Output",
		"type" => "int"
	),
	"ACTIVITY" => array(
		"name" => "Activity",
		"type" => "int"
	),
	"REPORTINGDATE" => array(
		"name" => "Reporting Date",
		"type" => "datetime"
	),
	"MEETING_PLACE" => array(
		"name" => "Meeting Place",
		"type" => "text"
	),
	"PROGRAM" => array(
		"name" => "Meeting held under Program",
		"type" => "select",
		"options" => array(
			"urban_wash" => "Urban WASH",
			"other" => "Other"
		)
	),
	"MEETING_OBJECTIVE" => array(
		"name" => "Meeting Objective",
		"type" => "text"
	),
	"SENIOR_PERSON_NAME" => array(
		"name" => "Senior Name",
		"type" => "text"
	),
	"SENIOR_PERSON_DEPARTMENT" => array(
		"name" => "Senior Department",
		"type" => "text"
	),
	"SENIOR_PERSON_DESIGNATION" => array(
		"name" => "Senior Designation",
		"type" => "text"
	),
	"THEMES_COVERED" => array(
		"name" => "Themes covered",
		"type" => "text"
	),
	"ACTION_COMITTED" => array(
		"name" => "Action Comitted with deadliens",
		"type" => "text"
	),
	"ALIGNMENT_LEVEL" => array(
		"name" => "Level of alignment on our issues",
		"type" => "int"
	),
	"UNDERSTANDING_LEVEL" => array(
		"name" => "Level of understanding of issues",
		"type" => "int"
	),
	"INFLUENCE_LEVEL" => array(
		"name" => "Level of influence on these issues",
		"type" => "int"
	),
	"OBSERVATIONS" => array(
		"name" => "Additional observations/comments",
		"type" => "text"
	),
	"WAY_FORWARD" => array(
		"name" => "Way forward/Next agenda point",
		"type" => "text"
	),
	"REPORT_COMPLETE" => array(
		"name" => "Report Complete",
		"type" => "select",
		"options" => array(
			"Yes" => "Yes",
			"No" => "No"
		)
	)
);

if ( !function_exists("appcontrol_getprojectcodes") ) {
	function appcontrol_getprojectcodes() {

		global $appcontroldb;
		global $tablesfields;

		$sql = "SELECT PROJECT_CODE FROM ( ";

		$tables_sql = array();

		foreach ($tablesfields as $tablename => $tablesfield) {
			if( isset($tablesfield["PROJECT_CODE"]) )
				$tables_sql[] = "(SELECT PROJECT_CODE FROM {$tablename})";
		}

		$sql .= implode(" UNION ", $tables_sql);

		$sql .= ") d";

		$project_codes = $appcontroldb->rawQuery($sql);

		return $project_codes;

	}
}

if ( !function_exists("appcontrol_hasview_permission") ) {
	function appcontrol_hasview_permission($formid = "form1_GroupFormationReport") {

		$user_id = $_SESSION["user"]->ID;

		if(get_user_meta($user_id,$formid."_view_data", true) == "1")
			return true;

		return false;

	}
}

if ( !function_exists("appcontrol_hasedit_permission") ) {
	function appcontrol_hasedit_permission($formid = "form1_GroupFormationReport") {

		$user_id = $_SESSION["user"]->ID;

		if(get_user_meta($user_id,$formid."_edit_data", true) == "1")
			return true;

		return false;

	}
}

if ( !function_exists("appcontrol_getonlylocationdata") ) {
	function appcontrol_getonlylocationdata() {

		global $appcontroldb;
		
		//"“form1, form2, form3, form4a, form5 and form6….core”"
		$locationtables = array(
			"FORM1_GROUP_FORMATION_REPORT_CORE",
        	"FORM18EPORT_CORE",
        	"FORM3_AWARENESS_REPORT_CORE",
        	"FORM4A_INFRA_INITIATION_REPORT_CORE",
        	"FORM5_25EPORT_CORE",
        	"FORM6_LOBBY_MEETING_REPORT_CORE"
		);

		$sql = "SELECT LOCATION_LNG, LOCATION_LAT, PROJECT_CODE, ACTIVITY, OUTCOME FROM ( ";

		$tables_sql = array();

		foreach ($locationtables as $tablename) {
			$tables_sql[] = "(SELECT LOCATION_LNG, LOCATION_LAT, PROJECT_CODE, ACTIVITY, OUTCOME FROM {$tablename})";
		}

		$sql .= implode(" UNION ", $tables_sql);

		$sql .= ") d";

		$locationdata = $appcontroldb->rawQuery($sql);

		return $locationdata;

	}
}

if ( !function_exists("appcontrol_locationdatabytbl") ) {
	function appcontrol_locationdatabytbl( $formid ) {

		global $appcontroldb;
		
		//"“form1, form2, form3, form4a, form5 and form6….core”"

		$form = getform_by_id2($formid);

		$locationtables = array(
			$form["tablename"]
			/*"form1_group_formation_report_core",
			"form18eport_core",
			"form3_awareness_report_core",
			"form4a_infra_initiation_report_core",
			"form5_25eport_core",
			"form6_lobby_meeting_report_core"*/
		);

		$sql = "SELECT LOCATION_LNG, LOCATION_LAT FROM ( ";

		$tables_sql = array();

		foreach ($locationtables as $tablename) {
			$tables_sql[] = "(SELECT LOCATION_LNG, LOCATION_LAT FROM {$tablename})";
		}

		$sql .= implode(" UNION ", $tables_sql);

		$sql .= ") d";

		$locationdata = $appcontroldb->rawQuery($sql);

		return $locationdata;

	}
}


if (!function_exists("appcontrol_beneficiaries")) {
	function appcontrol_beneficiaries($project_code = "") {

		global $appcontroldb;
		global $tablesfields;

		global $othertables;

		$beneficiaries = 0;

		$memberDetailTable = $othertables["FORM1_GROUP_FORMATION_REPORT_CORE"]["member_details"];

		$memberCount = $appcontroldb->getOne($memberDetailTable, "count(*) as cnt");
		
		$beneficiaries += $memberCount["cnt"];

		if( $project_code != "" && isset( $tablesfields["FORM18EPORT_CORE"]["PROJECT_CODE"] ) ) {
			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form2Count = $appcontroldb->getOne("FORM18EPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_TOTAL) as cnt");
		} elseif( $project_code != "" ) {
			$form2Count["cnt"] = 0;
		} else {
			$form2Count = $appcontroldb->getOne("FORM18EPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_TOTAL) as cnt");
		}
		
		$beneficiaries += $form2Count["cnt"];

		$form3Count = $appcontroldb->getOne("FORM3_AWARENESS_REPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_TOTAL) as cnt");

		if( $project_code != "" && isset( $tablesfields["FORM3_AWARENESS_REPORT_CORE"]["PROJECT_CODE"] ) ) {
			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form3Count = $appcontroldb->getOne("FORM3_AWARENESS_REPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_TOTAL) as cnt");
		} elseif( $project_code != "" ) {
			$form3Count["cnt"] = 0;
		} else {
			$form3Count = $appcontroldb->getOne("FORM3_AWARENESS_REPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_TOTAL) as cnt");
		}
		
		$beneficiaries += $form3Count["cnt"];

		

		return $beneficiaries;

	}
}

if (!function_exists("appcontrol_beneficiarydetails")) {
	function appcontrol_beneficiarydetails($project_code = "") {

		global $appcontroldb;
		global $tablesfields;

		global $othertables;

		$beneficiaries = 0;

		$form1 = 0;
		$form2 = 0;
		$form3 = 0;
		$form4a = 0;
		$form5 = 0;

		$totalMales = 0;
		$totalFemales = 0;

		$memberDetailTable = $othertables["FORM1_GROUP_FORMATION_REPORT_CORE"]["member_details"];

		$appcontroldb->where("GROUP_MEMBER_GENDER", "Female");
		$femaleCount = $appcontroldb->getOne($memberDetailTable, "count(*) as cnt");
		$femaleCount = $femaleCount["cnt"];

		$beneficiaries += $femaleCount;
		$totalFemales += $femaleCount;

		$appcontroldb->where("GROUP_MEMBER_GENDER", "Male");
		$maleCount = $appcontroldb->getOne($memberDetailTable, "count(*) as cnt");
		$maleCount = $maleCount["cnt"];

		$beneficiaries += $maleCount;
		$totalMales += $maleCount;

		$form1 = $beneficiaries;

		if( $project_code != "" && isset( $tablesfields["FORM18EPORT_CORE"]["PROJECT_CODE"] ) ) {

			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form2Count = $appcontroldb->getOne("FORM18EPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_FEMALE) as cnt");
			$beneficiaries += $form2Count["cnt"];
			$form2 += $form2Count["cnt"];
			$totalFemales += $form2Count["cnt"];

			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form2Count = $appcontroldb->getOne("FORM18EPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_MALE) as cnt");
			$beneficiaries += $form2Count["cnt"];
			$form2 += $form2Count["cnt"];
			$totalMales += $form2Count["cnt"];

		} elseif( $project_code != "" ) {

			$form2Count["cnt"] = 0;

		} else {

			$form2Count = $appcontroldb->getOne("FORM18EPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_FEMALE) as cnt");
			$beneficiaries += $form2Count["cnt"];
			$form2 += $form2Count["cnt"];
			$totalFemales += $form2Count["cnt"];

			$form2Count = $appcontroldb->getOne("FORM18EPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_MALE) as cnt");
			$beneficiaries += $form2Count["cnt"];
			$form2 += $form2Count["cnt"];
			$totalMales += $form2Count["cnt"];

		}

		$form3Count = array();

		if( $project_code != "" && isset( $tablesfields["FORM3_AWARENESS_REPORT_CORE"]["PROJECT_CODE"] ) ) {

			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form3Count = $appcontroldb->getOne("FORM3_AWARENESS_REPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_MALE) as cnt");
			$beneficiaries += $form3Count["cnt"];
			$form3 += $form3Count["cnt"];
			$totalMales += $form3Count["cnt"];

			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form3Count = $appcontroldb->getOne("FORM3_AWARENESS_REPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_FEMALE) as cnt");
			$form3 += $form3Count["cnt"];
			$beneficiaries += $form3Count["cnt"];
			$totalFemales += $form3Count["cnt"];


		} elseif( $project_code != "" ) {
			$form3Count["cnt"] = 0;
		} else {
			
			$form3Count = $appcontroldb->getOne("FORM3_AWARENESS_REPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_MALE) as cnt");
			$beneficiaries += $form3Count["cnt"];
			$form3 += $form3Count["cnt"];
			$totalMales += $form3Count["cnt"];

			$form3Count = $appcontroldb->getOne("FORM3_AWARENESS_REPORT_CORE", "SUM(ATTENDANCE_PARTICIPANTS_FEMALE) as cnt");
			$beneficiaries += $form3Count["cnt"];
			$form3 += $form3Count["cnt"];
			$totalFemales += $form3Count["cnt"];

		}


		if( $project_code != "" && isset( $tablesfields["FORM4A_INFRA_INITIATION_REPORT_CORE"]["PROJECT_CODE"] ) ) {

			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form4Count = $appcontroldb->getOne("FORM4A_INFRA_INITIATION_REPORT_CORE", "SUM(ESTIMATED_BENEFICIARIES_MALE) as cnt");
			$beneficiaries += $form4Count["cnt"];
			$form4a += $form4Count["cnt"];
			$totalMales += $form4Count["cnt"];

			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form4Count = $appcontroldb->getOne("FORM4A_INFRA_INITIATION_REPORT_CORE", "SUM(ESTIMATED_BENEFICIARIES_FEMALE) as cnt");
			$beneficiaries += $form4Count["cnt"];
			$form4a += $form4Count["cnt"];
			$totalFemales += $form4Count["cnt"];


		} elseif( $project_code != "" ) {
			$form4Count["cnt"] = 0;
		} else {
			
			$form4Count = $appcontroldb->getOne("FORM4A_INFRA_INITIATION_REPORT_CORE", "SUM(ESTIMATED_BENEFICIARIES_MALE) as cnt");
			$beneficiaries += $form4Count["cnt"];
			$form4a += $form4Count["cnt"];
			$totalMales += $form4Count["cnt"];

			$form4Count = $appcontroldb->getOne("FORM4A_INFRA_INITIATION_REPORT_CORE", "SUM(ESTIMATED_BENEFICIARIES_FEMALE) as cnt");
			$beneficiaries += $form4Count["cnt"];
			$form4a += $form4Count["cnt"];
			$totalFemales += $form4Count["cnt"];

		}

		if( $project_code != "" && isset( $tablesfields["FORM5_25EPORT_CORE"]["PROJECT_CODE"] ) ) {

			$appcontroldb->where("PROJECT_CODE", $project_code);
			$form5Count = $appcontroldb->getOne("FORM5_25EPORT_CORE", "COUNT(*) as cnt");
			$form5 += $form5Count["cnt"];
			$beneficiaries += $form5Count["cnt"];


		} elseif( $project_code != "" ) {
			$form5Count["cnt"] = 0;
		} else {
			
			$form5Count = $appcontroldb->getOne("FORM5_25EPORT_CORE", "COUNT(*) as cnt");
			$form5 += $form5Count["cnt"];
			$beneficiaries += $form5Count["cnt"];

		}

		return array( "beneficiaries" => $beneficiaries, "male" => $totalMales, "female" => $totalFemales, "form1" => $form1,"form2" => $form2,"form3" => $form3, "form4a" => $form4a, "form5" => $form5 );

	}
}

if ( !function_exists("appcontrol_fetchWorkPlan") ) {
	function appcontrol_fetchWorkPlan( $project_code = "" ) {

		global $appcontroldb;

		if( $project_code != "" )
			$appcontroldb->where("project_code", $project_code);

		$workplans = $appcontroldb->get("workplans");

		return $workplans;

	}
}

if (!function_exists("DOMinnerHTML")) {
	function DOMinnerHTML(DOMNode $element) 
	{
		$innerHTML = ""; 
		$children  = $element->childNodes;

		foreach ($children as $child) 
		{ 
			$innerHTML .= $element->ownerDocument->saveHTML($child);
		}

		return $innerHTML; 
	}
}

if (!function_exists("appcontrol_getsheetid")) {
	function appcontrol_getsheetid( $formid ) {
		
		$forms = appcontrol_getforms();
		
		global $sheet_ids;
		
		foreach ($forms as $k => $form) {
			if($form["formid"] == $formid) {
				$sheet_id = $sheet_ids[$k];
				return $sheet_id;
				break;
			}
		}

	}
}

if (!function_exists("ParseFormFields")) {
	function ParseFormFields($elements) 
	{

		$form_fields = array();
		
		foreach ($elements as $node) {
			if(isset($node->tagName)) {
				if(count($node->childNodes) == 0)
					$form_fields[] = array("node" => $node);
				else
					$form_fields[] = array("node" => $node, "childs" => ParseFormFields($node->childNodes));
			}
		}

		return $form_fields; 
	}
}

if (!function_exists("getform_data")) {
	function getform_data($form_row = '') {

		global $appcontroldb;
		
		$form_data = array();

		$form_doc = new DOMDocument();
		$form_doc->loadXML($form_row["VALUE"]);
		
		$rows = $form_doc->getElementsByTagName("title");

		$title_tag = $rows[0];

		$form_data["title"] = DOMinnerHTML($title_tag);

		$instances = $form_doc->getElementsByTagName("instance");

		$form_instance = $instances[0];

		$form_instance = isset($form_instance->childNodes[1]) ? $form_instance->childNodes[1] : $form_instance->childNodes[0];

		$formid = $form_instance->getAttribute("id");

		$form_data["formid"] = $formid;

		$form_data["formname"] = $form_instance->tagName;

		$appcontroldb->where("ELEMENT_TYPE", "GROUP");
		$appcontroldb->where("ELEMENT_NAME", $form_data["formname"]);

		$tabledata = $appcontroldb->get("_form_data_model");

		$tablename = $tabledata[0]["PERSIST_AS_TABLE_NAME"];
		$table_uri = $tabledata[0]["_URI"];

		$form_data["tablename"] = $tabledata[0]["PERSIST_AS_TABLE_NAME"];
		$form_data["table_uri"] = $tabledata[0]["_URI"];

		$form_fields_data = array();

		$_form_fields_data = ParseFormFields( $form_instance->childNodes );
		
		$_names = [];
		$_fieldsdata = [];
		foreach ($_form_fields_data as $_form_field_data) {
			if( count($_form_field_data["childs"]) == 0 ) {
				if( !in_array(strtolower($_form_field_data["node"]->tagName), $_names) ) {
					
					$_names[] = strtolower($_form_field_data["node"]->tagName);
					
					$appcontroldb->where("PARENT_URI_FORM_DATA_MODEL", $table_uri);
					$appcontroldb->where("ELEMENT_NAME", strtolower($_form_field_data["node"]->tagName));

					$_fielddata = $appcontroldb->get("_form_data_model");

					$arr = array();

					if( $_fielddata[0]["ELEMENT_TYPE"] == "BINARY" ) {
						
						$arr["ELEMENT_TYPE"] = "REF_BLOB";
						$arr["PERSIST_AS_TABLE_NAME"] = $_fieldsdata[2]["PERSIST_AS_TABLE_NAME"];
						$arr["_URI"] = $_fieldsdata[2]["_URI"];

						$__k = array_search( strtolower($_form_field_data["node"]->tagName),$_names );
						unset($_names[$__k]);
						$_names = array_values($_names);

					} else {

						$arr["ELEMENT_TYPE"] = $_fieldsdata[0]["ELEMENT_TYPE"];
						$arr["PERSIST_AS_TABLE_NAME"] = $_fieldsdata[0]["PERSIST_AS_TABLE_NAME"];
						$arr["_URI"] = $_fieldsdata[0]["_URI"];

					}
					
					$_fieldsdata[strtolower($_form_field_data["node"]->tagName)] = $arr;

				}
			}
		}

		$form_data["formfields"] = $_names;
		$form_data["formfieldsdata"] = $_fieldsdata;

		return $form_data;

	}
}

if (!function_exists("getClient")) {
	
	function getClient()
	{
		
		$client = new Google_Client();
		$client->setApplicationName('Google Sheets API PHP Quickstart');
		$client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
		$client->setAuthConfig(APPCONTROL_PATH.'client_secret.json');
		$client->setAccessType('offline');

		$credentialsPath = APPCONTROL_PATH.'/credentials.json';
		$accessToken = json_decode(file_get_contents($credentialsPath), true);
		$client->setAccessToken($accessToken);

		if ($client->isAccessTokenExpired()) {
			$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
		}

		return $client;

	}

}

$gsheetdatas = [];

if (!function_exists("appcontrol_getsheetdata")) {
	function appcontrol_getsheetdata($sheet_id, $form_id) {

		$cacheDriver = CacheManager::getInstance('files');
		$kk = $sheet_id.$form_id;

		$cacheData = $cacheDriver->getItem($kk);

		global $gsheetdatas;

		if( !is_null($cacheData->get()) ) {
			$gsheetdatas[$sheet_id.$form_id] = $cacheData->get();
		}

		if( isset( $gsheetdatas[$sheet_id.$form_id] ) ) {
			return appcontrol_processdata($gsheetdatas[$sheet_id.$form_id]);
		}

		$client = getClient();
		$service = new Google_Service_Sheets($client);

		$range = $form_id.'!A1:AZ';
		$response = $service->spreadsheets_values->get($sheet_id, $range);
		$values = $response->getValues();

		$cacheData->set( $values )->expiresAfter(60);

		$cacheDriver->save( $cacheData );

		$gsheetdatas[$sheet_id.$form_id] = $values;

		return appcontrol_processdata($values);

	}
}

if (!function_exists("appcontrol_getsheetdata_byid")) {
	function appcontrol_getsheetdata_byid($sheet_id, $form_id, $rowid) {

		$data = appcontrol_getsheetdata($sheet_id, $form_id);
		foreach ($data as $row) {
			if( $row[0] == $rowid ) {
				return $row;
			}
		}
		return array();

	}
}

if (!function_exists("appcontrol_datecompare")) {
	function appcontrol_datecompare($keyindex, $ordertype)
	{
		return function($a, $b) use ($keyindex, $ordertype) {
			$t1 = strtotime($a[$keyindex]);
			$t2 = strtotime($b[$keyindex]);
			if( $ordertype == "ASC" )
				return $t1 - $t2;
			else
				return $t2 - $t1;
		};
	}
}

if (!function_exists("appcontrol_numcompare")) {
	function appcontrol_numcompare($keyindex, $ordertype)
	{
		return function($a, $b) use ($keyindex, $ordertype) {
			if( $ordertype == "ASC" )
				return doubleval( $a[$keyindex] ) - doubleval( $b[$keyindex] );
			else
				return doubleval( $b[$keyindex] ) - doubleval( $a[$keyindex] );
		};
	}
}

if (!function_exists("appcontrol_strcompare")) {
	function appcontrol_strcompare($keyindex, $ordertype)
	{
		return function($a, $b) use ($keyindex, $ordertype) {
			if( $ordertype == "ASC" )
				return strcmp($a[$keyindex], $b[$keyindex]);
			else
				return strcmp($b[$keyindex], $a[$keyindex]);
		};
	}
}

if (!function_exists("appcontrol_processdata")) {
	function appcontrol_processdata($data) {

		$_newdata = array();

		$firstrow = array_shift($data);

		$sort_column_index = -1;

		$newdata = array();

		foreach ($data as $_data) {
			if( isset( $_POST["search_date"] ) && $_POST["search_date"] != "" ) {
				$dt = strtotime($_data[3]);
				$dt = date("m/d/Y",$dt);
				//echo strcmp(date("m-d-Y",$dt), $_POST["search_date"]) ."<br />";
				if( $dt == $_POST["search_date"] ) {
					$newdata[] = $_data;
				}
			} else {
				$newdata[] = $_data;
			}
		}

		$data = $newdata;

		foreach ($firstrow as $k => $v) {
			if( getqueryvar("sortby")==$v )
				$sort_column_index = $k;
		}

		if( $sort_column_index != -1 && in_array( getqueryvar("sortby"), array( "date","starttime" )) ) {
			usort($data, appcontrol_datecompare($sort_column_index, getqueryvar("sort")));
			$_newdata = $data;
		} else if( $sort_column_index != -1 && in_array( getqueryvar("sortby"), array( "imei" )) ) {
			usort($data, appcontrol_numcompare($sort_column_index, getqueryvar("sort")));
			$_newdata = $data;
		} else {
			usort($data, appcontrol_strcompare($sort_column_index, getqueryvar("sort")));
			$_newdata = $data;
		}

		array_unshift($_newdata, $firstrow);

		return $_newdata;

	}
}

if (!function_exists("appcontrol_getsheetrow")) {
	function appcontrol_getsheetrow($sheetdata, $uuid) {

		foreach ($sheetdata as $sheetrow) {
			
			if( $sheetrow[0] == $uuid )
				return $sheetrow;

		}

		return array();

	}
}

//appcontrol_extract_images

if (!function_exists("appcontrol_extract_images")) {
	function appcontrol_extract_images($formid, $_uri) {
		global $image_tables;
		global $appcontroldb;

		$_images = array();

		$form = getform_by_id2( $formid );

		foreach ($image_tables[$form["tablename"]] as $imagetable) {
			$appcontroldb->where("_TOP_LEVEL_AURI", $_uri);
			$images = $appcontroldb->get($imagetable);
			foreach ($images as $image) {
				$_images[] = $imagetable."|".$image["_URI"];
			}
		}

		return $_images;

	}
}

if (!function_exists("appcontrol_extractsheetrow_images")) {
	function appcontrol_extractsheetrow_images($sheetdata, $uuid) {

		$row = appcontrol_getsheetrow($sheetdata, $uuid);

		$firstrow = $sheetdata[0];

		$pictures = [];

		foreach ( $firstrow as $k => $column ) {
			if( strpos(strtolower($column), "photo") !== FALSE || strpos(strtolower($column), "picture") !== FALSE ) {
				$pictures[] = $row[$k];
			}
		}

		return $pictures;

	}
}

if (!function_exists("appcontrol_exportform")) {

	function appcontrol_exportform($sheet_id, $form_id) {
		// Get the API client and construct the service object.
		$client = getClient();
		$service = new Google_Service_Sheets($client);

		$range = $form_id.'!A1:AZ';
		$response = $service->spreadsheets_values->get($sheet_id, $range);
		$values = $response->getValues();

		$csvdata = "";

		if (empty($values)) {
			//print "No data found.\n";
		} else {
			//print "Name, Major:\n";
			foreach ($values as $row) {
				foreach ($row as $val) {
					$csvdata .= "\"". str_replace("\"","\"\"",$val) ."\",";
				}
				$csvdata = substr($csvdata,0,-1) . "\r\n";
				// Print columns A and E, which correspond to indices 0 and 4.
				//printf("%s, %s\n", $row[0], $row[4]);
			}
		}

		header('Content-Type: text/application');
		header('Content-Disposition: attachment; filename="'. $form_id .'.csv"');
		echo $csvdata;

	}

}

if (!function_exists("appcontrol_exportform2")) {

	function appcontrol_exportform2($formtable) {

		global $appcontroldb;
		
		$csvdata = "";

		$values = $appcontroldb->get($formtable);

		foreach ($values as $row) {
			$firstrow = "";
			foreach ($row as $col_name => $v) {
				$firstrow .= "\"". str_replace("\"","\"\"",$col_name) ."\",";
			}
			$csvdata .= substr($firstrow,0,-1) . "\r\n";
			break;
		}

		if (empty($values)) {
			//print "No data found.\n";
		} else {
			//print "Name, Major:\n";
			foreach ($values as $row) {
				foreach ($row as $val) {
					$csvdata .= "\"". str_replace("\"","\"\"",$val) ."\",";
				}
				$csvdata = substr($csvdata,0,-1) . "\r\n";
				// Print columns A and E, which correspond to indices 0 and 4.
				//printf("%s, %s\n", $row[0], $row[4]);
			}
		}

		header('Content-Type: text/application');
		header('Content-Disposition: attachment; filename="'. $formtable .'.csv"');
		echo $csvdata;

	}

}

if (!function_exists("appcontrol_getforms")) {

	function appcontrol_getforms() {
		global $appcontroldb;
		$app_forms = $appcontroldb->get("_form_info_xform_blb");

		$_app_forms = array();

		foreach ($app_forms as $key => $app_form) {
			
			$_d = getform_data($app_form);
			
			$_app_forms[] = $_d;

			if( $key == 3 )
				break;

		}

		return $_app_forms;
	}

}

if (!function_exists("appcontrol_getforms2")) {
	function appcontrol_getforms2() {

		global $reporttables;
		global $alltables;
		global $tablesfields;

		$forms = [];

		foreach ($reporttables as $formtable ) {
			
			$form = $alltables[$formtable];
			$formdata = array();
			$formdata["title"] = $form[1];
			$formdata["formid"] = $form[0];
			$formdata["formname"] = $form[0];
			$formdata["tablename"] = $formtable;
			$formdata["formfields"] = $tablesfields[$formtable];

			$forms[$form[0]] = $formdata;

		}

		return $forms;

	}
}

if (!function_exists("getform_by_id2")) {

	function getform_by_id2($formid) {

		$forms = appcontrol_getforms2();

		return isset($forms[$formid]) ? $forms[$formid] : false;

	}

}

if (!function_exists("appcontrol_getform_data2")) {
	function appcontrol_getform_data2($formid, $withtable = false) {

		if( $withtable == false )
			$form = getform_by_id2( $formid );

		global $appcontroldb;

		if( isset( $_POST["search_date"] ) && $_POST["search_date"] != "" ) {
			
			$_dt = strtotime($_POST["search_date"]);//$_data["_CREATION_DATE"]);

			$dt = date("Y-m-d 00:00:00",$_dt);
			$dt2 = date("Y-m-d 23:59:59",$_dt);

			$appcontroldb->where("_CREATION_DATE > '". $dt ."'");
			$appcontroldb->where("_CREATION_DATE < '". $dt2 ."'");

		}

		if( getqueryvar("sort") && getqueryvar("sortby") ) {

			$appcontroldb->orderBy(getqueryvar("sortby"), getqueryvar("sort"));

		}

		if( $withtable == false )
			return $appcontroldb->get($form["tablename"]);

		$appcontroldb->where("_PARENT_AURI",get_query_var("row"));
		return $appcontroldb->get($formid);

	}
}

if (!function_exists("appcontrol_formsubmissions")) {
	function appcontrol_formsubmissions( $form_id = "", $project_code = "" ) {

		global $appcontroldb;
		global $tablesfields;

		$form = getform_by_id2( $form_id );

		if( $project_code != "" && isset( $tablesfields[$form["tablename"]]["PROJECT_CODE"] ) ) {
			$appcontroldb->where("PROJECT_CODE", $project_code);
		} elseif( $project_code != "" ) {
			return 0;
		}

		$submissions = $appcontroldb->getOne($form["tablename"], "count(*) as cnt");

		return $submissions["cnt"];

	}
}

if (!function_exists("appcontrol_lastsubmission")) {
	function appcontrol_lastsubmission( $form_id = "" ) {

		global $appcontroldb;

		$form = getform_by_id2( $form_id );
		
		$appcontroldb->orderBy("_CREATION_DATE", "DESC");

		$lastsubmission = $appcontroldb->getValue($form["tablename"], "_CREATION_DATE");

		return $lastsubmission;

	}
}

if (!function_exists("appcontrol_totalsubmissions")) {
	function appcontrol_totalsubmissions($project_code = "") {
		$forms = appcontrol_getforms2();
		$formsubmissions = 0;
		foreach ($forms as $form) {
			$formsubmissions += appcontrol_formsubmissions($form["formid"], $project_code);
		}
		return $formsubmissions;
	}
}

if (!function_exists("appcontrol_uccovered")) {
	
	function appcontrol_uccovered( $table ) {

		global $appcontroldb;

		$covereduc = $appcontroldb->rawQueryOne('SELECT COUNT(*) as total_uc FROM ( SELECT uc, COUNT(*) as ct FROM '. $table .' GROUP BY uc HAVING COUNT(*) > 1 ) a');

		return $covereduc["total_uc"];

	}

}

if (!function_exists("appcontrol_alluccovered")) {

	function appcontrol_alluccovered() {

		$forms = appcontrol_getforms();
		$uccovered = 0;
		foreach ($forms as $form) {
			$uccovered += appcontrol_uccovered($form["tablename"]);
		}
		return $uccovered;

	}

}

if (!function_exists("appcontrol_districtcovered")) {
	
	function appcontrol_districtcovered( $table ) {

		global $appcontroldb;

		$covereddistrict = $appcontroldb->rawQueryOne('SELECT COUNT(*) as total_district FROM ( SELECT District, COUNT(*) as ct FROM '. $table .' GROUP BY District HAVING COUNT(*) > 1 ) a');

		return $covereddistrict["total_district"];

	}

}

if (!function_exists("appcontrol_allmonthlycompletedsurvey")) {
	
	function appcontrol_allmonthlycompletedsurvey( $project_code = "" ) {

		global $appcontroldb;
		global $tablesfields;

		$monthly_values = [];

		$forms = appcontrol_getforms();

		for ($i=1; $i <= 12; $i++) {

			$surveys = 0;
			
			foreach ($forms as $form) {

				$where = "";

				if( $project_code != "" && isset( $tablesfields[$form["tablename"]]["PROJECT_CODE"] ) ) {
					$where = " AND PROJECT_CODE='". $appcontroldb->escape($project_code) ."'";
				} elseif( $project_code != "" ) {
					continue;
				}
				
				$q = $appcontroldb->rawQueryOne("SELECT COUNT(*) as surveys FROM ". $form["tablename"] ." WHERE _CREATION_DATE > '". date("Y-") .$i. "-01' {$where} and _CREATION_DATE < '". date("Y-") . $i ."-". cal_days_in_month(CAL_GREGORIAN, intval($i), intval(date("Y")) ) ."'");

				$surveys += $q["surveys"];

			}

			$monthly_values[] = $surveys;

		}

		return $monthly_values;

	}

}

if (!function_exists("appcontrol_monthlycompletedsurvey")) {
	
	function appcontrol_monthlycompletedsurvey( $formid ) {

		global $appcontroldb;

		$monthly_values = [];

		$forms = appcontrol_getforms();

		for ($i=1; $i <= 12; $i++) {

			$surveys = 0;
			
			foreach ($forms as $form) {

				if( $formid != $form["formid"] )
					continue;
				
				$q = $appcontroldb->rawQueryOne("SELECT COUNT(*) as surveys FROM ". $form["tablename"] ." WHERE _CREATION_DATE > '". date("Y-") .$i. "-01' and _CREATION_DATE < '". date("Y-") . $i ."-". cal_days_in_month(CAL_GREGORIAN, intval($i), intval(date("Y")) ) ."'");

				$surveys += $q["surveys"];

			}

			$monthly_values[] = $surveys;

		}

		return $monthly_values;

	}

}

if (!function_exists("appcontrol_alldailycompletedsurvey")) {
	
	function appcontrol_alldailycompletedsurvey( $project_code = "" ) {

		global $appcontroldb;
		global $tablesfields;

		$daily_values = [];

		$forms = appcontrol_getforms();

		for ($i=1; $i <= cal_days_in_month(CAL_GREGORIAN, intval(date("m")), intval(date("Y")) ); $i++) {

			$surveys = 0;
			
			foreach ($forms as $form) {

				$where = "";

				if( $project_code != "" && isset( $tablesfields[$form["tablename"]]["PROJECT_CODE"] ) ) {
					$where = " AND PROJECT_CODE='". $appcontroldb->escape($project_code) ."'";
				} elseif( $project_code != "" ) {
					continue;
				}
				
				$q = $appcontroldb->rawQueryOne("SELECT COUNT(*) as surveys FROM ". $form["tablename"] ." WHERE _CREATION_DATE > '". date("Y-m-") .$i. " 00:00:00' {$where} and _CREATION_DATE < '". date("Y-m-") .$i. " 23:59:59'");

				$surveys += $q["surveys"];

			}

			$daily_values[] = $surveys;

		}

		return $daily_values;

	}

}

if (!function_exists("appcontrol_dailycompletedsurvey")) {
	
	function appcontrol_dailycompletedsurvey( $formid, $project_code = "" ) {

		global $appcontroldb;
		global $tablesfields;

		$daily_values = [];

		$forms = appcontrol_getforms();

		for ($i=1; $i <= cal_days_in_month(CAL_GREGORIAN, intval(date("m")), intval(date("Y")) ); $i++) {

			$surveys = 0;
			
			foreach ($forms as $form) {

				if( $formid != $form["formid"] )
					continue;

				$where = "";

				if( $project_code != "" && isset( $tablesfields[$form["tablename"]]["PROJECT_CODE"] ) ) {
					$where = " AND PROJECT_CODE='". $appcontroldb->escape($project_code) ."' ";
				} elseif( $project_code != "" ) {
					break;
				}
				
				$q = $appcontroldb->rawQueryOne("SELECT COUNT(*) as surveys FROM ". $form["tablename"] ." WHERE _CREATION_DATE > '". date("Y-m-") .$i. " 00:00:00' {$where} and _CREATION_DATE < '". date("Y-m-") .$i. " 23:59:59'");

				$surveys += $q["surveys"];

			}

			$daily_values[] = $surveys;

		}

		return $daily_values;

	}

}

if (!function_exists("appcontrol_allgrouptypevals")) {
	
	function appcontrol_allgrouptypevals($project_code = "") {

		global $appcontroldb;
		global $tablesfields;

		$grouptypevals = [];

		$forms = appcontrol_getforms2();
    
		foreach ($forms as $form) {

			if( !isset( $tablesfields[$form["tablename"]] ) || !isset($tablesfields[$form["tablename"]]["GROUP_TYPE"]) ) continue;

			$where = "";

			if( $project_code != "" && isset( $tablesfields[$form["tablename"]]["PROJECT_CODE"] ) ) {
				$where = "WHERE PROJECT_CODE='". $appcontroldb->escape($project_code) ."'";
			} elseif( $project_code != "" ) {
				continue;
			}
				
			$q = $appcontroldb->rawQuery("SELECT GROUP_TYPE, COUNT(*) as groupcnt FROM {$form['tablename']} {$where} GROUP BY GROUP_TYPE HAVING COUNT(*) > 0");
        

			foreach ($q as $row) {
				if( isset($grouptypevals[ $row["GROUP_TYPE"] ]) ) {
					$grouptypevals[ $row["GROUP_TYPE"] ] += $row["groupcnt"];
				} else {
					$grouptypevals[ $row["GROUP_TYPE"] ] = $row["groupcnt"];
				}
			}

			break;

		}

		return $grouptypevals;

	}

}

if (!function_exists("appcontrol_grouptypevals")) {
	
	function appcontrol_grouptypevals() {

		global $appcontroldb;
		
		$form1_group_formation_report_core = strtoupper("form1_group_formation_report_core");

		$form1_group_formation_report_member_details = strtoupper("form1_group_formation_report_member_details");

		$grouptypevals = [];

		$q = $appcontroldb->rawQuery("SELECT GROUP_TYPE, COUNT(*) as groupcnt FROM {$form1_group_formation_report_core} GROUP BY GROUP_TYPE HAVING COUNT(*) > 0");

		$q2 = $appcontroldb->rawQuery("

			SELECT
			
			{$form1_group_formation_report_core}.GROUP_TYPE,
			{$form1_group_formation_report_member_details}.GROUP_MEMBER_GENDER,
			COUNT(*) as gendercnt

			FROM {$form1_group_formation_report_member_details}

			LEFT JOIN {$form1_group_formation_report_core}
				ON(
					{$form1_group_formation_report_member_details}._PARENT_AURI={$form1_group_formation_report_core}._URI
				)

			WHERE

			{$form1_group_formation_report_member_details}.GROUP_MEMBER_GENDER='Male'

			GROUP BY {$form1_group_formation_report_core}.GROUP_TYPE

			HAVING COUNT(*) > 0

		");

		$q3 = $appcontroldb->rawQuery("

			SELECT
			
			{$form1_group_formation_report_core}.GROUP_TYPE,
			{$form1_group_formation_report_member_details}.GROUP_MEMBER_GENDER,
			COUNT(*) as gendercnt

			FROM {$form1_group_formation_report_member_details}

			LEFT JOIN {$form1_group_formation_report_core}
				ON(
					{$form1_group_formation_report_member_details}._PARENT_AURI={$form1_group_formation_report_core}._URI
				)

			WHERE

			{$form1_group_formation_report_member_details}.GROUP_MEMBER_GENDER='Female'

			GROUP BY {$form1_group_formation_report_core}.GROUP_TYPE

			HAVING COUNT(*) > 0

		");

		foreach ($q as $row) {
			if( isset($grouptypevals[ $row["GROUP_TYPE"] ]) ) {
				$grouptypevals[ $row["GROUP_TYPE"] ]["total"] += $row["groupcnt"];
			} else {
				$grouptypevals[ $row["GROUP_TYPE"] ] = [];
				$grouptypevals[ $row["GROUP_TYPE"] ]["total"] = $row["groupcnt"];
			}
		}

		if( !isset($grouptypevals[ $row["GROUP_TYPE"] ]["total"]) ) {
			$grouptypevals[ $row["GROUP_TYPE"] ]["total"] = 0;
		}

		foreach ($q2 as $row) {
			if( isset($grouptypevals[ $row["GROUP_TYPE"] ]) ) {
				$grouptypevals[ $row["GROUP_TYPE"] ]["male"] += $row["gendercnt"];
			} else {
				$grouptypevals[ $row["GROUP_TYPE"] ] = [];
				$grouptypevals[ $row["GROUP_TYPE"] ]["male"] = $row["gendercnt"];
			}
		}

		if( !isset($grouptypevals[ $row["GROUP_TYPE"] ]["male"]) ) {
			$grouptypevals[ $row["GROUP_TYPE"] ]["male"] = 0;
		}

		foreach ($q3 as $row) {
			if( isset($grouptypevals[ $row["GROUP_TYPE"] ]) ) {
				$grouptypevals[ $row["GROUP_TYPE"] ]["female"] += $row["gendercnt"];
			} else {
				$grouptypevals[ $row["GROUP_TYPE"] ] = [];
				$grouptypevals[ $row["GROUP_TYPE"] ]["female"] = $row["gendercnt"];
			}
		}

		if( !isset($grouptypevals[ $row["GROUP_TYPE"] ]["female"]) ) {
			$grouptypevals[ $row["GROUP_TYPE"] ]["female"] = 0;
		}

		return $grouptypevals;

	}

}

if (!function_exists("appcontrol_allupidpsdata")) {
	function appcontrol_allupidpsdata( $project_code = "" ) {

		global $appcontroldb;
		global $tablesfields;

		$where = "";

		$form2432318_core = strtoupper("form18eport_core");

		if( $project_code != "" && isset( $tablesfields["form18eport_core"]["PROJECT_CODE"] ) ) {
			$where = "WHERE PROJECT_CODE='". $appcontroldb->escape($project_code) ."'";
		} elseif( $project_code != "" ) {
			return array();
		}

		$q = $appcontroldb->rawQuery("SELECT COUNT(*) as level_count, UPIDP_LEVEL FROM {$form2432318_core} {$where} GROUP BY {$form2432318_core}.UPIDP_LEVEL HAVING COUNT(*) > 0");

		$data = array();

		foreach ($q as $k => $v) {
			$data[$v["UPIDP_LEVEL"]] = intval($v["level_count"]);
		}

		return $data;

	}
}

if (!function_exists("appcontrol_sumupidpsdata")) {
	function appcontrol_sumupidpsdata( $project_code = "" ) {

		$data = appcontrol_allupidpsdata( $project_code );

		$sum = 0;

		if( !empty($data) )
		foreach ($data as $v) {
			$sum += $v;
		}

		return $sum;

	}
}

if (!function_exists("appcontrol_sumgrouptypes")) {
	function appcontrol_sumgrouptypes($project_code = "") {

		global $appcontroldb;
		global $tablesfields;

		$form1_group_formation_report_core = strtoupper("form1_group_formation_report_core");

		$where = "";

		if( $project_code != "" && isset( $tablesfields[$form["tablename"]]["PROJECT_CODE"] ) ) {
			$where = "WHERE PROJECT_CODE='". $appcontroldb->escape($project_code) ."'";
		} elseif( $project_code != "" ) {
			return 0;
		}

		$q = $appcontroldb->rawQuery("SELECT COUNT(*) as sumall FROM {$form1_group_formation_report_core} $where");

		return intval($q[0]["sumall"]);

	}
}

if (!function_exists("appcontrol_memberdetailsByGroup")) {
	
	function appcontrol_memberdetailsByGroup($groupname) {

		global $appcontroldb;

		$groupname = $appcontroldb->escape($groupname);

		$form1_group_formation_report_core = strtoupper("form1_group_formation_report_core");

		$form1_group_formation_report_member_details = strtoupper("form1_group_formation_report_member_details");



		$q = $appcontroldb->rawQuery("

			SELECT
			
			{$form1_group_formation_report_core}.GROUP_TYPE,
			{$form1_group_formation_report_member_details}.*

			FROM {$form1_group_formation_report_member_details}

			LEFT JOIN {$form1_group_formation_report_core}
				ON(
					{$form1_group_formation_report_member_details}._PARENT_AURI={$form1_group_formation_report_core}._URI
				)

			WHERE

			{$form1_group_formation_report_core}.GROUP_TYPE='{$groupname}'

		");

		return $q;

	}

}

//GROUP_TYPE

if (!function_exists("appcontrol_alldistrictcovered")) {

	function appcontrol_alldistrictcovered() {

		$forms = appcontrol_getforms();
		$districtcovered = 0;
		foreach ($forms as $form) {
			$districtcovered += appcontrol_districtcovered($form["tablename"]);
		}
		return $districtcovered;

	}

}

if (!function_exists("appcontrol_tehsilcovered")) {
	
	function appcontrol_tehsilcovered( $table ) {

		global $appcontroldb;

		$coveredtehsil = $appcontroldb->rawQueryOne('SELECT COUNT(*) as total_tehsil FROM ( SELECT tehsil, COUNT(*) as ct FROM '. $table .' GROUP BY tehsil HAVING COUNT(*) > 1 ) a');

		return $coveredtehsil["total_tehsil"];

	}

}

if (!function_exists("appcontrol_alltehsilcovered")) {

	function appcontrol_alltehsilcovered() {

		$forms = appcontrol_getforms();
		$tehsilcovered = 0;
		foreach ($forms as $form) {
			$tehsilcovered += appcontrol_tehsilcovered($form["tablename"]);
		}
		return $tehsilcovered;

	}

}

if (!function_exists("getform_by_id")) {

	function getform_by_id($formid) {

		global $appcontroldb;
		$app_forms = $appcontroldb->get("_form_info_xform_blb");

		$_app_form = false;

		foreach ($app_forms as $key => $app_form) {
			
			$_d = getform_data($app_form);
			
			if( $_d["formid"] == $formid ) {
				$_app_form = $_d;
				break;
			}

		}

		return $_app_form;

	}

}

if (!function_exists('appcontrol_redirect')) {
	function appcontrol_redirect($url) {
		$url = admin_url('admin.php?page='. $url);
		wp_redirect($url);
		echo '<script>location="'. $url .'"</script>';
	}
}

if (!function_exists('appcontrol_frontredirect')) {
	function appcontrol_frontredirect($url) {
		$url = site_url($url);
		wp_redirect($url);
		echo '<script>location="'. $url .'"</script>';
	}
}

if (!function_exists('appcontrol_url')) {
	function appcontrol_url($url) {
		$url = admin_url('admin.php?page='. $url);
		return $url;
	}
}


if (!function_exists("getpostvar")) {
	function getpostvar($name, $default = false) {
		return isset($_POST[$name]) ? $_POST[$name] : $default;
	}
}

if (!function_exists("getqueryvar")) {
	function getqueryvar($name, $default = false) {
		return isset($_GET[$name]) ? $_GET[$name] : $default;
	}
}


if (!function_exists("get_appcontrolusers")) {
	function get_appcontrolusers() {
		$wpusers = get_users();
		return $wpusers;
	}
}

if (!function_exists("appcontrol_getreports")) {
	function appcontrol_getreports( $formid ) {

		global $appcontroldb;
		$appcontroldb->where("formid",$formid);
		$reports = $appcontroldb->get("reports");
		return $reports;

	}
}

if (!function_exists("appcontrol_getreport_by_id")) {
	function appcontrol_getreport_by_id( $id ) {

		global $appcontroldb;
		$appcontroldb->where("id",$id);
		$reports = $appcontroldb->get("reports");

		$report = $reports[0];

		$appcontroldb->where("reportid", $id);
		$columns = $appcontroldb->get("report_columns");

		$report["columns"] = array();

		foreach ($columns as $column) {
			$report["columns"][] = $column["formcolumn"];
		}

		return $report;

	}
}

if (!function_exists("appcontrol_updatereportfields")) {
	function appcontrol_updatereportfields( $fields, $reportid ) {

		global $appcontroldb;
		//$appcontroldb->where("reportid",$reportid);
		//$report_columns = $appcontroldb->get("report_columns");
		$appcontroldb->rawQuery("DELETE FROM report_columns WHERE reportid=?", array($reportid));

		$columns = array();

		foreach ($fields as $field) {
			$columns[] = array(
				"reportid" => $reportid,
				"formcolumn" => $field
			);
		}

		$appcontroldb->insertMulti("report_columns", $columns);
		//return $report_columns[0];

	}
}

if (!function_exists("appcontrol_deleteformrow")) {
	function appcontrol_deleteformrow( $formid, $rowid ) {

		global $appcontroldb;

		$form = getform_by_id2( $formid );
		//$appcontroldb->where("reportid",$reportid);
		//$report_columns = $appcontroldb->get("report_columns");
		return $appcontroldb->rawQuery("DELETE FROM ". $form["tablename"] ." WHERE _URI=?", array($rowid));

	}
}

if (!function_exists("appcontrol_addreportfields")) {
	function appcontrol_addreportfields( $reportname, $fields, $formid ) {

		global $appcontroldb;
		
		$reportid = $appcontroldb->insert("reports", array("reportname" => $reportname, "user_id" => 1, "adddate" => date("Y-m-d H:i:s"), "formid" => $formid));

		$columns = array();

		foreach ($fields as $field) {
			$columns[] = array(
				"reportid" => $reportid,
				"formcolumn" => $field
			);
		}

		$appcontroldb->insertMulti("report_columns", $columns);
		//return $report_columns[0];

		return $reportid;

	}
}

if (!function_exists("from_camel_case")) {
	function from_camel_case($input) {
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
		$ret = $matches[0];
		foreach ($ret as &$match) {
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}
		return implode('_', $ret);
	}
}

if (!function_exists("appcontrol_get_formdata")) {
	function appcontrol_get_formdata($formid) {

		global $appcontroldb;


		$form = getform_by_id( $formid );

		$data = $appcontroldb->get($form["tablename"]);

		return $data;

	}
}

if (!function_exists("appcontrol_getformrowdata")) {
	function appcontrol_getformrowdata($form,$rowid, $tablename = false) {
		//$form_uuid = $form["table_uri"];
		//$form_uuid
		global $appcontroldb;

		$appcontroldb->where("_URI", $rowid);

		if( $tablename == false )
			$row = $appcontroldb->get( $form["tablename"] );
		else
			$row = $appcontroldb->get( $form );

		return $row[0];
	}
}

if (!function_exists("adminlte_inc")) {
	function adminlte_inc($path) {
		require APPCONTROL_PATH."inc/adminlte/".$path;
	}
}

if (!function_exists("adminlte_assets")) {
	function adminlte_assets($path) {
		return APPCONTROL_URL."assets/adminlte/".$path;
	}
}

if (!function_exists("appcontrol_buildquery")) {
	function appcontrol_buildquery($params = array()) {
		$paramstring = "";
		$allparams = array();
		foreach ($_GET as $k => $v) {
			$allparams[urlencode($k)] = urlencode($v);
		}
		foreach ($params as $k => $v) {
			$allparams[urlencode($k)] = urlencode($v);
		}
		foreach ($allparams as $k => $v) {
			$paramstring .= urlencode($k)."=".urlencode($v)."&";
		}
		$paramstring = substr($paramstring, 0, -1);
		return $paramstring;
	}
}

if (!function_exists("appcontrol_changesort")) {
	function appcontrol_changesort($default = "ASC") {
		if( !isset($_GET["sort"]) )
			return $default;
		else
			return $_GET["sort"] == "ASC" ? "DESC" : "ASC";
	}
}


if (!function_exists("appcontrol_checklogin")) {
	function appcontrol_checklogin() {
		if( isset( $_SESSION["user"] ) ) {
			return true;
		} else {
			return false;
		}
	}
}

if (!function_exists("appcontrol_sessionuser")) {
	function appcontrol_sessionuser() {
		if( isset( $_SESSION["user"] ) ) {
			return $_SESSION["user"];
		}
	}
}

if (!function_exists("appcontrol_projectprogress")) {
	function appcontrol_projectprogress($project_code = "") {

		global $appcontroldb;

		if( $project_code != "" ) {
			$appcontroldb->where("project_code", $project_code);
		}

		$plans = $appcontroldb->get("workplans");

		$totalAchievements = 0;
		$totalTotalTarget = 0;

		foreach ($plans as $plan) {
			$totalAchievements += $plan["achievement"];
			$totalTotalTarget += $plan["total_target"];
		}

		$totalTotalTarget = $totalTotalTarget == 0 ? 1 : $totalTotalTarget;

		return round($totalAchievements/$totalTotalTarget, 2);

	}
}


if (!function_exists("appcontrol_exportworkplan")) {
	function appcontrol_exportworkplan() {

		global $appcontroldb;
		
		$csvdata = "";

		$values = $appcontroldb->get("workplans");

		foreach ($values as $row) {
			$firstrow = "";
			foreach ($row as $col_name => $v) {
				$firstrow .= "\"". str_replace("\"","\"\"",$col_name) ."\",";
			}
			$csvdata .= substr($firstrow,0,-1) . "\r\n";
			break;
		}

		if (empty($values)) {
			//print "No data found.\n";
		} else {
			//print "Name, Major:\n";
			foreach ($values as $row) {
				foreach ($row as $val) {
					$csvdata .= "\"". str_replace("\"","\"\"",$val) ."\",";
				}
				$csvdata = substr($csvdata,0,-1) . "\r\n";
				// Print columns A and E, which correspond to indices 0 and 4.
				//printf("%s, %s\n", $row[0], $row[4]);
			}
		}

		header('Content-Type: text/application');
		header('Content-Disposition: attachment; filename="workplans.csv"');
		echo $csvdata;

	}
}

if (!function_exists("appcontrol_colorize")) {
	function appcontrol_colorize($num) {

		$r = 255;
		$b = 255;
		$g = 255;

		if( $num <= 255 )
			$r = $num;
		else {

			$_num = $num;
			$i = 1;
			$v = "b";

			while( $_num <= 255 ) {
				
				if( $i == 1 )
					$v = "b";
				else
					$v = "g";

				$_num2 = $_num / 255;
				if( $_num2 != floor($_num2) ) {
					$$v = floor( ($_num2 % 1) * 10 );
				} else
					$$v = 255;

				$_num -= 255;

				$i++;

			}

			$r = $_num;

		}

		$r = $r < 50 ? 100 : $r;
		$b = $b < 50 ? 150 : $b;
		$c = $c < 50 ? 120 : $c;

		return dechex($r) . dechex($b) . dechex($c);

	}
}

if (!function_exists("appcontrol_formfield")) {
	function appcontrol_formfield($name,$field_data, $value = "") {

		if( $field_data["type"] == "select" ) {

			$html = '<select name="formfields['.$name.']" class="form-control">';
			foreach ($field_data["options"] as $k => $v) {
				$html .= '<option value="'. $k .'" '. ($k == $value ? "selected" : "") .'>';
				$html .= $v;
				$html .= '</option>';
			}
			$html .= '</select>';

			return $html;

		} else {

			return '<input type="text" class="form-control" name="formfields['.$name.']" value="'. $value .'" />';

		}

	}
}