<h1>
	Form Data
</h1>

<h4>
	<?php echo $form["title"] ?>
</h4>

<?php $formdata = appcontrol_get_formdata( $form["formid"] ); ?>

<div class="col-md-12">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<?php foreach ($form["formfields"] as $formfield): ?>
				<th>
					<?= $formfield ?>
				</th>
				<?php endforeach ?>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($formdata as $_data): ?>

			<tr>
				<?php foreach ($form["formfields"] as $formfield) { ?>
					<td>
						<?= $_data[strtoupper($formfield)] ?>
					</td>
				<?php } ?>
				<td>
					<a href="<?= admin_url("admin.php?page=appcontrol-editformdata&formid=". $app_form_data["formid"] ."&row=". $_data["_URI"]) ?>" class="btn btn-primary">
						Edit
					</a>
					<button onclick="if(confirm('You really want to delete this record?')) window.location = this.getAttribute('data-url');" data-url="<?= admin_url("admin.php?page=appcontrol-deleteformdata&formid=". $app_form_data["formid"] ."&row=". $_data["_URI"]) ?>" class="btn btn-danger">
						Delete
					</button>
				</td>
			</tr>

			<?php endforeach ?>
			<?php if (count($formdata) == 0): ?>
			<tr>
				<td colspan="<?= count($form["formfields"]) ?>">
					<center>
						No records found
					</center>
				</td>
			</tr>
			<?php endif ?>
		</tbody>
	</table>
</div>