<h1>
	Edit Form
</h1>

<h4>
	<?php echo $form["title"] ?>
</h4>

<?php $data = appcontrol_getformrowdata( $form,$row_id ) ?>

<div class="col-md-12">
	<form class="form" method="POST" action="<?= appcontrol_url("appcontrol-editformdata&formid=". $form["formid"] ."&row=". $row_id) ?>">
		<?php foreach ($form["formfields"] as $formfield): ?>
		<div class="form-group">
			<label>
				<?= $formfield ?>
			</label>
			<input type="text" name="formfields[<?= $formfield ?>]" value="<?= $data[strtoupper($formfield)] ?>" />
		</div>
		<?php endforeach ?>
		<input type="hidden" name="saveformdata" value="1" />
		<input type="submit" value="Update" class="btn btn-primary" />
	</form>
</div>