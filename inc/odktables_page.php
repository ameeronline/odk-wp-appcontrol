<h1>Odk Tables</h1>

<?php global $appcontroldb; ?>

<?php $app_forms = $appcontroldb->get("_form_info_xform_blb"); ?>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>
				#
			</th>
			<th>
				Form name
			</th>
			<th>
				Action
			</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($app_forms as $key => $app_form): ?>
		<?php $app_form_data = getform_data($app_form) ?>
		<tr>
			<td>
				<?php echo $key+1 ?>
			</td>
			<td>
				<?php echo $app_form_data["title"] ?>
			</td>
			<td>
				<a href="<?= admin_url("admin.php?page=appcontrol-formdata&formid=". $app_form_data["formid"]) ?>" class="btn btn-warning">
					Data
				</a>
				<a href="<?= admin_url("admin.php?page=appcontrol-formpermission&formid=". $app_form_data["formid"]) ?>" class="btn btn-info">
					Permissions
				</a>
				<a href="<?= admin_url("admin.php?page=appcontrol-formreports&formid=". $app_form_data["formid"]) ?>" class="btn btn-primary">
					Reports
				</a>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>