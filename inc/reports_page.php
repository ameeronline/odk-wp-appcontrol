<h1 class="page-Title">
	Reports <a href="<?= admin_url("admin.php?page=appcontrol-formaddreport") ?>" class="btn btn-primary">Add New</a>
</h1>

<h4>Form name: <?= $form["title"] ?></h4>

<div class="col-md-12">
	
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>
					Report name
				</th>
				<th>
					Created by Admin
				</th>
				<th>
					Actions
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($reports as $report): ?>
			<tr>
				<td>
					<?= $report["reportname"] ?>
				</td>
				<td>
					<?= $report["user_id"] == 1 ? "Yes" : "No" ?>
				</td>
				<td>
					<a href="<?= admin_url("admin.php?page=appcontrol-formeditreport&reportid=". $report["id"]) ?>" class="btn btn-primary">Edit</a>
					<button class="btn btn-danger" onclick="confirm('You really want to delete this report?') ? console.log(true) : console.log(false) ">Delete</button>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>

</div>