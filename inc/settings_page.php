<h2 class="page-title">
	Settings
</h2>

<?php if ( isset( $success ) ): ?>

	<p class='bg-success' style='padding:10px'>Settings Successfully saved</p>
	
<?php endif ?>

<form class="form" method="POST" action="?page=appcontrol-setting">
	<div class="form-group">
		<label>Database Host</label>
		<input type="text" name="appcontrol_dbhost" value="<?= get_option("appcontrol_dbhost", "localhost") ?>" />
	</div>
	<div class="form-group">
		<label>Database Username</label>
		<input type="text" name="appcontrol_dbuser" value="<?= get_option("appcontrol_dbuser", "root") ?>" />
	</div>
	<div class="form-group">
		<label>Database Password</label>
		<input type="text" name="appcontrol_dbpass" value="<?= get_option("appcontrol_dbpass", "") ?>" />
	</div>
	<div class="form-group">
		<label>Database Port</label>
		<input type="text" name="appcontrol_dbport" value="<?= get_option("appcontrol_dbport", "3306") ?>" />
	</div>
	<div class="form-group">
		<label>Database name</label>
		<input type="text" name="appcontrol_dbname" value="<?= get_option("appcontrol_dbname", "appcontrol_odkdb") ?>" />
	</div>
	<input type="hidden" name="saveappcontrol_settings" value="1">
	<button class="button">Submit</button>
</form>