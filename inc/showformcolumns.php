<?php foreach ($form["formfields"] as $formfield): ?>

<div class="col-md-4">

	<label style="color: #000;font-weight: normal;">

		<input type="checkbox" name="formfields[]" value="<?= $formfield ?>" <?= in_array($formfield, $report["columns"]) ? "checked" : "" ?> /> <span style="position: relative;top: 2.5px;"><?= strtoupper($formfield) ?></span>

	</label>
	
</div>
	
<?php endforeach ?>