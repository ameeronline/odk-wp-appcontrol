<h2 class="page-title">
	User management
</h2>

<?php $users = array("Ameer", "Ukasha", "Ahmad"); ?>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>
				Username
			</th>
			<th>
				Permissions
			</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $k => $user): ?>
		<tr>
			<td>
				<?php echo $k+1 ?>
			</td>
			<td>
				<?php echo $user  ?>
			</td>
			<td>
				<table class="table">
					<tr>
						<th>
							Create
						</th>
						<th>
							Update
						</th>
						<th>
							Delete
						</th>
					</tr>
					<tr>
						<td>
							<input type="checkbox" name="c1[1]" value="100" />
						</td>
						<td>
							<input type="checkbox" name="c2[1]" value="100" />
						</td>
						<td>
							<input type="checkbox" name="c3[1]" value="100" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>